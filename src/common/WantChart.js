import React from 'react';
import {
    View,
    Text,
    Image
} from 'react-native';
import styles from '../component/styles'
import { wp, hp } from '../helper/responsiveScreen';
import Constant from '../helper/themeHelper';
import { strings } from '../component/translation/i18n';
import { Circle, Line, Text as ChartText } from 'react-native-svg'
import { LineChart } from 'react-native-svg-charts';
import { TouchableOpacity } from 'react-native-gesture-handler';

class WantChart extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            budgetWidth: null,
            spentWidth: null
        };
    }

    render() {
        const { summaryTitle, chartView, btnView } = styles;
        const { budgetWidth, spentWidth } = this.state;
        const { wantChartData } = this.props;

        const WantChartData = [
            {
                data: wantChartData.line1,
                svg: { stroke: '#FABE00', strokeWidth: wp(0.8) },
            },
            {
                data: wantChartData.line2,
                svg: { stroke: '#AD896E', strokeWidth: wp(0.8) },
            },
        ]

        const WantHorizontalLine = (({ y }) => (
            <Line
                key={'zero-axis'}
                x1={'0%'}
                x2={'100%'}
                y1={y(wantChartData.budgetLineValue)}
                y2={y(wantChartData.budgetLineValue)}
                stroke={'grey'}
                strokeDasharray={[4, 6]}
                strokeWidth={2}
            />
        ))

        const WantDecorator1 = ({ x, y, data }) => {
            return (
                <Circle
                    // key={ index }
                    cx={x(wantChartData.line1.length - 1)}
                    cy={y(wantChartData.line1[wantChartData.line1.length - 1])}
                    r={4}
                    stroke={'#FABE00'}
                    fill={'#FABE00'}
                />
            )
        }

        const WantDecorator2 = ({ x, y, data }) => {
            return (
                <Circle
                    // key={ index }
                    cx={x(wantChartData.line2.length - 1)}
                    cy={y(wantChartData.line2[wantChartData.line2.length - 1])}
                    r={4}
                    stroke={'#AD896E'}
                    fill={'#AD896E'}
                />
            )
        }

        const WantLabel = (({ x, y }) => (
            <ChartText
                stroke={Constant.color.gray}
                fontSize={Constant.fontSize.mini}
                // fontWeight="bold"
                x={x(2.5)}
                y={y(wantChartData.budgetLineValue) - 7}
                textAnchor="middle"
            >
                {`Budget`}
            </ChartText>
        ))
        const WantLabelValue = (({ x, y }) => (
            <ChartText
                stroke={Constant.color.gray}
                fontSize={Constant.fontSize.mini}
                // fontWeight="bold"
                x={x(2.5)}
                y={y(wantChartData.budgetLineValue) + 14}
                textAnchor="middle"
            >
                {`$${wantChartData.budgetLineValue}`}
            </ChartText>
        ))

        const wantYMax = wantChartData.line1[wantChartData.line1.length - 1] > wantChartData.line2[wantChartData.line2.length - 1] ?
            (wantChartData.line1[wantChartData.line1.length - 1] > wantChartData.budgetLineValue ? wantChartData.line1[wantChartData.line1.length - 1] : wantChartData.budgetLineValue) :
            wantChartData.line2[wantChartData.line2.length - 1] > wantChartData.budgetLineValue ? wantChartData.line2[wantChartData.line2.length - 1] : wantChartData.budgetLineValue

        return (
            <View style={chartView}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <View>
                        <Text style={{ ...summaryTitle, marginTop: 0, fontSize: Constant.fontSize.small }}>{strings('profile.Wants')}</Text>
                        <Text style={{ color: Constant.color.textgray, fontSize: Constant.fontSize.xxsmall, fontFamily:'Lato-Light' }}>{strings('profile.CurrentWants')}</Text>
                    </View>
                    <Image style={{ width: wp(6), height: hp(3) }} resizeMode='contain' source={require('../component/img/infoicon.png')}></Image>

                </View>
                {wantChartData.spentValue > wantChartData.budgetLineValue &&
                    <View style={{ alignSelf: 'flex-end', height: hp(4) }}>
                        <Text style={{ fontSize: Constant.fontSize.mini, fontFamily:'Lato-Bold',color: Constant.color.textgray, fontWeight: 'bold', marginTop: Constant.isX ? hp(0.5) : 0 }}>{`$${wantChartData.spentValue}`}</Text>
                        <Image style={{ width: wp(4), height: hp(2), position: 'absolute', right: -wp(1.5), marginTop: hp(2) }} resizeMode='contain' source={require('../component/img/downarrow.png')}></Image>
                    </View>
                    ||
                    <View style={{ flex: wantChartData.budgetLineValue, flexDirection: 'row', borderRadius: wp(1), marginTop: hp(1)}}>
                        <View onLayout={event => {
                            const layout = event.nativeEvent.layout;
                            this.setState({ spentWidth: layout.width })
                        }} style={{ flex: wantChartData.spentValue, height: hp(4) }}>

                            {spentWidth != null && <Text style={{ fontSize: Constant.fontSize.mini, color: Constant.color.textgray, fontWeight: 'bold', fontFamily:'Lato-Bold',position: 'absolute', left: spentWidth - wp(5), width: wp(15), marginTop: Constant.isX ? hp(0.5) : 0 }}>
                                {`$${wantChartData.spentValue}`}</Text>}
                            {spentWidth != null && <Image style={{ width: wp(4), height: hp(2), position: 'absolute', left: spentWidth - wp(2), marginTop: hp(2) }} resizeMode='contain' source={require('../component/img/downarrow.png')}></Image>}
                       
                        </View>
                        <View style={{ flex: (wantChartData.budgetLineValue - wantChartData.spentValue) }}></View>
                    </View>
                }
                <View style={{ flex: wantChartData.spentValue > wantChartData.budgetLineValue ? (wantChartData.budgetLineValue + wantChartData.spentValue) : wantChartData.budgetLineValue, flexDirection: 'row', height: hp(3), borderRadius: wp(1), overflow: 'hidden', marginVertical: 2 }}>
                    <View style={{ flex: wantChartData.spentValue > wantChartData.budgetLineValue ? wantChartData.budgetLineValue : wantChartData.spentValue, backgroundColor: '#FABE00' }}></View>
                    <View style={{ flex: wantChartData.spentValue > wantChartData.budgetLineValue ? (wantChartData.spentValue - wantChartData.budgetLineValue) : (wantChartData.budgetLineValue - wantChartData.spentValue), backgroundColor: wantChartData.spentValue > wantChartData.budgetLineValue ? '#DC7263' : '#FEF2CB' }}></View>
                </View>
                {wantChartData.spentValue > wantChartData.budgetLineValue &&
                    <View style={{ flex: wantChartData.spentValue, flexDirection: 'row', borderRadius: wp(1), marginTop: 2, marginBottom: hp(2)
                     }}>
                        <View onLayout={event => {
                            const layout = event.nativeEvent.layout;
                            this.setState({ budgetWidth: layout.width })
                        }} style={{ flex: wantChartData.budgetLineValue, marginBottom: hp(3) }}>

                            {budgetWidth != null && <Image style={{ width: wp(4), height: hp(2), position: 'absolute', left: budgetWidth - wp(2) }} resizeMode='contain' source={require('../component/img/uparrow.png')}></Image>}
                            {budgetWidth != null && <Text style={{ color: Constant.color.textgray, fontWeight: 'bold',fontFamily:'Lato-Bold',position: 'absolute', left: budgetWidth - wp(5), width: wp(15), marginTop: hp(1.5) }}>
                                {`$${wantChartData.budgetLineValue}`}</Text>}

                        </View>
                        <View style={{ flex: (wantChartData.spentValue - wantChartData.budgetLineValue) }}></View>
                    </View>
                    ||
                    <View style={{ alignSelf: 'flex-end' }}>
                        <Image style={{ width: wp(4), height: hp(2), position: 'absolute', right: -wp(1.5), marginTop: 2 }} resizeMode='contain' source={require('../component/img/uparrow.png')}></Image>
                        <Text style={{ fontSize: Constant.fontSize.mini, color:Constant.color.textgray, fontWeight: 'bold',fontFamily:'Lato-Bold', marginTop: hp(1.8) }}>{`$${wantChartData.budgetLineValue}`}</Text>
                    </View>
                }

                <LineChart
                    style={{ height: hp(25) }}
                    data={WantChartData}
                    svg={{ stroke: 'rgb(134, 65, 244)' }}
                    contentInset={{ top:20, bottom: 20, right:20 }}
                    yMax={wantYMax}
                >
                    <WantHorizontalLine />
                    <WantDecorator1 />
                    <WantDecorator2 />
                    <WantLabel />
                    <WantLabelValue />
                </LineChart>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: wp(10) }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <View style={{ height: hp(2), width: hp(2), borderRadius: wp(1), backgroundColor: '#FABE00', marginRight: wp(2) }}></View>
                        <Text style={{ color: Constant.color.textgray, fontSize: Constant.fontSize.mini ,fontFamily:'Lato-Regular' }}>{strings('profile.CurrentMonth')}</Text>
                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <View style={{ height: hp(2), width: hp(2), borderRadius: wp(1), backgroundColor: '#AD896E', marginRight: wp(2) }}></View>
                        <Text style={{ color: Constant.color.textgray, fontSize: Constant.fontSize.mini ,fontFamily:'Lato-Regular'}}>{strings('profile.LastMonth')}</Text>
                    </View>
                </View>

                <View style={btnView}>
                <TouchableOpacity onPress={() =>  this.props.navigation.navigate('spendingSummary',{ title: 'Wants',})}>
                    <Text style={{ fontSize: Constant.fontSize.xxsmall, color: Constant.color.textgray, fontWeight: 'bold' ,fontFamily:'Lato-Bold' }}>{strings('profile.WantButtonTitle')}</Text>
                </TouchableOpacity>
                </View>
            </View>
        );
    }
}

export default WantChart;