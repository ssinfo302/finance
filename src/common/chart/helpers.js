import { Dimensions } from 'react-native';

const { width, height } = Dimensions.get('screen');
const screenWidth = Math.min(height, width) - 8;
const size = screenWidth - 16;

const getSizes = (length = 10) => {
  /**
   * The maximum number of items that will fit is supposed to be 10
   * We need to scale the medium and small items as there will be large spacings between them
   * To scale, we are calculating the factor, but the final value might be large such that such that it wont fit inside the screen
   * So, we are taking the minimum value of the space remaining and the result after multiplying with the factor
   */
  const factor = 10 / length;
  const large = Math.floor(size * 0.42);
  const medium = Math.floor(size * 0.32) * factor - 16;
  const small = Math.floor(size * 0.22) * factor - 16;
  return {
    large,
    medium: Math.min(medium, (screenWidth - large - 16) / 2),
    small: Math.min(small, (screenWidth - large - 16) / 2),
  };
};

const getConfig = (items) => {
  if (!items.length) {
    return;
  }

  /**
   * This is the default config for items (items length >= 8)
   * For items >= 8 there are
   * 1 large, 3 medium, rem small items
   */
  let config = {
    largeItem: items[0],
    mediumItems: items.slice(1, 4),
    smallItems: items.slice(4, items.length),
    positionMap: {
      medium: [0, 3, 5],
      small: [1, 2, 4, 6, 7, 8],
    },
  };

  /**
   * For items <= 8, there are
   * 1 large, 2 medium, rem small items
   */
  if (items.length <= 8) {
    config = {
      largeItem: items[0],
      mediumItems: items.slice(1, 3),
      smallItems: items.slice(3, items.length),
      positionMap: {
        medium: [0, 3],
        small: [1, 2, 4, 5, 6],
      },
    };
  }

  // TODO: validate at least 3 or 4 items or arrange accordingly
  return config;
};

export { getSizes, screenWidth, getConfig };
