import { StyleSheet } from 'react-native';

import { screenWidth } from './helpers';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: '#fff',
    textAlign: 'center',
  },
  canvas: {
    width: screenWidth,
    position: 'relative',
    height: screenWidth - 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bubble: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 12,
  },
});

export default styles;
