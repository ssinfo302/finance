import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import { screenWidth, getSizes } from './helpers';
import styles from './styles';

const ViewBubble = ({
  x,
  y,
  fill = '#E5382E',
  r = 50,
  title,
  subtitle,
  onPress,
  index = -1,
  itemsCount, // total item count
}) => {
  const { large } = getSizes(itemsCount);
  const outerItemsCount = itemsCount - 1;
  const angle = (index * (360 / outerItemsCount) * Math.PI) / 180;
  const outerDistance = large / 2 + r / 2 + 8; // + 8 = for spacing
  const offset = (screenWidth - r) / 2;
  const fontSize = Math.min(r / 6, 20); // limit max size to 20

  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={0.85}
      style={[
        styles.bubble,
        {
          width: r,
          height: r,
          left: x || offset + outerDistance * Math.cos(angle),
          top: y || offset + outerDistance * Math.sin(angle),
          borderRadius: r,
          backgroundColor: fill,
        },
      ]}>
      <Text numberOfLines={1} style={[styles.text, { fontSize }]}>
        {title}
      </Text>
      <Text
        numberOfLines={1}
        style={[styles.text, { fontSize: fontSize * 0.8 }]}>
        ${subtitle}
      </Text>
    </TouchableOpacity>
  );
};

export default ViewBubble;
