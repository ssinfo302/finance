const sampleData = [
  {
    Needs: {
      Groceries: {
        value: 1780,
        status: '#E5382E',
        size: 'large',
      },
      Home: {
        value: 388,
        status: 'green',
        size: 'small',
      },
      Utilities: {
        value: 1200,
        status: 'green',
        size: 'large',
      },
      Kids: {
        value: 158,
        status: '#ffbf00',
        size: 'small',
      },
      Loans: {
        value: 250,
        status: '#ffbf00',
        size: 'small',
      },
      Transport: {
        value: 560,
        status: 'green',
        size: 'small',
      },
      Allowance: {
        value: 380,
        status: '#E5382E',
        size: 'small',
      },
    },
  },
];

export const getItems = () => {
  /**
   * Returns an array of objects { title, amount, color } sorted in descending order of amount
   */
  // TODO: make API call and return items
  const needsObj = sampleData[0].Needs;
  const needKeys = Object.keys(needsObj);
  const items = needKeys.map((needKey) => {
    const { value, status } = needsObj[needKey];
    return {
      title: needKey,
      amount: value,
      color: status,
    };
  });
  // TODO: slice 10 items
  const sortedItems = items.sort((a, b) => b.amount - a.amount);
  return sortedItems.slice(0, Math.min(10, sortedItems.length));
};
