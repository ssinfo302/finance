import React from 'react';
import { View, Alert } from 'react-native';
import Bubble from './Bubble';
import { screenWidth, getSizes, getConfig } from './helpers';
import { getItems } from './api';
import styles from './styles';
import { useNavigation } from '@react-navigation/native';

const Chart = () => {
  const items = getItems();
  const { large, medium, small } = getSizes(8);
  const config = getConfig(items);
  const { largeItem, mediumItems, smallItems, positionMap } = config;
  const navigation = useNavigation();
  // TODO: onPress => navigation.navigate('targetScreen');
  return (
    <View style={styles.container}>
      <View style={styles.canvas}>
        <Bubble
          x={screenWidth / 2 - large / 2}
          y={screenWidth / 2 - large / 2}
          r={large}
          fill={largeItem.color}
          onPress={() => navigation.navigate('monthlyComparision',{ title: largeItem.title})}
          title={largeItem.title}
          subtitle={largeItem.amount}
          itemsCount={items.length}
        />
        {mediumItems.map((item, index) => (
          <Bubble
            key={index}
            r={medium}
            fill={item.color}
            title={item.title}
            onPress={() => navigation.navigate('monthlyComparision',{ title: item.title})}
            subtitle={item.amount}
            index={positionMap.medium[index]}
            itemsCount={items.length}
          />
        ))}
        {smallItems.map((item, index) => (
          <Bubble
            key={index}
            r={small}
            onPress={() => navigation.navigate('monthlyComparision',{ title: item.title})}
            fill={item.color}
            title={item.title}
            subtitle={item.amount}
            index={positionMap.small[index]}
            itemsCount={items.length}
          />
        ))}
      </View>
    </View>
  );
};

export default Chart;
