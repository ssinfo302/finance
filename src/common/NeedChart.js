import React from 'react';
import {
    View,
    Text,
    Image
} from 'react-native';
import styles from '../component/styles'
import { wp, hp } from '../helper/responsiveScreen';
import Constant from '../helper/themeHelper';
import { strings } from '../component/translation/i18n';
import { Circle, Line, Text as ChartText } from 'react-native-svg'
import { LineChart } from 'react-native-svg-charts'
import { TouchableOpacity } from 'react-native-gesture-handler';

class NeedChart extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            budgetWidth: null,
            spentWidth: null
        };
    }

    componentDidMount() {

    }

    render() {
        const { summaryTitle, chartView, btnView } = styles;
        const { budgetWidth, spentWidth } = this.state;
        const { needChartData } = this.props;

        const NeedChartData = [
            {
                data: needChartData.line1,
                svg: { stroke: '#F0C19D', strokeWidth: wp(0.8) },
            },
            {
                data: needChartData.line2,
                svg: { stroke: '#2AB98F', strokeWidth: wp(0.8) },
            },
        ]

        const NeedHorizontalLine = (({ y }) => (
            <Line
                key={'zero-axis'}
                x1={'0%'}
                x2={'100%'}
                y1={y(needChartData.budgetLineValue)}
                y2={y(needChartData.budgetLineValue)}
                stroke={'grey'}
                strokeDasharray={[4, 6]}
                strokeWidth={2}
            />
        ))

        const NeedDecorator1 = ({ x, y, data }) => {
            return (
                <Circle
                    // key={ index }
                    cx={x(needChartData.line1.length - 1)}
                    cy={y(needChartData.line1[needChartData.line1.length - 1])}
                    r={4}
                    stroke={'#F0C19D'}
                    fill={'#F0C19D'}
                />
            )
        }

        const NeedDecorator2 = ({ x, y, data }) => {
            return (
                <Circle
                    // key={ index }
                    cx={x(needChartData.line2.length - 1)}
                    cy={y(needChartData.line2[needChartData.line2.length - 1])}
                    r={4}
                    stroke={'#2AB98F'}
                    fill={'#2AB98F'}
                />
            )
        }

        const NeedLabel = (({ x, y }) => (
            <ChartText
                stroke={Constant.color.gray}
                fontSize={Constant.fontSize.mini}
                // fontWeight="bold"
                x={x(2.5)}
                y={y(needChartData.budgetLineValue) - 7}
                textAnchor="middle"
            >
                {`Budget`}
            </ChartText>
        ))
        const NeedLabelValue = (({ x, y }) => (
            <ChartText
                stroke={Constant.color.gray}
                fontSize={Constant.fontSize.mini}
                // fontWeight="bold"
                x={x(2.5)}
                y={y(needChartData.budgetLineValue) + 14}
                textAnchor="middle"
            >
                {`$${needChartData.budgetLineValue}`}
            </ChartText>
        ))

        const needYMax = needChartData.line1[needChartData.line1.length - 1] > needChartData.line2[needChartData.line2.length - 1] ?
            (needChartData.line1[needChartData.line1.length - 1] > needChartData.budgetLineValue ? needChartData.line1[needChartData.line1.length - 1] : needChartData.budgetLineValue) :
            needChartData.line2[needChartData.line2.length - 1] > needChartData.budgetLineValue ? needChartData.line2[needChartData.line2.length - 1] : needChartData.budgetLineValue

        return (
            <View style={chartView}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <View>
                        <Text style={{ ...summaryTitle, marginTop: 0, fontSize: Constant.fontSize.small, }}>{strings('profile.Needs')}</Text>
                        <Text style={{ color: Constant.color.textgray, fontSize: Constant.fontSize.xxsmall,fontFamily:'Lato-Light'}}>{strings('profile.CurrentSpend')}</Text>
                    </View>
                    <Image style={{ width: wp(6), height: hp(3) }} resizeMode='contain' source={require('../component/img/infoicon.png')}></Image>

                </View>

                {needChartData.spentValue > needChartData.budgetLineValue &&
                    <View style={{ alignSelf: 'flex-end', height: hp(4) }}>
                        <Text style={{ fontSize: Constant.fontSize.mini, fontFamily:'Lato-Bold',color: Constant.color.textgray, fontWeight: 'bold', marginTop: Constant.isX ? hp(0.5) : 0 }}>{`$${needChartData.spentValue}`}</Text>
                        <Image style={{ width: wp(4), height: hp(2), position: 'absolute', right: -wp(1.5), marginTop: hp(2) }} resizeMode='contain' source={require('../component/img/downarrow.png')}></Image>
                    </View>
                    ||
                    <View style={{ flex: needChartData.budgetLineValue, flexDirection: 'row', borderRadius: wp(1), marginTop: hp(1) }}>
                        <View onLayout={event => {
                            const layout = event.nativeEvent.layout;
                            this.setState({ spentWidth: layout.width })
                        }} style={{ flex: needChartData.spentValue, height: hp(4) }}>

                            {spentWidth != null && <Text style={{ fontSize: Constant.fontSize.mini, color: Constant.color.textgray, fontWeight: 'bold',fontFamily:'Lato-Bold', position: 'absolute', left: spentWidth - wp(5), width: wp(15), marginTop: Constant.isX ? hp(0.5) : 0 }}>
                                {`$${needChartData.spentValue}`}</Text>}
                            {spentWidth != null && <Image style={{ width: wp(4), height: hp(2), position: 'absolute', left: spentWidth - wp(2), marginTop: hp(2) }} resizeMode='contain' source={require('../component/img/downarrow.png')}></Image>}
                       
                        </View>
                        <View style={{ flex: (needChartData.budgetLineValue - needChartData.spentValue) }}></View>
                    </View>
                }
                <View style={{ flex: needChartData.spentValue > needChartData.budgetLineValue ? needChartData.spentValue : needChartData.budgetLineValue, flexDirection: 'row', height: hp(3), borderRadius: wp(1), overflow: 'hidden', marginVertical: 2 }}>
                    <View style={{ flex: needChartData.spentValue > needChartData.budgetLineValue ? needChartData.budgetLineValue : needChartData.spentValue, backgroundColor: '#2AB98F' }}></View>
                    <View style={{ flex: needChartData.spentValue > needChartData.budgetLineValue ? (needChartData.spentValue - needChartData.budgetLineValue) : (needChartData.budgetLineValue - needChartData.spentValue), backgroundColor: needChartData.spentValue > needChartData.budgetLineValue ? '#DC7263' : '#2AB98F60' }}></View>
                </View>
                {needChartData.spentValue > needChartData.budgetLineValue &&
                    <View style={{ flex: needChartData.spentValue, flexDirection: 'row', borderRadius: wp(1), marginTop: 2, marginBottom: hp(2) }}>
                        <View onLayout={event => {
                            const layout = event.nativeEvent.layout;
                            this.setState({ budgetWidth: layout.width })
                        }} style={{ flex: needChartData.budgetLineValue, marginBottom: hp(3) }}>

                            {budgetWidth != null && <Image style={{ width: wp(4), height: hp(2), position: 'absolute', left: budgetWidth - wp(2) }} resizeMode='contain' source={require('../component/img/uparrow.png')}></Image>}
                            {budgetWidth != null && <Text style={{ fontSize: Constant.fontSize.mini, color: Constant.color.textgray, fontWeight: 'bold',fontFamily:'Lato-Bold',position: 'absolute', left: budgetWidth - wp(5), width: wp(15), marginTop: hp(1.5) }}>
                                {`$${needChartData.budgetLineValue}`}</Text>}

                        </View>
                        <View style={{ flex: (needChartData.spentValue - needChartData.budgetLineValue) }}></View>
                    </View>
                    ||
                    <View style={{ alignSelf: 'flex-end' }}>
                        <Image style={{ width: wp(4), height: hp(2), position: 'absolute', right: -wp(1.5), marginTop: 2}} resizeMode='contain' source={require('../component/img/uparrow.png')}></Image>
                        <Text style={{ fontSize: Constant.fontSize.mini, color:Constant.color.textgray, fontWeight: 'bold',fontFamily:'Lato-Bold', marginTop: hp(1.6) }}>{`$${needChartData.budgetLineValue}`}</Text>
                    </View>
                }
                <LineChart
                    style={{ height: hp(25) }}
                    data={NeedChartData}
                    svg={{ stroke: 'rgb(134, 65, 244)' }}
                    contentInset={{ top: 20, bottom: 20, right: 20 }}
                    yMax={needYMax}
                    xMAx={50000}
                >
                    <NeedHorizontalLine />
                    <NeedDecorator1 />
                    <NeedDecorator2 />
                    <NeedLabel />
                    <NeedLabelValue />
                </LineChart>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: wp(10) }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <View style={{ height: hp(2), width: hp(2), borderRadius: wp(1), backgroundColor: '#2AB98F', marginRight: wp(2) }}></View>
                        <Text style={{ color: Constant.color.textgray, fontSize: Constant.fontSize.mini ,fontFamily:'Lato-Regular'}}>{strings('profile.CurrentMonth')}</Text>
                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <View style={{ height: hp(2), width: hp(2), borderRadius: wp(1), backgroundColor: '#F0C19D', marginRight: wp(2) }}></View>
                        <Text style={{ color: Constant.color.textgray, fontSize: Constant.fontSize.mini,fontFamily:'Lato-Regular' }}>{strings('profile.LastMonth')}</Text>
                    </View>
                </View>

                <View style={btnView}>
                    <TouchableOpacity onPress={() =>  this.props.navigation.navigate('spendingSummary',{ title: 'Needs',})}>
                         <Text style={{ fontSize: Constant.fontSize.xxsmall, color: Constant.color.textgray, fontWeight: 'bold' ,fontFamily:'Lato-Bold'}}>{strings('profile.NeedButtonTitle')}</Text>
                    </TouchableOpacity>
                    
                </View>
            </View>
        );
    }
}

export default NeedChart;