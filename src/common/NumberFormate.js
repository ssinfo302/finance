// 'use strict';
// import React, { Component } from 'react';
// import {
//   AppRegistry,
//   Text,
//   TextInput,
//   View
// } from 'react-native';


// export class NumberFormate extends Component {

//   constructor(props){
//     super(props);
    
//   }

const NumberFormate = {
    
    CommaFormatted: function(amount){
        var delimiter = ","; // replace comma if desired
    var a = amount.split('.',2)
    var d = a[1];
    var i = parseInt(a[0]);
    if(isNaN(i)) { return ''; }
    var minus = '';
    if(i < 0) { minus = '-'; }
    i = Math.abs(i);
    var n = new String(i);
    var a = [];
    while(n.length > 3) {
      var nn = n.substr(n.length-3);
      a.unshift(nn);
      n = n.substr(0,n.length-3);
    }
    if(n.length > 0) { a.unshift(n); }
    n = a.join(delimiter);
    if(d.length < 1 || d[0] == 0 ) { amount = n; }
    else { amount = n + '.' + d; }
    amount = minus + amount;
    return amount;

    },
   
}

export default NumberFormate;


// }