import React from 'react';
import { View, StyleSheet, TouchableOpacity, Image, Platform, Text } from 'react-native';
import Constant from '../helper/themeHelper';
import { wp, hp } from '../helper/responsiveScreen';

export default class InputNumberButton extends React.Component {
    render() {
        const { value, handleOnPress, index } = this.props;
        if (this.props.value == 'x') {
            return (
                <TouchableOpacity style={styles.containter} onPress={() => handleOnPress(value)}>
                    <Image source={require('../component/img/delete.png')} style={{ height: hp(2), width: wp(5), tintColor: 'rgba(98,99,100,1)' }} resizeMode='contain' />
                </TouchableOpacity>
            );
        }
        else if (this.props.value == '+') {
            return (
                <TouchableOpacity style={styles.containter} onPress={() => handleOnPress(value)}>
                    <Text style={{...styles.Calculatortext }}>+</Text>
                    <Text style={{...styles.Calculatortext }}>-</Text>
                </TouchableOpacity>
            );
        }
        else {
            return (
                <TouchableOpacity style={{ ...styles.containter, borderBottomColor: this.props.index == 3 ? 'white' : '#D3D3D3' }} onPress={() => handleOnPress(value)} >
                    <Text style={{...styles.Calculatortext }}>{value}</Text>
                </TouchableOpacity>
            );
        }
    };
};

const styles = StyleSheet.create({
    containter: {
        flex: 1,
        backgroundColor: Constant.color.white,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 0.2,
        borderColor: '#D3D3D3'
    },
    Calculatortext:{
        textAlign:'center',
        color: Constant.color.textgray,
        fontFamily: "Lato-Regular",
        fontWeight: 'normal', 
        fontSize: Constant.fontSize.medium 
      }
});

