import React from 'react';
import {
  View,
} from 'react-native';

import { XAxis, StackedBarChart } from 'react-native-svg-charts'
import { Line } from 'react-native-svg'
import * as scale from 'd3-scale'
import { Text as SVGText } from 'react-native-svg'

import styles from './styles'

const BarChartExample = ({ data, criteria }) => {
  console.log(data)
  if (!data) {
    return null;
  }
  const colors = ['#22D6A1', '#DC7263']
  const keys = ['val', 'over']

  const HiHorizontalLine = ({ y }) => <Line key={'zero-axis'} x1={'0%'} x2={'80%'} y1={y(criteria)} y2={y(criteria)} stroke={'gray'} strokeDasharray={[4, 8]} strokeWidth={2} />

  const HiLabel = ({ x, y, bandwidth, data }) => (
    <SVGText
      x={x(data.length - 1) + 10}
      y={y(criteria) - 10}
      fontSize={14}
      fill={'black'}
      alignmentBaseline={'middle'}
      textAnchor={'middle'}
    >
      {`Budget`}
    </SVGText>
  )

  const HiLabelAmount = ({ x, y, bandwidth, data }) => (
    <SVGText
      x={x(data.length - 1) + 10}
      y={y(criteria) + 10}
      fontSize={14}
      fill={'black'}
      alignmentBaseline={'middle'}
      textAnchor={'middle'}
    >
      {`$ ${String(criteria)}`}
    </SVGText>
  )

  const Label = ({ x, y, bandwidth, data }) => {
    return (
      data.map((value, index) => (
        <SVGText
          key={index}
          x={x(index) + (bandwidth / 2.4)}
          y={y(value.val + value.over) - 15}
          fontSize={14}
          fill={value.over > 0 ? '#DC7263' : 'black'}
          alignmentBaseline={'middle'}
          textAnchor={'middle'}
        >
          {value.total > 0 ? `$ ${value.total}` : ''}
        </SVGText>
      ))
    )
  }


  return (
    <View style={styles.barChartWrap}>
      <View style={{ flex: 1, flexDirection: 'row' }}>
        <StackedBarChart
          style={{ flex: 1, marginLeft: 20 }}
          keys={keys}
          colors={colors}
          data={data}
          yAccessor={({ item }) => item.val}
          showGrid={false}
          contentInset={{ top: 30, bottom: 30 }}
          spacingInner={0.15}
        >
          <HiHorizontalLine />
          <Label />
          <HiLabel />
          <HiLabelAmount />
        </StackedBarChart>
      </View>
      <XAxis
        data={data}
        svg={{
          fill: 'black',
          fontSize: 10,
          fontWeight: 'bold',
          y: 5,
          x: 100
        }}
        scale={scale.scaleBand}
        style={{ marginLeft: 20, height: 100 }}
        formatLabel={(_, index) => data[index].month}
        contentInset={{ left: 0, right: 0 }}
      />
    </View>
  )
}

export default BarChartExample