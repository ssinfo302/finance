import {
  StyleSheet,
  Dimensions
} from 'react-native';

const { height, width } = Dimensions.get('window')

const styles = StyleSheet.create({
  barChartWrap: {
    height: height / 3,
    paddingLeft: 20,
    flexDirection: 'column'
  },
});

export default styles;