import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { wp, hp } from '../helper/responsiveScreen';
import Constant from '../helper/themeHelper';
import { strings } from '../component/translation/i18n';
import InputNumber from './InputNumberButton';
import styles from '../component/styles';

const buttons = [
    [1, 2, 3, 'x'],
    [4, 5, 6, '+'],
    [7, 8, 9, ""],
    ['0', '00', '.', ""]
];

export default class GroceriesCalculator extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            Averagearr: [],
            budgetValue: this.props.budgetValue,
            pulseOrMinus: ""
        }
    }
    componentDidMount() {
        Averagearr = this.props.budgetValue.split('');
        if (Averagearr[0] == "-") {
            this.setState({ pulseOrMinus: 'minus' })
        }
        else {
            this.setState({ pulseOrMinus: 'plus' })
        }
    }

    setBadgetButtonClick = () => {
        this.props.onClose()
        this.props.onSetBudget(this.state.budgetValue)
    }

    renderButtons() {

        let layouts = buttons.map((buttonRows, index) => {
            let rowItem = buttonRows.map((buttonItems, buttonIndex) => {

                return <InputNumber
                    value={buttonItems}
                    index={buttonIndex}
                    handleOnPress={() => {
                        if (buttonItems == 'x') {
                            if (Averagearr.length == 1) {
                                this.setState({ budgetValue: "$0" })
                                this.setState({ pulseOrMinus: "" })
                            }
                            else {
                                Averagearr.splice(-1, 1)
                                var str = Averagearr.toString();
                                let replacestr = str.replace(/[,]/g, '')
                                this.setState({ budgetValue: replacestr })
                                if (Averagearr[Averagearr.length - 1] == ".") {
                                    Averagearr.splice(-1, 1)
                                    var str = Averagearr.toString();
                                    let replacestr = str.replace(/[,]/g, '')
                                    this.setState({ budgetValue: replacestr })
                                }
                                if (this.state.pulseOrMinus != '') {
                                    if (Averagearr.length == 2) {
                                        this.setState({ budgetValue: "$0" })
                                        this.setState({ pulseOrMinus: "" })
                                    }
                                }
                                else {
                                    if (Averagearr.length == 1) {
                                        this.setState({ budgetValue: "$0" })
                                        this.setState({ pulseOrMinus: "" })
                                    }
                                }
                            }
                        }
                        else if (buttonItems == '+') {
                            if (this.state.budgetValue != '$0') {
                                if (this.state.pulseOrMinus == "minus") {

                                    for (let i = 0; i < Averagearr.length; i++) {
                                        if (Averagearr[0] == "-") {
                                            Averagearr.splice(i, 1);
                                        }
                                        if (Averagearr[i] == "$") {
                                            Averagearr.splice(i, 1);
                                        }
                                    }
                                    Averagearr.unshift("$")
                                    this.setState({ pulseOrMinus: "plus" })
                                }
                                else {

                                    for (let i = 0; i < Averagearr.length; i++) {

                                        if (Averagearr[0] == "$") {
                                            Averagearr.splice(i, 1);
                                        }
                                    }
                                    if (!Averagearr.includes("-")) {
                                        Averagearr.unshift('-', "$")
                                    }
                                    this.setState({ pulseOrMinus: "minus" })
                                }
                                var str = Averagearr.toString();
                                let replacestr = str.replace(/[,]/g, '')
                                this.setState({ budgetValue: replacestr })
                            }
                        }
                        else if (buttonItems == "") {

                        }
                        else if (buttonItems == ".") {
                            if (!Averagearr.includes(".")) {
                                for (let i = 0; i < Averagearr.length; i++) {
                                    if (Averagearr[1] == undefined) {
                                        Averagearr[1] = "0"
                                    }
                                }
                                Averagearr = Averagearr.concat(buttonItems)

                                var str = Averagearr.toString();
                                let replacestr = str.replace(/[,]/g, '')
                                this.setState({ budgetValue: replacestr })
                            }
                        }
                        else {
                            if (this.state.budgetValue == '$0') {
                                this.setState({ budgetValue: "" })
                            }

                            if (this.state.pulseOrMinus == "") {

                                for (let i = 0; i < Averagearr.length; i++) {
                                    if (Averagearr[0] == "$") {
                                        Averagearr.splice(i, 1);

                                    }
                                    if (Averagearr[0] == "0") {

                                        Averagearr.splice(i, 1);

                                    }
                                    if (Averagearr[i] == "+" || Averagearr[i] == "-") {
                                        Averagearr.splice(i, 1);
                                    }
                                }

                                Averagearr = Averagearr.concat(buttonItems)
                                Averagearr.unshift("$")
                            }
                            else {
                                for (let i = 0; i < Averagearr.length; i++) {
                                    if (Averagearr[1] == "0") {
                                        Averagearr.splice(1, 1);
                                    }
                                }
                                Averagearr = Averagearr.concat(buttonItems)
                            }

                            var str = Averagearr.toString();
                            let replacestr = str.replace(/[,]/g, '')
                            this.setState({ budgetValue: replacestr })
                        }

                    }}
                    key={'btn-' + buttonIndex}
                />
            })
            return <View style={{ flex: 1, flexDirection: 'row' }} key={'row-' + index}>
                {rowItem}
            </View>
        })
        return layouts
    }

    render() {
        const { dialogView, budgetTitle, budgetValue, avgTitle, budgetButton ,budgetBtnText} = styles;
        return (
            <View style={{ flex: 1, backgroundColor: Constant.color.gray, alignItems: 'center', justifyContent: 'center' }}>
                <View style={dialogView}>
                    <TouchableOpacity onPress={() => this.props.onClose()} style={{ flexDirection: 'row', justifyContent: 'flex-end', padding: wp(2) }}>
                        <Image source={require('../component/img/close.png')} resizeMode='contain' style={{ height: hp(1.5), width: wp(4), tintColor: 'rgba(98,99,100,1)', marginRight: 5, marginTop: 5 }} />
                    </TouchableOpacity>

                    <Text style={budgetTitle}>{this.props.title}</Text>
                    <Text style={budgetValue}>{this.state.budgetValue}</Text>

                    <View style={avgTitle}>
                        <Text style={{ fontSize: Constant.fontSize.small, color: Constant.color.darkGray }}>{`${strings('profile.Average')}$${this.props.avgValue}`}</Text>
                    </View>
                    <View style={{ flexGrow: 1, backgroundColor: '#F9FAFB' }}>
                        {this.renderButtons()}
                    </View>
                    <TouchableOpacity style={budgetButton} onPress={this.setBadgetButtonClick}>
                        <Text style={{...budgetBtnText  }}>{strings('profile.SetBudget')}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
};

