import React from 'react';
import {
    View,
    Text,
    Image
} from 'react-native';
import styles from '../component/styles'
import { wp, hp } from '../helper/responsiveScreen';
import Constant from '../helper/themeHelper';
import { strings } from '../component/translation/i18n';
import { Circle, Line, Text as ChartText } from 'react-native-svg'
import { LineChart } from 'react-native-svg-charts'

class GoalChart extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            budgetWidth: null,
            spentWidth: null
        };
    }

    render() {
        const { summaryTitle, chartView, btnView } = styles;
        const { budgetWidth, spentWidth } = this.state;
        const { goalChartData } = this.props;

        const GoalChartData = [
            {
                data: goalChartData.line1,
                svg: { stroke: '#44A6F2', strokeWidth: wp(0.8) },
            },
            {
                data: goalChartData.line2,
                svg: { stroke: '#C6C6C6', strokeWidth: wp(0.8) },
            },
        ]

        const GoalHorizontalLine = (({ y }) => (
            <Line
                key={'zero-axis'}
                x1={'0%'}
                x2={'100%'}
                y1={y(goalChartData.budgetLineValue)}
                y2={y(goalChartData.budgetLineValue)}
                stroke={'grey'}
                strokeDasharray={[4, 6]}
                strokeWidth={2}
            />
        ))

        const GoalDecorator1 = ({ x, y, data }) => {
            return (
                <Circle
                    // key={ index }
                    cx={x(goalChartData.line1.length - 1)}
                    cy={y(goalChartData.line1[goalChartData.line1.length - 1])}
                    r={4}
                    stroke={'#44A6F2'}
                    fill={'#44A6F2'}
                />
            )
        }

        const GoalDecorator2 = ({ x, y, data }) => {
            return (
                <Circle
                    // key={ index }
                    cx={x(goalChartData.line2.length - 1)}
                    cy={y(goalChartData.line2[goalChartData.line2.length - 1])}
                    r={4}
                    stroke={'#C6C6C6'}
                    fill={'#C6C6C6'}
                />
            )
        }


        const GoalLabel = (({ x, y }) => (
            <ChartText
                stroke={Constant.color.gray}
                fontSize={Constant.fontSize.mini}
                // fontWeight="bold"
                x={x(2.5)}
                y={y(goalChartData.budgetLineValue) - 7}
                textAnchor="middle"
            >
                {`Budget`}
            </ChartText>
        ))
        const GoalLabelValue = (({ x, y }) => (
            <ChartText
                stroke={Constant.color.gray}
                fontSize={Constant.fontSize.mini}
                // fontWeight="bold"
                x={x(2.5)}
                y={y(goalChartData.budgetLineValue) + 14}
                textAnchor="middle"
            >
                {`$${goalChartData.budgetLineValue}`}
            </ChartText>
        ))

        const goalYMax = goalChartData.line1[goalChartData.line1.length - 1] > goalChartData.line2[goalChartData.line2.length - 1] ?
            (goalChartData.line1[goalChartData.line1.length - 1] > goalChartData.budgetLineValue ? goalChartData.line1[goalChartData.line1.length - 1] : goalChartData.budgetLineValue) :
            goalChartData.line2[goalChartData.line2.length - 1] > goalChartData.budgetLineValue ? goalChartData.line2[goalChartData.line2.length - 1] : goalChartData.budgetLineValue

        return (
            <View style={chartView}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <View>
                        <Text style={{ ...summaryTitle, marginTop: 0, fontSize: Constant.fontSize.small }}>{strings('profile.Goals')}</Text>
                        <Text style={{ color: Constant.color.textgray, fontSize: Constant.fontSize.xxsmall, fontFamily:'Lato-Light' }}>{strings('profile.CurrentSpend')}</Text>
                    </View>
                    <Image style={{ width: wp(6), height: hp(3) }} resizeMode='contain' source={require('../component/img/infoicon.png')}></Image>

                </View>

                {goalChartData.spentValue > goalChartData.budgetLineValue &&
                    <View style={{ alignSelf: 'flex-end', height: hp(4) }}>
                        <Text style={{ fontSize: Constant.fontSize.mini, color: Constant.color.textgray, fontWeight: 'bold',fontFamily:'Lato-Bold', marginTop: Constant.isX ? hp(0.5) : 0 }}>{`$${goalChartData.spentValue}`}</Text>
                        <Image style={{ width: wp(4), height: hp(2), position: 'absolute', right: -wp(1.5), marginTop: hp(2) }} resizeMode='contain' source={require('../component/img/downarrow.png')}></Image>
                    </View>
                    ||
                    <View style={{ flex: goalChartData.budgetLineValue, flexDirection: 'row', borderRadius: wp(1), marginTop: hp(1)}}>
                        <View onLayout={event => {
                            const layout = event.nativeEvent.layout;
                            this.setState({ spentWidth: layout.width })
                        }} style={{ flex: goalChartData.spentValue, height: hp(4) }}>

                            {spentWidth != null && <Text style={{ fontSize: Constant.fontSize.mini, color: Constant.color.textgray, fontWeight: 'bold',fontFamily:'Lato-Bold', position: 'absolute', left: spentWidth - wp(5), width: wp(15), marginTop: Constant.isX ? hp(0.5) : 0 }}>
                                {`$${goalChartData.spentValue}`}</Text>}
                            {spentWidth != null && <Image style={{ width: wp(4), height: hp(2), position: 'absolute', left: spentWidth - wp(2), marginTop: hp(2) }} resizeMode='contain' source={require('../component/img/downarrow.png')}></Image>}
                       
                        </View>
                        <View style={{ flex: (goalChartData.budgetLineValue - goalChartData.spentValue) }}></View>
                    </View>
                }
                <View style={{ flex: goalChartData.spentValue > goalChartData.budgetLineValue ? (goalChartData.budgetLineValue + goalChartData.spentValue) : goalChartData.budgetLineValue, flexDirection: 'row', height: hp(3), borderRadius: wp(1), overflow: 'hidden', marginVertical: 2 }}>
                    <View style={{ flex: goalChartData.spentValue > goalChartData.budgetLineValue ? goalChartData.budgetLineValue : goalChartData.spentValue, backgroundColor: '#44A6F2' }}></View>
                    <View style={{ flex: goalChartData.spentValue > goalChartData.budgetLineValue ? (goalChartData.spentValue - goalChartData.budgetLineValue) : (goalChartData.budgetLineValue - goalChartData.spentValue), backgroundColor: goalChartData.spentValue > goalChartData.budgetLineValue ? '#2AB98F60' : '#D9EDFD' }}></View>
                </View>
                {goalChartData.spentValue > goalChartData.budgetLineValue &&
                    <View style={{ flex: goalChartData.spentValue, flexDirection: 'row', borderRadius: wp(1), marginTop: 2, marginBottom: hp(2) }}>
                        <View onLayout={event => {
                            const layout = event.nativeEvent.layout;
                            this.setState({ budgetWidth: layout.width })
                        }} style={{ flex: goalChartData.budgetLineValue, marginBottom: hp(3) }}>

                            {budgetWidth != null && <Image style={{ width: wp(4), height: hp(2), position: 'absolute', left: budgetWidth - wp(2) }} resizeMode='contain' source={require('../component/img/uparrow.png')}></Image>}
                            {budgetWidth != null && <Text style={{ fontSize: Constant.fontSize.mini, color: Constant.color.textgray, fontWeight: 'bold',fontFamily:'Lato-Bold', position: 'absolute', left: budgetWidth - wp(5), width: wp(15), marginTop: hp(1.5) }}>
                                {`$${goalChartData.budgetLineValue}`}</Text>}

                        </View>
                        <View style={{ flex: (goalChartData.spentValue - goalChartData.budgetLineValue) }}></View>
                    </View>
                    ||
                    <View style={{ alignSelf: 'flex-end' }}>
                        <Image style={{ width: wp(4), height: hp(2), position: 'absolute', right: -wp(1.5), marginTop: 2 }} resizeMode='contain' source={require('../component/img/uparrow.png')}></Image>
                        <Text style={{ fontSize: Constant.fontSize.mini, color: Constant.color.textgray, fontWeight: 'bold',fontFamily:'Lato-Bold', marginTop: hp(1.8) }}>{`$${goalChartData.budgetLineValue}`}</Text>
                    </View>
                }

                <LineChart
                    style={{ height: hp(25) }}
                    data={GoalChartData}
                    svg={{ stroke: 'rgb(134, 65, 244)' }}
                    contentInset={{ top: 20, bottom: 20, right: 20 }}
                    yMax={goalYMax}
                >
                    <GoalHorizontalLine />
                    <GoalDecorator1 />
                    <GoalDecorator2 />
                    <GoalLabel />
                    <GoalLabelValue />
                </LineChart>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: wp(10) }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <View style={{ height: hp(2), width: hp(2), borderRadius: wp(1), backgroundColor: '#44A6F2', marginRight: wp(2) }}></View>
                        <Text style={{  color: Constant.color.textgray, fontSize: Constant.fontSize.mini,fontFamily:'Lato-Regular' }}>{strings('profile.CurrentMonth')}</Text>
                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <View style={{ height: hp(2), width: hp(2), borderRadius: wp(1), backgroundColor: '#C6C6C6', marginRight: wp(2) }}></View>
                        <Text style={{  color: Constant.color.textgray, fontSize: Constant.fontSize.mini,fontFamily:'Lato-Regular'  }}>{strings('profile.LastMonth')}</Text>
                    </View>
                </View>

                <View style={btnView}>
                    <Text style={{ fontSize: Constant.fontSize.xxsmall, color: Constant.color.textgray, fontWeight: 'bold' ,fontFamily:'Lato-Bold' }}>{strings('profile.GoalButtonTitle')}</Text>
                </View>
            </View>
        );
    }
}

export default GoalChart;