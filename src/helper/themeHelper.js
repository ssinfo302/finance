import { Platform, Dimensions, PixelRatio } from 'react-native';

const {
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
} = Dimensions.get('window');

const isIOS = (Platform.OS === 'ios');
const d = Dimensions.get("window")
const isiPAD = ((SCREEN_HEIGHT/SCREEN_WIDTH) < 1.6)

// based on iphone 5s's scale
const scale = SCREEN_WIDTH / 375;

export function normalize(size) {
    const newSize = size * scale;
    if (Platform.OS === 'ios') {
        return Math.round(PixelRatio.roundToNearestPixel(newSize))
    } else {
        return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
    }
}
module.exports = {
    //API Constant
    color:{
        background: '#ffffff',
       

        yellow: '#FFAA1D',
        green: '#2AB98F',
        red: '#D15A58',
        
        blue: '#44A6F2',
        darkBlue: '#093C72',
        lightBlue: '#2D80D7',
        skyBlue: '#0AB6F3',
        sky: '#44A6F2',
        lightSky: '#E3F0FF',
        darkGray:'#414141',
        gray: '#A4A7A8',
        lightGray: '#F7F7F7',
       
        black: '#02152a',
        white: '#ffffff',
     
        
        transparent:'transparent',
        transparentWhite: '#FFFFFF80',

        sepratorColor: '#E3F0FF',

        textgray:'#414141',
        sepratorColor:'#EAEDF2',
        customWhite: '#F4F6FA',
        selectedtextColor:'#236EEA',
        lightgrayText:'#A2A2A2',
        shadowColor:'#707070',
        offerText:'#313131',
        destext:'#555555',
        ShortListTextColor: '#D54C7C',
        ApplyBtnColor: '#4E6BF4',
        creditCardShadowColor:"#ECEFF7"
        

    },
    style:{
        container:{width:SCREEN_WIDTH*0.85,alignSelf:'center'}
    },
    screen: Dimensions.get('window'),
    isIOS: isIOS,
    isANDROID: Platform.OS === 'android',
    isiPAD: isiPAD,
    isX : Platform.OS === "ios" && (d.height > 800 || d.width > 800) ? true : false,

    screenHeight:  isIOS && SCREEN_HEIGHT || SCREEN_HEIGHT - 24,
    screenWidth:  SCREEN_WIDTH,
    fullScreenHeight:  SCREEN_HEIGHT,

    fontSize:{
        xxmini: normalize(8),
        xmini: normalize(10),
        mini: normalize(13),
        xxxsmall: normalize(12),
        xxsmall: normalize(14),
        xsmall: normalize(15),
        small: normalize(16),
        xmedium: normalize(18),
        medium: normalize(20),
        large: normalize(22),
        xlarge: normalize(24),
        xxlarge: normalize(28),
        xxxlarge: normalize(30),
    },


    shadowStyle:{
        shadowColor: 'rgba(0,0,0,0.14)',
        shadowOffset: {width: 1, height: 2},
        shadowOpacity: 1,
        shadowRadius: 5,
        elevation: 2,
        zIndex: 10000000
    },

    gradientColours: {
        green: ['transparent', '#245250'],
        darkGreen: ['transparent', '#0D4354'],
        blue: ['transparent', '#305489'],
        lightBlue: ['transparent', '#2D80D7'],
        darkBlue: ['transparent', '#093C72'],
    },

    loaderBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        backgroundColor: 'rgba(52, 52, 52, 0.5)'
    }
};
