import {
  StyleSheet
} from 'react-native';
import Constant from '../helper/themeHelper';
import { wp, hp } from '../helper/responsiveScreen';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Constant.color.background,
  },
  profileTitle: {
    color: Constant.color.white,
    fontSize: Constant.fontSize.xmedium,
    position: 'absolute',
    top: Constant.isIOS ? hp(8) : hp(6),
    alignSelf: 'center',
    fontWeight: 'bold',
    fontFamily: "Lato-Bold" 
  },
  detailView: {
    ...Constant.shadowStyle,
    backgroundColor: Constant.color.white,
    padding: hp(2),
    width: wp(85),
    alignSelf: 'center',
    // marginHorizontal: wp(8),
    borderRadius: wp(3),
    position: 'absolute',
    top: Constant.isIOS ? hp(19) : hp(17)
  },
  title: {
    color: Constant.color.sky,
    fontSize: Constant.fontSize.xmedium,
    alignSelf: 'center',
    fontWeight: 'bold',
    fontFamily: "Lato-Bold" ,
    marginTop: hp(1)
  },
  value: {
    color: Constant.color.gray,
    fontSize: Constant.fontSize.xxsmall,
    textAlign:'center',
    alignSelf: 'center',
    fontWeight: 'normal',
    fontFamily: "Lato-Regular",
    marginTop: hp(1)
  },
  valueTitle: {
    textAlign:'center',
    color: Constant.color.gray,
    fontSize: Constant.fontSize.xxsmall,
    fontWeight: 'bold',
    fontFamily: "Lato-Bold" 
  },
  valueView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: hp(2.5),
    paddingHorizontal: wp(3),
    paddingVertical: hp(0.5)
  },
  seprator: {
    width: 1,
    height: '80%',
    backgroundColor: '#E8E8E8',
  },
  summaryTitle: {
    color: Constant.color.textgray,
    fontSize: Constant.fontSize.xmedium,
    marginTop:Constant.isIOS ? Constant.isX ? hp(22) : hp(25) : hp(23),
    fontWeight: 'bold',
    fontFamily: "Lato-Bold" 
  },
  chartView: {
    ...Constant.shadowStyle,
    backgroundColor: Constant.color.white,
    padding: hp(2),
    borderRadius: wp(2),
    marginTop: hp(3)
  },
  btnView: {
    backgroundColor: '#E2F1FC',
    padding: hp(2.5),
    borderRadius: hp(5),
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: hp(2)
  },
  headerView: {
    paddingTop: Constant.isIOS ? hp(5) : hp(2),
    paddingHorizontal: wp(4),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#FAFAFA'
  },
  headerTitle: {
    color: Constant.color.black,
    fontSize: Constant.fontSize.medium,
   fontWeight: 'bold',
    marginLeft: wp(4),
    fontFamily: "Lato-Bold" 
  },
  cardView: {
    ...Constant.shadowStyle,
    paddingHorizontal: hp(2),
    borderRadius: wp(2),
    marginVertical: hp(0.8),
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: Constant.isX ? hp(2.3) : hp(2.7),
    flex: 1
  },
  listTitle: {
    color: Constant.color.white,
    fontSize: Constant.fontSize.xmedium,
     fontWeight: 'bold',
    fontFamily: "Lato-Bold" 
  },
  listSubTitle: {
    color: Constant.color.gray,
    fontSize: Constant.fontSize.xmini,
    fontWeight: 'bold',
    fontFamily: "Lato-Bold"
  },
  nextButton: {
    flex: 0.1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    borderLeftColor: Constant.color.white,
    borderLeftWidth: 1,
    height: Constant.isX ? hp(8) : hp(9)
  },
  listView: {
    ...Constant.shadowStyle,
    paddingHorizontal: hp(2),
    borderRadius: wp(2),
    marginVertical: hp(1.5),
    backgroundColor: Constant.color.white,
    borderLeftWidth: wp(1),
    borderLeftColor: Constant.color.green,
    paddingHorizontal: hp(1.5)
  },
  dropdownContainerStyle: {
    paddingLeft: wp(4),
    //backgroundColor: Constant.color.white,
    height: hp(5),
    width: wp(25),
    borderRadius: wp(2),
    justifyContent: 'center',
  },
  dropDownStyle: {
    ...Constant.shadowStyle,
    backgroundColor: Constant.color.white,
    // borderRadius: wp(2),
    borderBottomLeftRadius: wp(2),
    borderBottomRightRadius: wp(2),
    // overflow: 'hidden',
    position: 'absolute',
    alignItems: 'center',
    top: 10,
    width: wp(23),
    height: Constant.isX ? hp(11) : hp(14)
  },
  dropdownFontStyle: {
    marginLeft: -wp(1),
    fontSize: Constant.fontSize.xmedium,
    color: Constant.color.textgray,
    fontWeight: 'normal',
    fontFamily: "Lato-Regular" 
  },
  topView: {
    paddingVertical: hp(3),
    backgroundColor: '#FAFAFA',
    borderBottomRightRadius: 100,
    borderBottomLeftRadius: 100,
    height: Constant.isIOS ? (Constant.isX ? hp(35) : hp(40)) : hp(33),
  },
  dialogView: {
    borderRadius: 12,
    backgroundColor: Constant.color.white,
    height: Constant.isX ? hp(65) : hp(75),
    width: wp(85),
    overflow: 'hidden'
  },
  budgetTitle: {
    fontSize: Constant.fontSize.large,
    color: Constant.color.darkGray,
    alignSelf: 'center',
    fontFamily: "Lato-Light"
  },
  budgetValue: {
    fontSize: Constant.fontSize.xxxlarge,
    color: Constant.color.green,
    alignSelf: 'center',
    marginTop: hp(1),
    fontWeight: 'normal',
    fontFamily: "Lato-Regular" 
  },
  avgTitle:{
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F9FAFB',
    marginTop: hp(2),
    padding: hp(2),
    color: Constant.color.textgray,
    fontSize: Constant.fontSize.small,
    fontWeight: 'normal',
    fontFamily: "Lato-Regular"
  },
  budgetButton:{
    backgroundColor: Constant.color.blue,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: hp(2)
  },
  budgetBtnText:
  {
    color: Constant.color.white, 
    fontSize: Constant.fontSize.xmedium,
    fontWeight: 'normal',
    fontFamily: "Lato-Regular"
  },
  successButton:{
    backgroundColor: Constant.color.blue,
    alignSelf:'center',
    alignItems: 'center',
    justifyContent: 'center',
    width: wp(50),
    padding: wp(2),
    borderRadius: wp(2),
    marginTop: hp(3)
  },
  WayHeaderView:{
    height: (Platform.OS) == 'ios' ? hp(11): hp(8),
    flexDirection:'row',
    paddingTop:(Platform.OS) == 'ios' ? hp(5):hp(2),
    justifyContent:'space-between'
  },
  wayNavigationItem:{
    height: 35,
    width:35,
    alignItems:'center',
    justifyContent:'center'
  },
  WayRightNavigationItem:{
    height: 30,
    width:30, 
    marginTop:5,
  },
  wayUserNameTitle:{
    color: Constant.color.black, 
    fontSize: Constant.fontSize.large,
    fontWeight: 'bold',
    fontFamily: "Lato-Bold",
    marginTop: 10
  },
  waysubtitle:{
    color: Constant.color.textgray,
     fontSize: Constant.fontSize.xsmall,
     fontWeight: 'normal',
    fontFamily: "Lato-Regular" ,
     paddingTop: 5
  },
  wayTopListView:{
    backgroundColor: Constant.color.white,
    padding:hp(1)
  },
  wayListTitleText:{
    paddingVertical:hp(1.5),
       textAlign:'center',
      fontSize: Constant.fontSize.xxsmall,
      fontWeight: 'bold',
      fontFamily: "Lato-Bold" ,
  },
  wayDesView:{
    height: (Platform.OS) == 'ios' ? Constant.isX ? hp(6) : hp(7) : hp(5),
    paddingHorizontal:wp(2.5),
    paddingVertical:wp(2),
    // marginTop: hp(0.5), 
     alignItems:'center', 
     justifyContent:'center'
  },
  wayListDestext:{
    textAlign:'center',
    color: Constant.color.textgray,
    fontSize: Constant.fontSize.xxxsmall,
    fontWeight: 'normal',
    fontFamily: "Lato-Regular"  
  },
  wayTopListSubView:{
    width:wp(37),
    borderRadius:15,
    borderWidth: 0.5,
    backgroundColor: Constant.color.white, 
    shadowOffset: { width: 0,height: 0,},
    shadowOpacity: 0.30,
    shadowRadius: 3.0,
    elevation: 3,
        zIndex: 10000000
  },
  sliderbtn:{
    marginLeft: 15,
    paddingTop:5,
    borderBottomWidth:2.5,
    alignItems:'center',
    justifyContent:'center'
  },
  slidertext:{
    textAlign:'center',fontSize: 17,padding: 10, fontWeight: 'normal',
    fontFamily: "Lato-Regular"
  },
  waysliderView:{
    height: 50,
    borderBottomColor: Constant.color.sepratorColor,
    borderBottomWidth:1,
    flexDirection:'row',
   
  },
  LetsLooktext:{
    color: Constant.color.textgray,
     fontSize: Constant.fontSize.xsmall,
     fontWeight: 'normal',
    fontFamily: "Lato-Regular" ,
     paddingTop: 15,
     paddingBottom: 15,
     textAlign:'center'
  },
  FinanceListSpaceing:{
    justifyContent:'space-between',
    paddingLeft: wp(5),
    paddingBottom: wp(0),
    paddingRight:wp(5) 
  },
  MarchatListSpaceing:{
    justifyContent:'space-between',
    paddingLeft: wp(10),
    paddingBottom: wp(6),
    paddingRight:wp(10) 
  },
 
  FinaceListView:{
    borderRadius:10,
    backgroundColor: Constant.color.white, 
    shadowColor:Constant.color.shadowColor, 
    ...Constant.shadowStyle,
    shadowOffset: {width: 1, height: 5},
    margin: hp(1.8)
  },
  merchantListView:{
    width:wp(37),
    borderRadius:10,
    backgroundColor: Constant.color.white, 
    ...Constant.shadowStyle,
    shadowOffset: {width: 1, height: 5}
  },
  financeListTitletext:{
    padding:10,
    textAlign:'center',
    color: Constant.color.lightgrayText,
    fontSize: Constant.fontSize.xxsmall,
    fontWeight: 'bold',
    fontFamily: "Lato-Bold" 
  },
  financeOfferText:{
    paddingTop: 10,
    textAlign:'center',
    color: Constant.color.offerText,
    fontSize: Constant.fontSize.xxsmall,
    fontWeight: 'bold',
    fontFamily: "Lato-Bold" 
  },
  merchantOffertext:{
    paddingTop: 10,
    paddingBottom: 5,
    textAlign:'center',
    color: Constant.color.offerText,
    fontSize: Constant.fontSize.mini,
    fontWeight: 'bold',
    fontFamily: "Lato-Bold" 
  },
  DescriptionText:{
    textAlign:'center',
    color: Constant.color.destext,
    fontSize: 12,
    fontWeight: 'normal',
    fontFamily: "Lato-Regular" 
  },
  

});

export default styles;