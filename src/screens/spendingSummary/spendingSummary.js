
import React, { Component } from 'react'
import Constant from '../../helper/themeHelper';
import { View, Text, Image, FlatList, ScrollView,SectionList } from 'react-native'
import { wp, hp } from '../../helper/responsiveScreen';
import styles from '../../component/styles';
import summaryStyle from '../spendingSummary/styles';
import { strings } from '../../component/translation/i18n';
import { TouchableOpacity, TouchableNativeFeedback } from 'react-native-gesture-handler'
import Chart from '../../common/chart/index';
import ProgressBar from '../../common/ProgressBar';
import sampleData from '../../common/chart/api';


export default class spendingSummary extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
        Title: this.props.route.params.title,
        summaryData: [{ title: 'Food', image: require('./img/foodicon.png'), value: 24, amount: 280, percentage: 23, color: '#FF6232', progress: 75 },
        { title: 'Transport', image: require('./img/transporticon.png'), value: 5, amount: 240, percentage: 21, color: '#32D6FF', progress: 20 },
        { title: 'Entertainment', image: require('./img/televisionicon.png'), value: 2, amount: 260, percentage: 42, color: '#FFB300', progress: 90 }],
       
    }
  }

  //Back button click event
  handleBackButtonClick() {
    this.props.navigation.goBack()
    return true;
  };

  renderRow = ({ index, item }) => {
    console.log("row item...", JSON.stringify(item))
    const { listTitle, listSubTitle } = styles;
   const { SummaryListContentView,summaryDetailView,summaryDetailTopView,percentageText,amountText} = summaryStyle;
   
    return (
      <View style={{...SummaryListContentView}}>
        <View style={{...summaryDetailView}}>
          <View style={{...summaryDetailTopView}}>
            <Image style={{ width: wp(8), height: hp(5) }} resizeMode='contain' source={item.image}></Image>
            <View style={{ marginLeft: wp(3) }}>
              <Text style={{...listTitle,color: "#1B1B1B"}}>{item.title}</Text>
              <Text style={listSubTitle}>{`${item.value} transactions`}</Text>
            </View>
          </View>
          <View style={{ marginLeft: wp(3) }}>
            <Text style={{...amountText,  color: item.color}}>{`$${item.amount}`}</Text>
            <Text style={{...percentageText}}>{`${item.percentage}%`}</Text>
          </View>
        </View>
        <View style={{alignSelf:'center'}}>
          <ProgressBar
          
            height={hp(0.8)}
            width={wp(80)}
            value={item.progress}
            backgroundColor={item.color}
            underlyingColor={`${item.color}30`}
          />
        </View>
      </View>
    )
  };


  
  render() {
    const { WayHeaderView, container, wayNavigationItem, headerTitle } = styles;
    const {  summaryData,Title } = this.state;
    return (

      <View style={container} >
        {/*header view*/}
        <View style={WayHeaderView}>
          <View style={{ flexDirection: 'row', paddingLeft: 10 }}>
            <TouchableOpacity style={wayNavigationItem} onPress={() => this.handleBackButtonClick()}>
              <Image resizeMode='contain' source={require('../../component/img/backicon.png')} style={{ width: wp(5), height: hp(2.2) }} />
            </TouchableOpacity>
            <Text style={{ ...headerTitle, padding: 5 }}>{strings('profile.SummaryTitle')}</Text>
          </View>
        </View>
        <ScrollView style={container} showsVerticalScrollIndicator={false} contentContainerStyle={{ paddingBottom: hp(2) }}>
         <Text style={{  marginTop: hp(1),fontSize: Constant.fontSize.medium, fontWeight: 'normal', fontFamily: "Lato-Regular",textAlign:'center'}}>{Title}</Text>
         <Chart navigation={this.props.navigation}></Chart>
        
        <FlatList
                 contentContainerStyle={{ paddingVertical: hp(1) }}
                 style={{ paddingHorizontal: wp(3) }}
            showsVerticalScrollIndicator={false}
            data={summaryData}
            renderItem={this.renderRow}
            keyExtractor={(item, index) => {
              return index + "";
            }}
           
          />
        </ScrollView>
    </View>
    )
  };

};