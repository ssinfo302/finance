import {
    StyleSheet
  } from 'react-native';
  import Constant from '../../helper/themeHelper';
  import { wp, hp } from '../../helper/responsiveScreen';

  const styles = StyleSheet.create({
   
    SummaryListContentView:{
        flexDirection:'column',
        marginHorizontal: wp(3),
        paddingHorizontal: wp(3),
        paddingVertical: hp(2),
        ...Constant.shadowStyle,
        shadowOffset:{width: 0 ,height: 3},
        shadowColor: "rgba(0,192,253,0.17)",
        borderRadius: 10,
        backgroundColor: Constant.color.white,
        marginVertical: hp(1)
    },
    summaryDetailView:{
        flexDirection: 'row', 
        justifyContent: 'space-between',
         alignItems: 'center',
          marginBottom: hp(2)
    },
    summaryDetailTopView:{
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center' 
    },
    amountText:{
         fontSize: Constant.fontSize.xmedium, 
         fontWeight: 'normal',
         fontFamily: 'Lato-Regular',
         textAlign: 'right' 
    },
    percentageText:{
        color: Constant.color.textgray, 
        fontSize: Constant.fontSize.xxxsmall,
        fontWeight: 'normal',
        fontFamily: 'Lato-Regular', 
        textAlign: 'right'
    }
});

export default styles;