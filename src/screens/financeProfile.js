/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {
  ScrollView,
  View,
  Text,
  Image,
  processColor,
  FlatList 
} from 'react-native';
import styles from '../component/styles'
import { wp, hp } from '../helper/responsiveScreen';
import Constant from '../helper/themeHelper';
import { strings } from '../component/translation/i18n';
import NeedChart from '../common/NeedChart';
import WantChart from '../common/WantChart';
import GoalChart from '../common/GoalChart';

class FinanceProfile extends Component {

  constructor(props) {
    super(props);

    this.state = {
      needChartData: {
        line1: [0, 43, 90, 90, 133, 200, 205, 289, 350],
        line2: [0, 120],
        budgetLineValue: 300,
        spentValue: 400
      },
      wantChartData: {
        line1: [0, 200, 400, 800, 900, 1100, 1200, 1300, 1350, 1400],
        line2: [0, 100, 200, 400, 450, 600, 650, 800, 900, 950, 1100, 1200, 1200, 1300, 1400, 1400, 1400, 1400],
        budgetLineValue: 400,
        spentValue: 300
      },
      goalChartData: {
        line1: [0, 200, 275, 325, 300, 500, 550, 600, 850],
        line2: [0, 100, 200, 500, 500, 400, 650, 650, 750, 850, 950, 1085, 1085, 1085, 1085, 1085, 1085],
        budgetLineValue: 400,
        spentValue: 300
      },
      userData: {
        name: 'Ashley',
        decscription: '25 years old, Married have 1 dependent (s)',
        income: '$5,000/m',
        expense: '$4,750/m',
        savings: '$250/m'
      }
    }
  }

  render() {
    const { container, profileTitle, detailView, title, value, valueTitle, valueView, seprator, summaryTitle, chartView, btnView, headerView, headerTitle, titleText } = styles;
    const { userData, needChartData, wantChartData, goalChartData, summaryData } = this.state;
    
    return (

      <ScrollView style={container} showsVerticalScrollIndicator={false} contentContainerStyle={{ paddingBottom: hp(2) }}>

        <View>
          <Image style={{ width: wp(100), height: Constant.isIOS ? hp(23) : hp(20) }} resizeMode='stretch' source={require('../component/img/background.png')}></Image>
          <Text style={profileTitle}>{strings('profile.Title')}</Text>

          <View style={detailView}>
            <Image style={{ width: hp(11), height: hp(11), position: 'absolute', alignSelf: 'center', top: -hp(5) }} resizeMode='contain' source={require('../component/img/profile.png')}></Image>
            <Image style={{ width: wp(8), height: hp(4.5), alignSelf: 'flex-end' }} resizeMode='contain' source={require('../component/img/editicon.png')}></Image>
            <Text style={title}>{userData.name}</Text>
            <Text style={value}>{userData.decscription}</Text>
            <View style={valueView}>
              <View>
                <Text style={valueTitle}>{strings('profile.Income')}</Text>
                <Text style={{ ...value, marginTop: hp(0.5) }}>{userData.income}</Text>
              </View>
              <View style={seprator} />
              <View>
                <Text style={valueTitle}>{strings('profile.Expense')}</Text>
                <Text style={{ ...value, marginTop: hp(0.5) }}>{userData.expense}</Text>
              </View>
              <View style={seprator} />
              <View>
                <Text style={valueTitle}>{strings('profile.Savings')}</Text>
                <Text style={{ ...value, marginTop: hp(0.5) }}>{userData.savings}</Text>
              </View>
            </View>
          </View>
          <View style={{ paddingHorizontal: wp(8) }}>
            <Text style={summaryTitle}>{strings('profile.SummaryTitle')}</Text>

            <NeedChart needChartData={needChartData}  navigation={this.props.navigation}/>
            <WantChart wantChartData={wantChartData} navigation={this.props.navigation} />
            <GoalChart goalChartData={goalChartData}  navigation={this.props.navigation}/>

          </View>
        </View>
      </ScrollView>
    );
  }
}

export default FinanceProfile;
