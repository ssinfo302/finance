/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
  Modal 
} from 'react-native';
import styles from '../component/styles'
import { wp, hp } from '../helper/responsiveScreen';
import Constant from '../helper/themeHelper';
import GroceriesCalculator from '../common/GroceriesCalculator';
import LottieView from 'lottie-react-native';
import { strings } from '../component/translation/i18n';

class BudgetList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      Title: this.props.route.params.title,
      ListData: [
        { title: 'Groceries', image: require('../component/img/groceries.png'), budgetValue: '$120', avgValue: 150 },
        { title: 'Utilities & Bills', image: require('../component/img/utilities.png'), budgetValue: '$300', avgValue: 300 },
        { title: 'Transport', image: require('../component/img/transporticon.png'), budgetValue: '$0', avgValue: 80 }
      ],
      calculatorVisibility: false,
      selectedItem: {},
      successVisibility: false
    }
  }

  renderRow = ({ index, item }) => {
    console.log("row item...", JSON.stringify(item))
    const { listView, listTitle, nextButton } = styles;
    return (
      <View style={listView}>
        <View style={{ flex: 1, paddingVertical: hp(2.5), flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
          <View style={{ flexDirection: 'row', alignItems: 'center', flex: 0.7 }}>
            <Image style={{ width: wp(8), height: hp(5) }} resizeMode='contain' source={item.image}></Image>
            <Text style={{ ...listTitle, color: Constant.color.black, marginLeft: wp(4), fontSize: Constant.fontSize.xmedium }}>{item.title}</Text>
          </View>
          <View style={{ flex: 0.3, flexDirection: 'row', justifyContent: 'space-between' }}>
            <View>
              <Text style={{ color: item.budgetValue == '$0' ? Constant.color.gray : Constant.color.green, fontWeight: 'normal', fontFamily: "Lato-Regular" ,fontSize: Constant.fontSize.xmedium, textAlign: 'left' }}>{item.budgetValue == '$0' ? 'Not Set' : `${item.budgetValue}`}</Text>
              <Text style={{ color: Constant.color.gray, fontSize: Constant.fontSize.xxxsmall, textAlign: 'left' ,fontWeight: 'normal', fontFamily: "Lato-Regular"}}>{`Average $${item.avgValue}`}</Text>
            </View>
            <TouchableOpacity onPress={() => this.setState({ calculatorVisibility: true, selectedItem: item, selectedIndex: index })}>
              <Image style={{ width: wp(5), height: hp(2) }} resizeMode='contain' source={require('../component/img/edit.png')}></Image>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  };

  render() {
    const { container, headerView, headerTitle, title, successButton } = styles;
    const { ListData, Title, calculatorVisibility, selectedItem, selectedIndex, successVisibility } = this.state;


    // arc: { outerRadius: '100%', padAngle: 0 },
    return (
      calculatorVisibility &&
      <GroceriesCalculator
        title={selectedItem.title}
        budgetValue={selectedItem.budgetValue}
        avgValue={selectedItem.avgValue}
        onClose={() => this.setState({ calculatorVisibility: false })}
        onSetBudget={(value) => {
          this.state.ListData[selectedIndex].budgetValue = value;
          this.setState({successVisibility: true})
        }} />
      ||
      <View style={container} showsVerticalScrollIndicator={false} contentContainerStyle={{ paddingBottom: hp(2) }}>

        <View style={{...headerView,backgroundColor: Constant.color.white}}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Image style={{ width: wp(5), height: hp(2.2) }} resizeMode='contain' source={require('../component/img/backicon.png')}></Image>
            </TouchableOpacity>
            <Text style={headerTitle}>{Title}</Text>
          </View>
        </View>

        <FlatList
          showsVerticalScrollIndicator={false}
          data={ListData}
          renderItem={this.renderRow}
          keyExtractor={(item, index) => {
            return index + "";
          }}
          contentContainerStyle={{ paddingVertical: hp(1) }}
          style={{ paddingHorizontal: wp(3), marginTop: hp(5) }}
        />

        <Modal
          transparent={true}
          animationType={'none'}
          visible={successVisibility}
          onRequestClose={() => { console.log('close modal') }}>
          <View style={Constant.loaderBackground}>
            <View style={{ width: wp(80), backgroundColor: Constant.color.white, borderRadius: hp(1), padding: hp(2) }}>
              <LottieView autoPlay={true} ref={e => this["checkRef"] = e} source={require('../component/animation/checkmark.json')} loop={false}
                style={{ height: hp(8), width: wp(15), alignSelf: 'center' }} />
              <Text style={{ ...title, color: Constant.color.black }}>{strings('profile.SuccessMessage')}</Text>
              <TouchableOpacity
                onPress={() => this.setState({ successVisibility: false })}
                style={successButton}>
                <Text style={{ ...title, color: Constant.color.white, marginTop: 0 }}>OK</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

export default BudgetList;
