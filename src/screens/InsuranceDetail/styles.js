import {
    StyleSheet
  } from 'react-native';
  import Constant from '../../helper/themeHelper';
  import { wp, hp } from '../../helper/responsiveScreen';

  const styles = StyleSheet.create({
      ItemLabelText: {
        flex: 0.5,
        textAlign:'left',
        color: Constant.color.textgray,  
        paddingTop: hp(1),
        fontSize: Constant.fontSize.xsmall
      },
      b:{
        fontWeight: 'bold', 
        fontFamily: "Lato-Bold"
      },
      Itemtext:{
        flex: 0.5,
        color: Constant.color.textgray,
         fontSize: Constant.fontSize.xsmall, 
         paddingTop: hp(1),
         paddingBottom: hp(1.5),
          textAlign:'left'
      },
      DesTextView:{
        justifyContent: 'center', marginHorizontal: wp(2.5),
      },
   

});

export default styles;