import React, { Component } from 'react'
import Constant from '../../helper/themeHelper';
import { View, Text, Image, FlatList, ScrollView} from 'react-native';
import { WebView } from 'react-native-webview';
import { wp, hp } from '../../helper/responsiveScreen';
import styles from '../../component/styles';
import Creditcardstyles from '../../screens/FeaturedCard/styles';
import insuranceDetailStyle from '../InsuranceDetail/styles';
import insuranceStyle from '../../screens/Insurance/styles';
import creditCardDetailStyles from '../../screens/CreditCardDetail/styles';
import { strings } from '../../component/translation/i18n';
import { TouchableOpacity } from 'react-native-gesture-handler'
import NumberFormate from '../../common/NumberFormate';
import HTMLView from 'react-native-htmlview';

export default class insuranceDetail extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      Index: this.props.route.params.index,
      CardDetail: this.props.route.params.item,
      isdescription: true,
      isaboutPolicy: false,
      ispayout: false,
      isuniqueFeature: false,
      isOptionalRiders: false,
    }
  }

  //Back button click event
  handleBackButtonClick() {
    this.props.navigation.goBack()
    return true;
  };

  handleDescriptionSelection(value) {
    this.setState({ isdescription: value });
    this.setState({ isaboutPolicy: false });
    this.setState({ ispayout: false });
    this.setState({ isuniqueFeature: false });
    this.setState({ isOptionalRiders: false });
  }
  handleAboutPolicySelection(value) {
    this.setState({ isdescription: false });
    this.setState({ isaboutPolicy: value });
    this.setState({ ispayout: false });
    this.setState({ isuniqueFeature: false });
    this.setState({ isOptionalRiders: false });
  }
  handlPayoutFeature(value) {
    this.setState({ isdescription: false });
    this.setState({ isaboutPolicy: false });
    this.setState({ ispayout: value });
    this.setState({ isuniqueFeature: false });
    this.setState({ isOptionalRiders: false });
  }
  handleUniqueFeature(value) {
    this.setState({ isdescription: false });
    this.setState({ isaboutPolicy: false });
    this.setState({ ispayout: false });
    this.setState({ isuniqueFeature: value });
    this.setState({ isOptionalRiders: false });

  }
  handleOptionalRidersFeature(value) {
    this.setState({ isdescription: false });
    this.setState({ isaboutPolicy: false });
    this.setState({ ispayout: false });
    this.setState({ isuniqueFeature: false });
    this.setState({ isOptionalRiders: value });

  }

  //Policyincludes list item
PolicyIncludesListItems =(item,index)=>{
    const {policyincludeText} = insuranceStyle;

    var img = require('../Insurance/img/wheelchair.png');
    if (item == "TPD")
    {
        img = require('../Insurance/img/wheelchair.png');
    }
    else if (item == "WAIVER")
    {
        img = require('../Insurance/img/hand.png');
    }
    else if (item == "CI")
    {
        img = require('../Insurance/img/hiv.png');
    }
    else if (item == "MEDICAL")
    {
        img = require('../Insurance/img/heart-beat.png');
    }
    else if (item == "EARLY")
    {
        img = require('../Insurance/img/early-stage.png');
    }

    return(
         <View style={{marginHorizontal: wp(5)}}>
            <Image source={img} style={{alignSelf:'center',height: 40 ,width: 40,padding: 5}} resizeMode='contain'/>
            <Text style={{...policyincludeText}}>{item}</Text>
        </View>
    )
}

  // key feature list and eligibility fees UI
  KeyFeatureListItem = (item, index) => {
    const {sepratorView} = creditCardDetailStyles;
    const {ItemLabelText,Itemtext} = insuranceDetailStyle;

    return (
      
      <View style={{justifyContent:'space-between',flex: 1,flexDirection:'row'}}>
          <View style={{flex: 0.2,justifyContent: 'center',alignItems:'center'}}>
                 <Image resizeMode='contain' source={require('./img/check.png')} style={{ width: 30, height: 30}} />
          </View>
        <View style={{flex: 0.8,flexDirection:'column',alignItems: 'flex-start',paddingRight: wp(2)}}>
                           <HTMLView
                                  style= {ItemLabelText}
                                  value={(item.title) ? item.title == "" ||item.title == "null" ? "-" : item.title : ""}
                                 stylesheet={insuranceDetailStyle}
                                />
                                 <HTMLView
                                  style= {Itemtext}
                                  value={(item.text) ? item.text == "" ||item.text == "null" ? "-" : item.text : ""}
                                  stylesheet={{fontWeight: 'normal', fontFamily: "Lato-Regular",}}
                                />
           
        </View>
         
      </View>

    )
  }

  render() {
    const { WayHeaderView, container, wayNavigationItem, headerTitle } = styles;
    const {updownimg,DetailFeatureView, FeatureTitleText, updownImgView,sepratorView,AddFeatureDetailText,} = creditCardDetailStyles;
    const { HightLightValueTextView, CreditCardView, CreditCardImageView, ShortListBtnText,HeartImage, HighLightText,ShortListBtnView, HightLightSubText, HightLightView,CreditCardBtnBgView,ApplyBtnView,ApplyBtnText} = Creditcardstyles;
    const {CardNameText,sumText,Ammounttext,DesTextView} = insuranceStyle;

    var detail = this.state.CardDetail;
    var AboutDATA = detail.DATA.ABOUT;
    var DescriptionDATA = detail.DATA.DESCRIPTION;
    var PayoutDATA = detail.DATA.PAYOUT;

    var HighlightValue1 = detail.DATA.HIGHLIGHTS.Value1
    var HighlightValue2 = detail.DATA.HIGHLIGHTS.Value2
    var HighlightValue3 = detail.DATA.HIGHLIGHTS.Value3

    if (HighlightValue1.includes('$'))
    {
      HighlightValue1 = (HighlightValue1).replace('$', '');
    }
    if (HighlightValue2.includes('$'))
    {
      HighlightValue2 = (HighlightValue2).replace('$', '');
    }
    if (HighlightValue3.includes('$'))
    {
      HighlightValue3 = (HighlightValue3).replace('$', '');
    }


    return (

      <View style={container} >
        {/*header view*/}
        <View style={WayHeaderView}>
          <View style={{ flexDirection: 'row', paddingLeft: 10 }}>
            <TouchableOpacity style={wayNavigationItem} onPress={() => this.handleBackButtonClick()}>
              <Image resizeMode='contain' source={require('../../component/img/backicon.png')} style={{ width: wp(5), height: hp(2.2) }} />
            </TouchableOpacity>
            <Text style={{ ...headerTitle, padding: 5 }}>{detail.name}</Text>
          </View>
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
        {/* card view */}
        <View style={{ ...CreditCardView, marginBottom: hp(1) }}>
        <View style={{...CreditCardImageView,paddingTop: hp(1)}}>
            <Image  source={{uri: detail.image_url}} style={{alignSelf:'center',height: hp(15) ,width: wp(60)}} resizeMode='contain'/>
            <Text style={{...CardNameText}}>{detail.name}</Text>
        </View>
        <View style={{paddingHorizontal: wp(15)}}>
            <View style={{flex: 1,justifyContent:'space-between',flexDirection:'row',alignItems: 'flex-start'}}> 
                  <Text style={{...sumText}}>Sum Assured^ : </Text>
                 <Text style={{...Ammounttext}}>$1,500,000</Text>
            </View>
            <View style={{flex: 1,justifyContent:'space-between',flexDirection:'row',alignItems: 'flex-start'}}> 
                  <Text style={{...sumText}}>Premium^ : </Text>
                 <Text style={{...Ammounttext}}>$98.75 /mth</Text>
            </View>
        </View>
        <FlatList
                    style={{alignSelf:'center',marginVertical: hp(2)}}
                        
                        showsHorizontalScrollIndicator={false}
                        horizontal={true}
                        data={detail.policyIncludes}
                        renderItem={({item,  index}) => 
                           this.PolicyIncludesListItems(item,index)
                        }
                        keyExtractor={(item, index) => index.toString()}
                    />
          <View style={{ flexDirection: 'row', backgroundColor: "#FAF7F7", justifyContent: 'center', marginBottom: hp(2) }} >

            <View style={{ ...HightLightView }}>
              <View style={{ ...HightLightValueTextView }}>
                <Text style={{ ...HighLightText }}>{(detail.DATA.HIGHLIGHTS.Value1) ? ( detail.DATA.HIGHLIGHTS.Value1 == "" || detail.DATA.HIGHLIGHTS.Value1 == "null") ? '-' : (detail.DATA.HIGHLIGHTS.Value1.includes(',') ? detail.DATA.HIGHLIGHTS.Value1 : ( detail.DATA.HIGHLIGHTS.Value1 == HighlightValue1 ?  detail.DATA.HIGHLIGHTS.Value1 : ((isNaN(parseFloat(HighlightValue1))) ?  detail.DATA.HIGHLIGHTS.Value1 :  '$' + NumberFormate.CommaFormatted(parseFloat(HighlightValue1).toFixed(2))))): "-"}</Text>
              </View>

              <Text style={{ ...HightLightSubText }}>{detail.DATA.HIGHLIGHTS.Label1}</Text>
            </View>

            <View style={{ ...HightLightView }}>
              <View style={{ ...HightLightValueTextView }}>
                <Text style={{ ...HighLightText }}>{( detail.DATA.HIGHLIGHTS.Value2) ? ( detail.DATA.HIGHLIGHTS.Value2 == "" || detail.DATA.HIGHLIGHTS.Value2 == "null") ? '-' : (detail.DATA.HIGHLIGHTS.Value2.includes(',') ? detail.DATA.HIGHLIGHTS.Value2 : ( detail.DATA.HIGHLIGHTS.Value2 == HighlightValue2 ?  detail.DATA.HIGHLIGHTS.Value2 : ((isNaN(parseFloat(HighlightValue2))) ?  detail.DATA.HIGHLIGHTS.Value2 :  '$' + NumberFormate.CommaFormatted(parseFloat(HighlightValue2).toFixed(2))))): "-"}</Text>
              </View>
              <Text style={{ ...HightLightSubText }}>{detail.DATA.HIGHLIGHTS.Label2}</Text>
            </View>

            <View style={{ ...HightLightView }}>
              <View style={{ ...HightLightValueTextView }}>
                <Text style={{ ...HighLightText }}>{( detail.DATA.HIGHLIGHTS.Value3) ? ( detail.DATA.HIGHLIGHTS.Value3 == "" || detail.DATA.HIGHLIGHTS.Value3 == "null") ? '-' : (detail.DATA.HIGHLIGHTS.Value3.includes(',') ? detail.DATA.HIGHLIGHTS.Value3 : ( detail.DATA.HIGHLIGHTS.Value3 == HighlightValue3 ?  detail.DATA.HIGHLIGHTS.Value3 : ((isNaN(parseFloat(HighlightValue3))) ?  detail.DATA.HIGHLIGHTS.Value3 :  '$' + NumberFormate.CommaFormatted(parseFloat(HighlightValue3).toFixed(2))))):"-"}</Text>
              </View>
              <Text style={{ ...HightLightSubText }}>{detail.DATA.HIGHLIGHTS.Label3}</Text>
            </View>
          </View>

        </View>
        {/*Credit card Detail*/}
        <View style={{ marginHorizontal: wp(2.5) }}>

                        <View>
                              
                                <Text style={{...CardNameText,paddingVertical: hp(2)}}>{detail.tagline}</Text>
                                <View style={{...sepratorView ,marginHorizontal: wp(2)}}></View>
                        </View>
        
         {/* DESCRIPTION */}
          <View>
          
            <TouchableOpacity style={{...DetailFeatureView }} onPress={() => this.state.isdescription == true ? this.handleDescriptionSelection(false) : this.handleDescriptionSelection(true)}>
              <Text style={{...FeatureTitleText}}>{(detail.DATA.DESCRIPTION.TITLE) ? detail.DATA.DESCRIPTION.TITLE : "Key Features"}</Text>

              <View style={updownImgView}>
                <Image style={updownimg} resizeMode='contain' source={this.state.isdescription == false ? require('../CreditCardDetail/img/downarrow.png'): require('../CreditCardDetail/img/uparrow.png')}></Image>
              </View>
            </TouchableOpacity>
          </View>
          {
            this.state.isdescription == true ?
              <View>
                <View style={{...sepratorView ,marginHorizontal: wp(2)}}></View>
                {
                  Object.entries(DescriptionDATA).map(([key, value]) => {
                      if (key != "TITLE")
                      {
                        if (value == 'null' || value == '') {

                        }
                        else {
                          return (
                            <View style={DesTextView}>
                             
                               <HTMLView
                                    style= {{paddingVertical: hp(1.5),paddingLeft: wp(2),paddingRight: wp(2.5),textAlign:'left'}}
                                    value={value}
                                   stylesheet={insuranceDetailStyle}
                                 />
                              
                            </View>
                             )
                        }
                      }
                    
                  })
                }

              </View>
              : <View></View>
          }

    {/* ABOUT */}
    <View>
              <View style={{...sepratorView ,marginHorizontal: wp(2)}}></View>
            <TouchableOpacity style={{...DetailFeatureView }} onPress={() => this.state.isaboutPolicy == true ? this.handleAboutPolicySelection(false) : this.handleAboutPolicySelection(true)}>
              <Text style={{...FeatureTitleText}}>{(detail.DATA.ABOUT.TITLE) ? detail.DATA.ABOUT.TITLE : "About the Policy"}</Text>

              <View style={updownImgView}>
                <Image style={updownimg} resizeMode='contain' source={this.state.isaboutPolicy == false ? require('../CreditCardDetail/img/downarrow.png'): require('../CreditCardDetail/img/uparrow.png')}></Image>
              </View>
            </TouchableOpacity>
          </View>
          {
            this.state.isaboutPolicy == true ?
            
              <View>
                <View style={{...sepratorView ,marginHorizontal: wp(2)}}></View>
                {
                  Object.entries(AboutDATA).map(([key, value]) => {
                    if (key != "TITLE")
                    {
                        if (value == 'null' || value == '') {

                        }
                        else {
                            if (value.includes('-'))
                            {
                                value = value.replace(/-/gi,'•');
                             
                               return (
                                <View style={DesTextView}>
                                   <HTMLView
                                    style= {{paddingVertical: hp(1.5),paddingLeft: wp(5),paddingRight: wp(2.5),textAlign:'left'}}
                                    value={value}
                                   stylesheet={insuranceDetailStyle}
                                 />
                                  
                                </View>
                                 )
                            }
                            else
                            {
                                return (
                                    <View style={{...DesTextView,}}>
                                     
                                     <HTMLView
                                    style= {{paddingVertical: hp(1.5),paddingLeft: wp(2),paddingRight: wp(2.5),textAlign:'left'}}
                                    value={value}
                                   stylesheet={{...insuranceDetailStyle}}
                                 />
                                      
                                    </View>
                                     )
                            }
                         
                        }
                    }
                   
                  })
                }

              </View>
              : <View></View>
          }

          {/* PAYOUT */}
          <View>
          <View style={{...sepratorView ,marginHorizontal: wp(2)}}></View>
            <TouchableOpacity style={{...DetailFeatureView }} onPress={() => this.state.ispayout == true ? this.handlPayoutFeature(false) : this.handlPayoutFeature(true)}>
              <Text style={{...FeatureTitleText}}>{(detail.DATA.PAYOUT.TITLE) ? detail.DATA.PAYOUT.TITLE : "Key Features"}</Text>

              <View style={updownImgView}>
                <Image style={updownimg} resizeMode='contain' source={this.state.ispayout == false ? require('../CreditCardDetail/img/downarrow.png'): require('../CreditCardDetail/img/uparrow.png')}></Image>
              </View>
            </TouchableOpacity>
          </View>
          {
            this.state.ispayout == true ?
              <View>
                <View style={{...sepratorView ,marginHorizontal: wp(2)}}></View>
                {
                  Object.entries(PayoutDATA).map(([key, value]) => {
                      if (key != "TITLE")
                      {
                        if (value == 'null' || value == '') {

                        }
                        else {
                          return (
                            <View style={DesTextView}>
                              <HTMLView
                                  style= {{paddingVertical: hp(1.5),paddingLeft: wp(2),paddingRight: wp(2.5),textAlign:'left'}}
                                  value={value}
                                 stylesheet={insuranceDetailStyle}
                                />
                               
                            </View>
                             )
                        }
                      }
                    
                  })
                }

              </View>
              : <View></View>
          }

          {/** FEATURES */}

           <View>
           <View style={{...sepratorView ,marginHorizontal: wp(2)}}></View>
            <TouchableOpacity style={{...DetailFeatureView}} onPress={() => this.state.isuniqueFeature == true ? this.handleUniqueFeature(false) : this.handleUniqueFeature(true)}>
              <Text style={{...FeatureTitleText }}>{(detail.DATA.FEATURES.TITLE) ? detail.DATA.FEATURES.TITLE : "Key Features"}</Text>

              <View style={updownImgView}>
                <Image style={updownimg} resizeMode='contain' source={ this.state.isuniqueFeature == false ? require('../CreditCardDetail/img/downarrow.png'): require('../CreditCardDetail/img/uparrow.png')}></Image>
              </View>
            </TouchableOpacity>
          </View>
          {
            this.state.isuniqueFeature == true ?
              <View>
                <View style={{...sepratorView,marginHorizontal: wp(2)}}></View>
                <FlatList
                  contentContainerStyle={{ marginHorizontal: wp(2.5) }}
                  showsVerticalScrollIndicator={false}
                  data={detail.DATA.FEATURES.DATA}
                  renderItem={({ item, index }) =>
                    this.KeyFeatureListItem(item, index)
                  }
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
              : <View></View>
          } 
          {/** RIDERS */}

          <View>
          <View style={{...sepratorView ,marginHorizontal: wp(2)}}></View>
            <TouchableOpacity style={{...DetailFeatureView}} onPress={() => this.state.isOptionalRiders == true ? this.handleOptionalRidersFeature(false) : this.handleOptionalRidersFeature(true)}>
              <Text style={{...FeatureTitleText }}>{(detail.DATA.RIDERS.TITLE) ? detail.DATA.RIDERS.TITLE : "Key Features"}</Text>

              <View style={updownImgView}>
                <Image style={updownimg} resizeMode='contain' source={ this.state.isOptionalRiders == false ? require('../CreditCardDetail/img/downarrow.png'): require('../CreditCardDetail/img/uparrow.png')}></Image>
              </View>
            </TouchableOpacity>
          </View>
          {
            this.state.isOptionalRiders == true ?
              <View>
                <View style={{...sepratorView,marginHorizontal: wp(2)}}></View>
                <FlatList
                  contentContainerStyle={{ marginHorizontal: wp(2.5) }}
                  showsVerticalScrollIndicator={false}
                  data={detail.DATA.RIDERS.DATA}
                  renderItem={({ item, index }) =>
                    this.KeyFeatureListItem(item, index)
                  }
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
              : <View></View>
          } 
          
        </View>
        <View style={{...CreditCardBtnBgView,marginVertical: hp(2)}}>
            <TouchableOpacity style={{...ApplyBtnView}}>
                <Text style={{...ApplyBtnText}}>{strings('profile.getQuotes')}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{...ShortListBtnView}}>
                <Image source={require('../FeaturedCard/img/heart.png')} style={{...HeartImage}} resizeMode='contain'/>
                <Text style={{...ShortListBtnText}}>{strings('profile.shortList')}</Text>
            </TouchableOpacity>
        </View>
        </ScrollView>
    </View>
    )
  };

};