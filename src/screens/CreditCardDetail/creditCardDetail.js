import React, { Component } from 'react'
import Constant from '../../helper/themeHelper';
import { View, Text, Image, FlatList, ScrollView,ImageBackground } from 'react-native'
import { wp, hp } from '../../helper/responsiveScreen';
import styles from '../../component/styles';
import Creditcardstyles from '../../screens/FeaturedCard/styles';
import creditCardDetailStyles from '../../screens/CreditCardDetail/styles';
import { strings } from '../../component/translation/i18n';
import { TouchableOpacity, TouchableNativeFeedback } from 'react-native-gesture-handler'
import NumberFormate from '../../common/NumberFormate';

export default class creditCardDetail extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      Index: this.props.route.params.index,
      CardDetail: this.props.route.params.item,
      isKeyFeature: true,
      iseligibilityfees: false,
      isPromo: false,
      isAddFeature: false,
    }
  }

  componentDidMount() {
    console.log("....index", this.state.CardDetail);
  }

  //Back button click event
  handleBackButtonClick() {
    this.props.navigation.goBack()
    return true;
  };

  handleKeyFeatureSelection(value) {
    this.setState({ isKeyFeature: value });
    this.setState({ iseligibilityfees: false });
    this.setState({ isPromo: false });
    this.setState({ isAddFeature: false });
  }
  handleEligibilitySelection(value) {
    this.setState({ isKeyFeature: false });
    this.setState({ iseligibilityfees: value });
    this.setState({ isPromo: false });
    this.setState({ isAddFeature: false });
  }
  handlPromoFeature(value) {
    this.setState({ isKeyFeature: false });
    this.setState({ iseligibilityfees: false });
    this.setState({ isPromo: value });
    this.setState({ isAddFeature: false });
  }
  handleAddFeature(value) {
    this.setState({ isKeyFeature: false });
    this.setState({ iseligibilityfees: false });
    this.setState({ isPromo: false });
    this.setState({ isAddFeature: value });

  }


  // key feature list and eligibility fees UI
  KeyFeatureListItem = (item, index) => {
    const {sepratorView,DetailText,labelText} = creditCardDetailStyles;

    var v = item.value
  if (item.value)
  {
    if (item.value.includes('$'))
    {
      v = (item.value).replace('$', '');
     
    }
  }
  

    return (
      
      <View style={{justifyContent:'center'}}>
      <View style={{flex: 1,justifyContent:'space-between',flexDirection:'row',alignItems: 'flex-start' }}>
             <Text style={{...labelText}}>{(item.label) ? item.label == "" ||item.label == "null" ? "-" : item.label : ""}</Text>
            <Text style={{...DetailText}}>{ (item.value) ? (item.value == "" ||item.value == "null") ? '-' : (item.value == v ? item.value : ((isNaN(parseFloat(v))) ? item.value :  '$' + NumberFormate.CommaFormatted(parseFloat(v).toFixed(2)))) : "-"}</Text>
        </View>
     <View style={{...sepratorView}}></View>
      </View>

    )
  }

  render() {
    const { WayHeaderView, container, wayNavigationItem, headerTitle, WayRightNavigationItem } = styles;
    const {updownimg,DetailFeatureView, FeatureTitleText, updownImgView,sepratorView,DetailText,Conditiontext,hypertext,AddFeatureDetailText,PromoDetailText} = creditCardDetailStyles;
    const { HightLightValueTextView, CreditCardView, CreditCardImageView, ShortListBtnText,CreditCardNameText,HeartImage, HighLightText,ShortListBtnView, HightLightSubText, HightLightView,CreditCardBtnBgView,ApplyBtnView,ApplyBtnText} = Creditcardstyles;

    var detail = this.state.CardDetail;
    var promoDATA = detail.DATA.PROMO_FEATURES.DATA;
    var AddFeatureDATA = detail.DATA.ADD_FEATURES.DATA;

    var HighlightValue1 = detail.DATA.HIGHLIGHTS.Value1
    var HighlightValue2 = detail.DATA.HIGHLIGHTS.Value2
    var HighlightValue3 = detail.DATA.HIGHLIGHTS.Value3

    if (HighlightValue1.includes('$'))
    {
      HighlightValue1 = (HighlightValue1).replace('$', '');
    }
    if (HighlightValue2.includes('$'))
    {
      HighlightValue2 = (HighlightValue2).replace('$', '');
    }
    if (HighlightValue3.includes('$'))
    {
      HighlightValue3 = (HighlightValue3).replace('$', '');
    }

    return (

      <View style={container} >
        {/*header view*/}
        <View style={WayHeaderView}>
          <View style={{ flexDirection: 'row', paddingLeft: 10 }}>
            <TouchableOpacity style={wayNavigationItem} onPress={() => this.handleBackButtonClick()}>
              <Image resizeMode='contain' source={require('../../component/img/backicon.png')} style={{ width: wp(5), height: hp(2.2) }} />
            </TouchableOpacity>
            <Text style={{ ...headerTitle, padding: 5 }}>{detail.CardName}</Text>
          </View>
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
        {/* card view */}
        <View style={{ ...CreditCardView, marginBottom: hp(1) }}>
          <View style={{ ...CreditCardImageView }}>
           <ImageBackground source={require('../FeaturedCard/img/BubbleCardImgBg.png')} style={{alignSelf:'center',width: (Platform.OS) == 'ios' ? wp(73): wp(67)}} resizeMode='contain'>
              <Image source={require('../FeaturedCard/img/card.png')} style={{alignSelf:'center',width: wp(65)}} resizeMode='contain'/>
            </ImageBackground>
            <Text style={{ ...CreditCardNameText }}>{(detail.CardName) ? detail.CardName : ""}</Text>
          </View>
          <View style={{ flexDirection: 'row', backgroundColor: "#FAF7F7", justifyContent: 'center', marginBottom: hp(2) }} >

            <View style={{ ...HightLightView }}>
              <View style={{ ...HightLightValueTextView }}>
                <Text style={{ ...HighLightText }}>{(detail.DATA.HIGHLIGHTS.Value1) ? ( detail.DATA.HIGHLIGHTS.Value1 == "" || detail.DATA.HIGHLIGHTS.Value1 == "null") ? '-' : (detail.DATA.HIGHLIGHTS.Value1.includes(',') ? detail.DATA.HIGHLIGHTS.Value1 : ( detail.DATA.HIGHLIGHTS.Value1 == HighlightValue1 ?  detail.DATA.HIGHLIGHTS.Value1 : ((isNaN(parseFloat(HighlightValue1))) ?  detail.DATA.HIGHLIGHTS.Value1 :  '$' + NumberFormate.CommaFormatted(parseFloat(HighlightValue1).toFixed(2))))): "-"}</Text>
              </View>

              <Text style={{ ...HightLightSubText }}>{detail.DATA.HIGHLIGHTS.Label1}</Text>
            </View>

            <View style={{ ...HightLightView }}>
              <View style={{ ...HightLightValueTextView }}>
                <Text style={{ ...HighLightText }}>{( detail.DATA.HIGHLIGHTS.Value2) ? ( detail.DATA.HIGHLIGHTS.Value2 == "" || detail.DATA.HIGHLIGHTS.Value2 == "null") ? '-' : (detail.DATA.HIGHLIGHTS.Value2.includes(',') ? detail.DATA.HIGHLIGHTS.Value2 : ( detail.DATA.HIGHLIGHTS.Value2 == HighlightValue2 ?  detail.DATA.HIGHLIGHTS.Value2 : ((isNaN(parseFloat(HighlightValue2))) ?  detail.DATA.HIGHLIGHTS.Value2 :  '$' + NumberFormate.CommaFormatted(parseFloat(HighlightValue2).toFixed(2))))): "-"}</Text>
              </View>
              <Text style={{ ...HightLightSubText }}>{detail.DATA.HIGHLIGHTS.Label2}</Text>
            </View>

            <View style={{ ...HightLightView }}>
              <View style={{ ...HightLightValueTextView }}>
                <Text style={{ ...HighLightText }}>{( detail.DATA.HIGHLIGHTS.Value3) ? ( detail.DATA.HIGHLIGHTS.Value3 == "" || detail.DATA.HIGHLIGHTS.Value3 == "null") ? '-' : (detail.DATA.HIGHLIGHTS.Value3.includes(',') ? detail.DATA.HIGHLIGHTS.Value3 : ( detail.DATA.HIGHLIGHTS.Value3 == HighlightValue3 ?  detail.DATA.HIGHLIGHTS.Value3 : ((isNaN(parseFloat(HighlightValue3))) ?  detail.DATA.HIGHLIGHTS.Value3 :  '$' + NumberFormate.CommaFormatted(parseFloat(HighlightValue3).toFixed(2))))):"-"}</Text>
              </View>
              <Text style={{ ...HightLightSubText }}>{detail.DATA.HIGHLIGHTS.Label3}</Text>
            </View>
          </View>

        </View>
        {/*Credit card Detail*/}
        <View style={{ marginHorizontal: wp(2.5) }}>
          {/* key feature */}
          <View>
            <TouchableOpacity style={{...DetailFeatureView}} onPress={() => this.state.isKeyFeature == true ? this.handleKeyFeatureSelection(false) : this.handleKeyFeatureSelection(true)}>
              <Text style={{...FeatureTitleText }}>{(detail.DATA.KEY_FEATURES.TITLE) ? detail.DATA.KEY_FEATURES.TITLE : "Key Features"}</Text>

              <View style={updownImgView}>
                <Image style={updownimg} resizeMode='contain' source={ this.state.isKeyFeature == false ? require('../CreditCardDetail/img/downarrow.png'): require('../CreditCardDetail/img/uparrow.png')}></Image>
              </View>
            </TouchableOpacity>
          </View>
          {
            this.state.isKeyFeature == true ?
              <View>
                <View style={{...sepratorView,marginHorizontal: wp(2)}}></View>
                <FlatList
                  contentContainerStyle={{ marginHorizontal: wp(2.5) }}
                  showsVerticalScrollIndicator={false}
                  data={detail.DATA.KEY_FEATURES.MILES}
                  renderItem={({ item, index }) =>
                    this.KeyFeatureListItem(item, index)
                  }
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
              : <View style={{ ...sepratorView ,marginHorizontal: wp(2)}}></View>
          }
          {/* ELIGIBILITY_FEES */}
          <View>
            <TouchableOpacity style={{...DetailFeatureView}} onPress={() => this.state.iseligibilityfees == true ? this.handleEligibilitySelection(false) : this.handleEligibilitySelection(true)}>
              <Text style={{...FeatureTitleText}}>{(detail.DATA.ELIGIBILITY_FEES.TITLE) ? detail.DATA.ELIGIBILITY_FEES.TITLE : "Eligibility \u0026 Fees"}</Text>

              <View style={updownImgView}>
                <Image style={updownimg} resizeMode='contain' source={this.state.iseligibilityfees == false ? require('../CreditCardDetail/img/downarrow.png'): require('../CreditCardDetail/img/uparrow.png')}></Image>
              </View>
            </TouchableOpacity>
          </View>
          {
            this.state.iseligibilityfees == true ?
              <View>
                <View style={{...sepratorView,marginHorizontal: wp(2)}}></View>
                <FlatList
                  contentContainerStyle={{ marginHorizontal: wp(2.5) }}
                  showsVerticalScrollIndicator={false}
                  data={detail.DATA.ELIGIBILITY_FEES.DATA}
                  renderItem={({ item, index }) =>
                    this.KeyFeatureListItem(item, index)
                  }
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
              : <View style={{ ...sepratorView ,marginHorizontal: wp(2)}}></View>
          }

          {/* PROMO_FEATURES */}
          <View>
            <TouchableOpacity style={{...DetailFeatureView }} onPress={() => this.state.isPromo == true ? this.handlPromoFeature(false) : this.handlPromoFeature(true)}>
              <Text style={{...FeatureTitleText }}>{(detail.DATA.PROMO_FEATURES.TITLE) ? detail.DATA.PROMO_FEATURES.TITLE : "Promotional Features"}</Text>

              <View style={updownImgView}>
                <Image style={updownimg} resizeMode='contain' source={this.state.isPromo == false ? require('../CreditCardDetail/img/downarrow.png'): require('../CreditCardDetail/img/uparrow.png')}></Image>
              </View>
            </TouchableOpacity>
          </View>
          {
            this.state.isPromo == true ?
              <View>
                <View style={{...sepratorView,marginHorizontal: wp(2) }}></View>
                {
                  Object.entries(promoDATA).map(([key, value]) => {
                    if(value)
                    {
                      if (value == 'null' || value == '') {

                      }
                      else {
                        const words = value.split('\n');
                     
                       if (words.length == 3) {
                      
                          return (
  
                            <View style={{ justifyContent: 'center', marginHorizontal: wp(2.5) }}>
                             
                                      <Text style={{...hypertext }}>{words[0].trim()}</Text>
                                     <Text style={{...PromoDetailText}}>{words[1]}</Text>
                                     <Text style={{...PromoDetailText}}>{words[2]}</Text>
  
                              <View style={{...sepratorView}}></View>
                            </View>
  
                          )
                       }
  
                      }
                    }
                    

                  })
                }

              </View>
              : <View style={{ ...sepratorView ,marginHorizontal: wp(2)}} ></View>
          }
          {/* ADD_FEATURES */}
          <View>
            <TouchableOpacity style={{...DetailFeatureView }} onPress={() => this.state.isAddFeature == true ? this.handleAddFeature(false) : this.handleAddFeature(true)}>
              <Text style={{...FeatureTitleText}}>{(detail.DATA.ADD_FEATURES.TITLE) ? detail.DATA.ADD_FEATURES.TITLE : "Additional Features"}</Text>

              <View style={updownImgView}>
                <Image style={updownimg} resizeMode='contain' source={this.state.isAddFeature == false ? require('../CreditCardDetail/img/downarrow.png'): require('../CreditCardDetail/img/uparrow.png')}></Image>
              </View>
            </TouchableOpacity>
          </View>
          {
            this.state.isAddFeature == true ?
              <View>
                <View style={{...sepratorView ,marginHorizontal: wp(2)}}></View>
                {
                  Object.entries(AddFeatureDATA).map(([key, value]) => {
                    if (value == 'null' || value == '') {

                    }
                    else {
                      return (
                        <View style={{ justifyContent: 'center', marginHorizontal: wp(2.5) }}>
                          <View style={{flexDirection: 'row',alignItems: 'flex-start',flexWrap: 'wrap', flex: 1}}> 
                            <View style={{width: 20,paddingVertical: hp(1)}}>
                                <Text style={{fontWeight: 'bold',fontFamily: "Lato-Bold"}}>{'\u2022'}</Text>
                             </View>
                           <Text style={{...AddFeatureDetailText,flex: 0.95}}>{value}</Text>
                          </View>
                          <View style={{...sepratorView }}></View>
                        </View>
                         )
                    }
                  })
                }

              </View>
              : <View style={{ ...sepratorView ,marginHorizontal: wp(2)}}></View>
          }
        </View>
        <View style={{...CreditCardBtnBgView,marginVertical: hp(2)}}>
            <TouchableOpacity style={{...ApplyBtnView}}>
             <Text style={{...ApplyBtnText}}>{strings('profile.applyNow')}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{...ShortListBtnView}}>
                <Image source={require('../FeaturedCard/img/heart.png')} style={{...HeartImage}} resizeMode='contain'/>
                <Text style={{...ShortListBtnText}}>{strings('profile.shortList')}</Text>
            </TouchableOpacity>
        </View>
        </ScrollView>
    </View>
    )
  };

};