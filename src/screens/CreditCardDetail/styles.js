import {
    StyleSheet
  } from 'react-native';
  import Constant from '../../helper/themeHelper';
  import { wp, hp } from '../../helper/responsiveScreen';

  const styles = StyleSheet.create({
   
    updownimg:{
      width: 15,
       height: 15
    },
    DetailFeatureView:{
      paddingHorizontal: wp(2.5), 
      backgroundColor: 'white', 
      flexDirection: 'row', 
      paddingVertical: hp(2), 
      justifyContent: 'space-between'
    },
    FeatureTitleText: {
      color: Constant.color.black,
       fontSize: Constant.fontSize.medium,
        fontWeight: 'bold', 
        fontFamily: "Lato-Bold"
    },
    updownImgView:{
      justifyContent: 'center',
       paddingHorizontal: wp(0.5)
    },
    Conditiontext: {
      flex:0.4,
      color: Constant.color.textgray,
       fontSize: Constant.fontSize.xsmall,
        fontWeight: 'bold', 
        fontFamily: "Lato-Bold", 
        paddingTop: hp(1),
        textAlign:'left'
    },
    DetailText:{
      flex: 0.5,
      color: Constant.color.textgray,
       fontSize: Constant.fontSize.xsmall, 
       fontWeight: 'bold', 
       fontFamily: "Lato-Bold",
        paddingVertical: hp(1) ,
        textAlign:'right'
    },
    PromoDetailText:{
      flex: 1,
      color: Constant.color.textgray,
       fontSize: Constant.fontSize.xsmall, 
       fontWeight: 'normal', 
       fontFamily: "Lato-Regular",
        paddingVertical: hp(1) ,
        textAlign:'left'
    },
    AddFeatureDetailText:{
      flex: 0.5,
      color: Constant.color.textgray,
       fontSize: Constant.fontSize.xsmall, 
       fontWeight: 'normal', 
       fontFamily: "Lato-Regular",
        paddingVertical: hp(1) ,
        textAlign:'left'
    },
    hypertext:{
      flex: 0.4,
      color: Constant.color.selectedtextColor, 
      fontSize: Constant.fontSize.xsmall,
       fontWeight: 'bold', 
       fontFamily: "Lato-Bold", 
       paddingTop: hp(1),
       textAlign: 'left'
    },
    sepratorView:{
      height: 1,
       backgroundColor: Constant.color.sepratorColor
    },
    labelText:{
      flex: 0.5,
      textAlign:'left',
      color: Constant.color.textgray, 
      fontSize: Constant.fontSize.xsmall, 
      fontWeight: 'normal', 
      fontFamily: "Lato-Regular", 
      paddingVertical: hp(1)
    }
 
});

export default styles;