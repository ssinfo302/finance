import {
    StyleSheet
  } from 'react-native';
  import Constant from '../../helper/themeHelper';
  import { wp, hp } from '../../helper/responsiveScreen';

  const styles = StyleSheet.create({
CreditCardTitleText:{
    color: Constant.color.textgray,
    fontSize: Constant.fontSize.xmedium,
    marginVertical: hp(1),
   fontWeight: 'normal',
    fontFamily: "Lato-Regular",
     textAlign:"center"
  },
  CreditCardView:{
    marginHorizontal: wp(2.5),
    marginTop: hp(1),
    marginBottom: hp(2),
    borderRadius:20,
    borderColor: Constant.color.creditCardShadowColor,
    borderWidth: 0,
    backgroundColor: Constant.color.white,
    ...Constant.shadowStyle,
    shadowColor:Constant.color.creditCardShadowColor,
   
  },
  CreditCardImageView:{
    paddingTop:  Constant.isX ? hp(2) : hp(1.5),
    paddingBottom:Constant.isX ? hp(0) : hp(0.5) ,
    paddingHorizontal:hp(1.5),
    alignItems:'center'
  },
  CreditCardNameText:{
    color: Constant.color.textgray,
    fontSize: Constant.fontSize.small,
    fontWeight: 'normal',
    fontFamily: "Lato-Regular",
    paddingVertical:hp(1),
     textAlign:"center"
  },
  HightLightView:{
    width: '33.3%',
    alignItems:'center',
    flexDirection:'column',
    paddingVertical: (Platform.OS) == 'ios' ? hp(1.5): hp(0.5),
    paddingHorizontal: (Platform.OS) == 'ios' ? 5 : 10,
    backgroundColor:Constant.color.white
  },
  HightLightValueTextView:{
    height: Constant.isX ? hp(5.5) : hp(6),
    justifyContent:'center'
  },

  HighLightText:{
    textAlign:'center',
    color:Constant.color.selectedtextColor,
    fontSize: Constant.fontSize.xsmall,
    fontWeight: 'bold',
    fontFamily: "Lato-Bold",
  },
  HightLightSubText:{
    textAlign:'center',
    color: Constant.color.offerText,
    fontSize: Constant.fontSize.xxsmall,
    fontWeight: 'normal',
    fontFamily: "Lato-Regular",
  },
  CreditCardSubtext:{
    paddingHorizontal: Constant.isX ? wp(19) : wp(15),
    paddingTop: (Platform.OS) == 'ios' ? Constant.isX ? hp(0.2) : hp(1) : hp(0.5),
    textAlign:'center',
    color:Constant.color.offerText,
    fontSize: Constant.fontSize.small,
    fontWeight: 'normal',
    fontFamily: "Lato-Regular",
  },
  CustomMsgValuetext:{
    textAlign:'center',
    color:Constant.color.offerText,
    fontSize: Constant.fontSize.small, 
    paddingBottom: (Platform.OS) == 'ios' ? Constant.isX ? hp(2) : hp(1.5) : hp(0.5),
    fontWeight: 'bold',
    fontFamily: "Lato-Bold",
  },
  CreditCardBtnBgView:{
    paddingHorizontal: wp(5),
    paddingBottom: hp(2),
    flexDirection:'row',
    justifyContent:'space-around'
  },
  ApplyBtnView:{
    backgroundColor: Constant.color.ApplyBtnColor, 
    height:52,
    width:wp(50),
    borderRadius: hp(4),
    alignItems:'center',
    justifyContent:'center'
  },
  ApplyBtnText:{
    color:Constant.color.white,
    fontSize: Constant.fontSize.mini,
    fontWeight: 'bold',
    fontFamily: "Lato-Bold"
  },
  ShortListBtnView:{
    backgroundColor:'rgba(250,231,231,1)', 
    height:52,
    width:wp(30),
    borderRadius: hp(4),
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'row'
  },
  ShortListBtnText:{
    padding: 5,
    color:Constant.color.ShortListTextColor,
    fontSize: Constant.fontSize.mini,
    fontWeight: 'bold',
    fontFamily: "Lato-Bold"
  },
  MoreDetailBtnView:{
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'row',
    paddingVertical: (Platform.OS) == 'ios' ? hp(2) : hp(1)
  },
  MoreDetailBtnText:{
    textAlign:'center',
    color:Constant.color.offerText,
    fontSize: Constant.fontSize.xxsmall,
    fontWeight: 'normal',
    fontFamily: "Lato-Regular",
    paddingHorizontal: 5
  },
  HeartImage:{
    alignSelf:'center',
    height:15,
    width:15
  },
  RightBtnImage:{
    alignSelf:'center',
    height:25,
    width:25
  },
 
});

export default styles;
