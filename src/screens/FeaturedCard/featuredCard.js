import React, { Component } from 'react'
import Constant from '../../helper/themeHelper';
import { View,Text, Image,FlatList,ScrollView,ImageBackground} from 'react-native'
import { wp, hp } from '../../helper/responsiveScreen';
import styles from '../../component/styles';
import cardstyles from '../../screens/FeaturedCard/styles';
import { strings } from '../../component/translation/i18n';
import { TouchableOpacity, TouchableNativeFeedback } from 'react-native-gesture-handler'
import creditCardJson from '../../screens/FeaturedCard/FeaturedCard.json';
import NumberFormate from '../../common/NumberFormate';

export default class featuredCard extends React.Component {

    constructor(props) {
      super(props);
        this.state = {
            dataSource: []
        }
    }

    componentDidMount() {
      
        this.setState({ dataSource: creditCardJson });
      }
//Back button click event
handleBackButtonClick() {
    this.props.route.params.onGoBack();
    this.props.navigation.goBack();
    return true;
  }


  
  
  //Credit Card list UI
  CreditCardListItem =(item)=>{
    const {  MoreDetailBtnText,HeartImage,MoreDetailBtnView,ShortListBtnText,RightBtnImage,HightLightValueTextView,ShortListBtnView,ApplyBtnText,CustomMsgValuetext, ApplyBtnView,CreditCardView,CreditCardBtnBgView,CreditCardImageView,CreditCardNameText,HighLightText,HightLightSubText,HightLightView,CreditCardSubtext} = cardstyles;
   
    var HighlightValue1 = item.DATA.HIGHLIGHTS.Value1
    var HighlightValue2 = item.DATA.HIGHLIGHTS.Value2
    var HighlightValue3 = item.DATA.HIGHLIGHTS.Value3

    if (HighlightValue1)
    {
        if (HighlightValue1.includes('$'))
        {
          HighlightValue1 = (HighlightValue1).replace('$', '');
        }
    }
   
    if (HighlightValue2)
    {
        if (HighlightValue2.includes('$'))
        {
          HighlightValue2 = (HighlightValue2).replace('$', '');
        }
    }
    
    if (HighlightValue3)
    {
        if (HighlightValue3.includes('$'))
        {
          HighlightValue3 = (HighlightValue3).replace('$', '');
        }
    }
   

    return(
        <View style={{...CreditCardView}}>
        <View style={{...CreditCardImageView}}>
            <ImageBackground source={require('../FeaturedCard/img/BubbleCardImgBg.png')} style={{alignSelf:'center',width:(Platform.OS) == 'ios' ? wp(73): wp(67)}} resizeMode='contain'>
              <Image source={require('../FeaturedCard/img/card.png')} style={{alignSelf:'center',width: wp(65)}} resizeMode='contain'/>
            </ImageBackground>
            <Text style={{...CreditCardNameText}}>{item.CardName}</Text>
        </View>
        <View style={{flexDirection: 'row',backgroundColor: "#FAF7F7",justifyContent:'center'}} >
            
        <View style={{...HightLightView}}>
                <View style={{...HightLightValueTextView}}>
                <Text style={{...HighLightText}}>{(item.DATA.HIGHLIGHTS.Value1) ? ( item.DATA.HIGHLIGHTS.Value1 == "" || item.DATA.HIGHLIGHTS.Value1 == "null") ? '-' : (item.DATA.HIGHLIGHTS.Value1.includes(',') ? item.DATA.HIGHLIGHTS.Value1 : ( item.DATA.HIGHLIGHTS.Value1 == HighlightValue1 ?  item.DATA.HIGHLIGHTS.Value1 : ((isNaN(parseFloat(HighlightValue1))) ?  item.DATA.HIGHLIGHTS.Value1 :  '$' + NumberFormate.CommaFormatted(parseFloat(HighlightValue1).toFixed(2))))) : "-"}</Text>
                </View>
             
                <Text style={{...HightLightSubText}}>{item.DATA.HIGHLIGHTS.Label1}</Text>
            </View>
        
            <View style={{...HightLightView}}>
                 <View style={{...HightLightValueTextView}}>
                <Text style={{...HighLightText}}>{(item.DATA.HIGHLIGHTS.Value2) ? ( item.DATA.HIGHLIGHTS.Value2 == "" || item.DATA.HIGHLIGHTS.Value2 == "null") ? '-' : (item.DATA.HIGHLIGHTS.Value2.includes(',') ? item.DATA.HIGHLIGHTS.Value2: ( item.DATA.HIGHLIGHTS.Value2 == HighlightValue2 ?  item.DATA.HIGHLIGHTS.Value2 : ((isNaN(parseFloat(HighlightValue2))) ?  item.DATA.HIGHLIGHTS.Value2 :  '$' + NumberFormate.CommaFormatted(parseFloat(HighlightValue2).toFixed(2))))) : "-"}</Text>
                </View>
                <Text style={{...HightLightSubText}}>{item.DATA.HIGHLIGHTS.Label2}</Text>
            </View>
        
            <View style={{...HightLightView}}>
            <View style={{...HightLightValueTextView}}>
                <Text style={{...HighLightText}}>{(item.DATA.HIGHLIGHTS.Value3) ? ( item.DATA.HIGHLIGHTS.Value3 == "" || item.DATA.HIGHLIGHTS.Value3 == "null") ? '-' : (item.DATA.HIGHLIGHTS.Value3.includes(',') ? item.DATA.HIGHLIGHTS.Value3 : ( item.DATA.HIGHLIGHTS.Value3 == HighlightValue3 ?  item.DATA.HIGHLIGHTS.Value3 : ((isNaN(parseFloat(HighlightValue3))) ?  item.DATA.HIGHLIGHTS.Value3 :  '$' + NumberFormate.CommaFormatted(parseFloat(HighlightValue3).toFixed(2))))): "-"}</Text>
            </View>
                <Text style={{...HightLightSubText}}>{item.DATA.HIGHLIGHTS.Label3}</Text>
            </View>
        </View>
        <View style={{paddingVertical: hp(2)}}> 
                <Text style={{...CreditCardSubtext,paddingBottom: hp(1.5)}}>{item.DATA.custom_Message.msg1}</Text>
                <Text style={{...CreditCardSubtext}}>{item.DATA.custom_Message.savings}</Text>
                <Text style={{...CustomMsgValuetext}}>{item.DATA.custom_Message.value}</Text>
        </View>
        <View style={{...CreditCardBtnBgView}}>
            <TouchableOpacity style={{...ApplyBtnView}}>
                <Text style={{...ApplyBtnText}}>{strings('profile.applyNow')}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{...ShortListBtnView}}>
                <Image source={require('../FeaturedCard/img/heart.png')} style={{...HeartImage}} resizeMode='contain'/>
                <Text style={{...ShortListBtnText}}>{strings('profile.shortList')}</Text>
            </TouchableOpacity>
        </View>
        <View style={{flex: 1,height: hp(8),justifyContent:'center'}}>
            <View style={{backgroundColor:'#FAF7F7',height: 2}}></View>
            <TouchableOpacity style={{...MoreDetailBtnView}} onPress={() => this.props.navigation.navigate('creditCardDetail', { item: item})}>
                <Text style={{...MoreDetailBtnText}}>More Details</Text>
                <Image source={require('../FeaturedCard/img/chevron-right.png')} style={{...RightBtnImage}} resizeMode='contain'/>
            </TouchableOpacity>
        </View>
    
    </View>
      );
  }

    render() {
        const { WayHeaderView,container,wayNavigationItem,headerTitle,WayRightNavigationItem} = styles;
        const {CreditCardTitleText,} = cardstyles;
          return (
          
          <View style={container} >
            {/*header view*/}
            <View style={WayHeaderView}>
               <View style={{flexDirection:'row',paddingLeft:10}}>
               <TouchableOpacity style={wayNavigationItem} onPress={()=>this.handleBackButtonClick()}>
                    <Image resizeMode='contain' source={require('../../component/img/backicon.png')} style={{width: wp(5), height: hp(2.2)}}/>
               </TouchableOpacity>
               <Text style={{...headerTitle,padding:5}}>{strings('profile.featuredCard')}</Text>
               </View>
               <View style={{flexDirection:'row',paddingRight:10}}>
               <TouchableOpacity style={wayNavigationItem} >
                    <Image source={require('../../component/img/question-icon.png')} style={WayRightNavigationItem} resizeMode='contain'/>
               </TouchableOpacity>
               <TouchableOpacity style={wayNavigationItem} >
                    <Image source={require('../../component/img/avatar-icon.png')} style={WayRightNavigationItem} resizeMode='contain'/>
               </TouchableOpacity>
               </View>
           
            </View>
           
             {/*card List*/}
                <Text style={CreditCardTitleText}>{strings('profile.FeaturedCardtohelpyousave')}</Text>
               
                <FlatList
                        style={{backgroundColor: Constant.color.white}}
                        contentContainerStyle= {{backgroundColor: Constant.color.white}}
                        showsVerticalScrollIndicator={false}
                        data={this.state.dataSource}
                        renderItem={({item,  index}) => 
                           this.CreditCardListItem(item)
                        }
                        keyExtractor={(item, index) => index.toString()}
                    />
          </View> 
          
         
    )};

};