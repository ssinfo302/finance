import React, { Component } from 'react'
import Constant from '../../helper/themeHelper';
import { View,Text, Image,FlatList,ScrollView} from 'react-native'
import { wp, hp } from '../../helper/responsiveScreen';
import styles from '../../component/styles';
import cardstyles from '../FeaturedCard/styles';
import Creditcardstyles from '../CreditCard/styles';
import insurancestyles from '../Insurance/styles';
import { strings } from '../../component/translation/i18n';
import { TouchableOpacity, TouchableNativeFeedback } from 'react-native-gesture-handler'
import InvestmentJson from './investmentdata.json';


const InvestmentFilter = ["All","CPF", "No Minimum"];

export default class investmentList extends React.Component {

    constructor(props) {
      super(props);
        this.state = {
            dataSource: [],    
            selectedItem: 0
         }
    }

    componentDidMount() {
        this.setState({ dataSource: InvestmentJson });
      }

      _choosen(isSelected) {
        
        this.setState({ selectedItem : isSelected });
      }
      _categorySelected = () => {
        var { InvestmentFilter } = this.state;
       
      }
      // key feature list UI
      InvestmentFilterListItem =(item,index)=>{
        const { filtertextView,filterText }  = Creditcardstyles;
        const isSelected = (this.state.selectedItem === index);
    
        let backgroundColor = isSelected ? Constant.color.selectedtextColor : Constant.color.white;
        let fontColor = isSelected ? Constant.color.white : Constant.color.black;
        let borderColor = isSelected ? Constant.color.selectedtextColor : "#AEB7C8"
        
        return(
            <TouchableOpacity selected={this.state.selected} onPress={() => this._choosen(index)}>
            <View style={{backgroundColor: backgroundColor,borderColor: borderColor,...filtertextView}}>
             <Text style={{color:fontColor,...filterText}}>{item}</Text>
            </View>
            </TouchableOpacity>
           
        )
      }

//Back button click event
handleBackButtonClick() {
    this.props.route.params.onGoBack();
    this.props.navigation.goBack();
    return true;
  }
;


  //Credit Card list UI
  InvestmentCardListItem =(item,index)=>{

    const {CardNameText,sumText,Ammounttext} = insurancestyles;
    const {  MoreDetailBtnText,HeartImage,MoreDetailBtnView,ShortListBtnText,RightBtnImage,HightLightValueTextView,ShortListBtnView,ApplyBtnText,CustomMsgValuetext, ApplyBtnView,CreditCardView,CreditCardBtnBgView,CreditCardImageView,CreditCardNameText,HighLightText,HightLightSubText,HightLightView,CreditCardSubtext} = cardstyles;
   
    return(
        <View style={{...CreditCardView}}>
            
        <View style={{...CreditCardImageView,paddingTop: hp(1)}}>
            <Image source={require('./img/stash.png')} style={{alignSelf:'center',height: hp(10) ,width: wp(60)}} resizeMode='contain'/>
            <Text style={{...CardNameText}}>{item.name}</Text>
        </View>
       
        <View style={{flexDirection: 'row',backgroundColor: "#FAF7F7",justifyContent:'center'}} >
            
            <View style={{...HightLightView,paddingTop: hp(0.5)}}>
                <View style={{...HightLightValueTextView}}>
                <Text style={{...HighLightText}}>{item.DATA.HIGHLIGHTS.Value1}</Text>
                </View>
             
                <Text style={{...HightLightSubText}}>{item.DATA.HIGHLIGHTS.Label1}</Text>
            </View>
        
            <View style={{...HightLightView,paddingTop: hp(0.5)}}>
                 <View style={{...HightLightValueTextView}}>
                <Text style={{...HighLightText}}>{item.DATA.HIGHLIGHTS.Value2}</Text>
                </View>
                <Text style={{...HightLightSubText}}>{item.DATA.HIGHLIGHTS.Label2}</Text>
            </View>
        
            <View style={{...HightLightView,paddingTop: hp(0.5)}}>
            <View style={{...HightLightValueTextView}}>
                <Text style={{...HighLightText}}>{item.DATA.HIGHLIGHTS.Value3}</Text>
            </View>
                <Text style={{...HightLightSubText}}>{item.DATA.HIGHLIGHTS.Label3}</Text>
            </View>
        </View>
        <View style={{...CreditCardBtnBgView,marginTop: (Platform.OS) == 'ios' ? hp(1.2): hp(2)}}>
            <TouchableOpacity style={{...ApplyBtnView}} onPress={() => this.props.navigation.navigate('investmentDetail', { item: item})}>
                <Text style={{...ApplyBtnText}}>{strings('profile.MoreDetails')}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{...ShortListBtnView}}>
                <Image source={require('../FeaturedCard/img/heart.png')} style={{...HeartImage}} resizeMode='contain'/>
                <Text style={{...ShortListBtnText}}>{strings('profile.shortList')}</Text>
            </TouchableOpacity>
        </View>
           
    </View>
      );
  }

    render() {
        const { filterView }  = Creditcardstyles;
        const { WayHeaderView,container,wayNavigationItem,headerTitle,WayRightNavigationItem} = styles;
       
          return (
          
          <View style={container} >
            {/*header view*/}
            <View style={WayHeaderView}>
               <View style={{flexDirection:'row',paddingLeft:10}}>
               <TouchableOpacity style={wayNavigationItem} onPress={()=>this.handleBackButtonClick()}>
                    <Image resizeMode='contain' source={require('../../component/img/backicon.png')} style={{width: wp(5), height: hp(2.2)}}/>
               </TouchableOpacity>
               <Text style={{...headerTitle,padding:5}}>{strings('profile.investmentCard')}</Text>
               </View>
               <View style={{flexDirection:'row',paddingRight:10}}>
               <TouchableOpacity style={wayNavigationItem} >
                    <Image source={require('../../component/img/question-icon.png')} style={WayRightNavigationItem} resizeMode='contain'/>
               </TouchableOpacity>
               <TouchableOpacity style={wayNavigationItem} >
                    <Image source={require('../../component/img/avatar-icon.png')} style={WayRightNavigationItem} resizeMode='contain'/>
               </TouchableOpacity>
               </View>
           
            </View>
                 {/*filter list*/}
                 <View style={{...filterView}}>
                      <FlatList
                        showsHorizontalScrollIndicator={false}
                        horizontal={true}
                        data={InvestmentFilter}
                        renderItem={({item,  index}) => 
                           this.InvestmentFilterListItem(item,index)
                        }
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
                {/*card List*/}
                <FlatList
                        showsVerticalScrollIndicator={false}
                        data={this.state.dataSource}
                        renderItem={({item,  index}) => 
                           this.InvestmentCardListItem(item,index)
                        }
                        keyExtractor={(item, index) => index.toString()}
                    />
          </View> 
          
         
    )};

};