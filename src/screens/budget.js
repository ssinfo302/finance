/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import {
  ScrollView,
  View,
  Text,
  Image,
  TouchableOpacity,
  FlatList
} from 'react-native';
import styles from '../component/styles'
import { wp, hp } from '../helper/responsiveScreen';
import Constant from '../helper/themeHelper';
import { strings } from '../component/translation/i18n';
import { PieChart } from 'react-native-svg-charts'
import { G, Text as ChartText } from "react-native-svg";
import { ModalDropdown } from '../common/modalDropdown';

class Budget extends Component {

  constructor(props) {
    super(props);
    this.state = {
      BudgetData: [{ title: 'Needs', color: '#2AB98F' }, { title: 'Wants', color: '#FABE00' }, { title: 'Goals', color: '#44A6F2' }],
      Chartdata: [
        {
          key: 1,
          amount: 50,
          value: 'Needs',
          svg: { fill: '#2AB98F' },
          arc: { outerRadius: '100%', padAngle: 0 }
        },
        {
          key: 2,
          amount: 30,
          value: 'Wants',
          svg: { fill: '#FABE00' },
          arc: { outerRadius: '100%', padAngle: 0 }
        },
        {
          key: 3,
          amount: 20,
          value: 'Goals',
          svg: { fill: '#44A6F2' },
          arc: { outerRadius: '100%', padAngle: 0 }
        },

      ],
      selectedType: "50-30-20",
      dropdownData: [{
        value: '50-30-20',
      }, {
        value: '30-30-40',
      }, {
        value: '40-40-20',
      }]
    }
  }

  componentDidMount() {
  }

  renderRow = ({ index, item }) => {
    const { cardView, listTitle, nextButton } = styles;
    return (
      <TouchableOpacity onPress={() => this.props.navigation.navigate('BudgetList', { title: item.title })} style={{ ...cardView, backgroundColor: item.color, justifyContent: 'space-between' }}>
        <Text style={{ ...listTitle }}>{item.title}</Text>
        <View>
          <Image style={{ width: wp(5), height: hp(2) }} resizeMode='contain' source={require('../component/img/forward.png')}></Image>
        </View>
      </TouchableOpacity>
    )
  };

  renderDropdownRow = (item, index) => {

    return (
      <View style={{ backgroundColor: Constant.color.white }}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Text style={{ color: Constant.color.black, fontSize: Constant.fontSize.xsmall, padding: 5, textAlign: 'center' }}>{item.value}</Text>
        </View>
        <View style={{ width: '100%', height: hp(0.2), backgroundColor: Constant.color.gray }} />
      </View>
    )
  }

  render() {
    const { container, title, headerView, headerTitle, topView, dropdownContainerStyle } = styles;
    const { BudgetData, Chartdata, selectedType, dropdownData } = this.state;

    const Labels = ({ slices, height, width }) => {
      return slices.map((slice, index) => {
        const { labelCentroid, pieCentroid, data } = slice;
        return (
          <G
            key={index}
            x={labelCentroid[0]}
            y={labelCentroid[1]}
          >
            <ChartText
              y={0}
              fontSize={Constant.fontSize.small}
              fontWeight={'bold'}
              fontFamily={'Lato-Regular'}
              textAnchor={'middle'}
              stroke={'white'}
              fill={'white'}>{`${data.amount}%`}</ChartText>
            <ChartText
              y={17}
              fontSize={Constant.fontSize.small}
              fontWeight={'normal'}
              fontFamily={'Lato-Regular'}
              textAnchor={'middle'}
              stroke={'white'}
              fill={'white'}
              strokeWidth={0.2}>{data.value}</ChartText>
          </G>
        )
      })
    }

    return ( 

      <View style={container} showsVerticalScrollIndicator={false} contentContainerStyle={{ paddingBottom: hp(2) }}>

        <View style={headerView}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Image style={{ width: wp(5), height: hp(2.2) }} resizeMode='contain' source={require('../component/img/backicon.png')}></Image>
            <Text style={headerTitle}>{strings('profile.BudgetTitle')}</Text>
          </View>
        </View>
        <ScrollView >
          <View style={topView}>
            <Text style={{ ...title, color: Constant.color.darkGray, marginTop: hp(0) }}>{strings('profile.BudgetRule')}</Text>
            <View style={{ marginTop: hp(1), marginBottom: Constant.isIOS ? hp(4) : hp(2), flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
              <ModalDropdown ref={e => this["dropdownRef1"] = e}
                options={dropdownData}
                style={dropdownContainerStyle}
                defaultValue={selectedType === '' && 'Select Type' || selectedType}
                textStyle={{
                  ...styles.dropdownFontStyle,
                }}
                dropdownStyle={[styles.dropDownStyle]}
                adjustFrame={(style) => {
                  style.top -= Constant.isIOS ? -hp(0.6) : -hp(1)
                  style.left -= wp(4);
                  return style;
                }}
                onSelect={(selectedIndex, value) => {
                  this.setState({ selectedType: value }, () => {
                    let data = this.state.selectedType.value.split("-")
                    this.state.Chartdata[0].amount = data[0]
                    this.state.Chartdata[1].amount = data[1]
                    this.state.Chartdata[2].amount = data[2]
                    this.forceUpdate()
                  })
                }}
                renderSeparator={() => <View />}
                renderRow={(item, index) => this.renderDropdownRow(item, index)}
                keyboardShouldPersistTaps={'always'}
              >
              </ModalDropdown>

              <TouchableOpacity onPress={() => this.dropdownRef1.show()}>
                <Image style={{ width: wp(5), height: hp(2), marginLeft: wp(1) }} resizeMode='contain' source={require('../component/img/edit.png')}></Image>
              </TouchableOpacity>
            </View>
            <PieChart
              style={{ height: Constant.isIOS ? (Constant.isX ? hp(28) : hp(33)) : hp(30) }}
              valueAccessor={({ item }) => item.amount}
              data={Chartdata}
              spacing={0}
              outerRadius={'99%'}
              innerRadius={'0%'}
            >
              <Labels />
            </PieChart>
          </View>

          <Text style={{ ...title, color: Constant.color.darkGray, marginTop: hp(14) }}>{strings('profile.BudgetCat')}</Text>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={BudgetData}
            renderItem={this.renderRow}
            keyExtractor={(item, index) => {
              return index + "";
            }}
            contentContainerStyle={{ paddingVertical: hp(1) }}
            style={{ paddingHorizontal: wp(3) }}
          />

        </ScrollView>
      </View>
    );
  }
}

export default Budget;
