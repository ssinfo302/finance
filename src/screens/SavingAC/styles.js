import {
    StyleSheet
  } from 'react-native';
  import Constant from '../../helper/themeHelper';
  import { wp, hp } from '../../helper/responsiveScreen';

  const styles = StyleSheet.create({
      savingsACCardView:{
        marginHorizontal: wp(2.5),
        marginTop: hp(0.5),
        marginBottom: hp(2),
        borderRadius:20,
        borderColor: Constant.color.creditCardShadowColor,
        borderWidth: 0,
        backgroundColor: Constant.color.white,
        ...Constant.shadowStyle,
        shadowColor:Constant.color.creditCardShadowColor,
      },
  savingImageView:{
    flex:1, 
    paddingTop: Constant.isX ? hp(1.5) : hp(1),
    paddingBottom: hp(0.2) ,
    paddingHorizontal:hp(1.5),
    alignItems:'center',
    flexDirection:'row'
    
  }
});

export default styles;