import React, { Component } from 'react'
import Constant from '../../helper/themeHelper';
import { View,Text, Image,FlatList,ScrollView} from 'react-native'
import { wp, hp } from '../../helper/responsiveScreen';
import styles from '../../component/styles';
import Creditcardstyles from '../../screens/CreditCard/styles';
import cardstyles from '../../screens/FeaturedCard/styles';
import savingsACStyles from '../../screens/SavingAC/styles';
import { strings } from '../../component/translation/i18n';
import { TouchableOpacity, TouchableNativeFeedback } from 'react-native-gesture-handler'
import savingCardJson from '../../screens/SavingAC/SavingsAccount.json';

const SavingsList = ["All","Savings","Bonus Interest","Salary Crediting"];

export default class savingAC extends React.Component {

    constructor(props) {
      super(props);
        this.state = {
            dataSource: [],
            selectedItem: 0
         }
    }

    componentDidMount() {
        this.setState({ dataSource: savingCardJson });
      }
//Back button click event
handleBackButtonClick() {
    this.props.route.params.onGoBack();
    this.props.navigation.goBack();
    return true;
  }
;
//   };
_choosen(isSelected) {
    console.log('....',isSelected);
    this.setState({ selectedItem : isSelected });
  }
  _categorySelected = () => {
    var { keyFeature } = this.state;
    console.log(keyFeature);
  }
  // key feature list UI
  SavingsListItem =(item,index)=>{
    const { filtertextView,filterText }  = Creditcardstyles;
    const isSelected = (this.state.selectedItem === index);

    let backgroundColor = isSelected ? Constant.color.selectedtextColor : Constant.color.white;
    let fontColor = isSelected ? Constant.color.white : Constant.color.black;
    let borderColor = isSelected ? Constant.color.selectedtextColor : "#AEB7C8"
    
    return(
        <TouchableOpacity selected={this.state.selected} onPress={() => this._choosen(index)}>
        <View style={{backgroundColor: backgroundColor,borderColor: borderColor,...filtertextView}}>
         <Text style={{color:fontColor,...filterText}}>{item}</Text>
        </View>
        </TouchableOpacity>
       
    )
  }
  //Credit Card list UI
  SavingCardListItem =(item,index)=>{
      const {savingImageView} = savingsACStyles
    const {  MoreDetailBtnText,HeartImage,MoreDetailBtnView,ShortListBtnText,RightBtnImage,HightLightValueTextView,ShortListBtnView,ApplyBtnText,CustomMsgValuetext, ApplyBtnView,CreditCardView,CreditCardBtnBgView,CreditCardImageView,CreditCardNameText,HighLightText,HightLightSubText,HightLightView,CreditCardSubtext} = cardstyles;
    return(
        <View style={{...CreditCardView}}>
            
        <View style={{...CreditCardImageView}}>
            <Image source={require('./img/dbs_logo.png')} style={{alignSelf:'center',height: hp(5) ,width: wp(30),}} resizeMode='cover'/>
            <Text style={{...CreditCardNameText}}>{item.Name}</Text>
        </View>
        <View style={{flexDirection: 'row',backgroundColor: "#FAF7F7",justifyContent:'center'}} >
            
            <View style={{...HightLightView,paddingTop: hp(0.5)}}>
                <View style={{...HightLightValueTextView}}>
                <Text style={{...HighLightText}}>{item.DATA.HIGHLIGHTS.Value1}</Text>
                </View>
             
                <Text style={{...HightLightSubText}}>{item.DATA.HIGHLIGHTS.Label1}</Text>
            </View>
        
            <View style={{...HightLightView,paddingTop: hp(0.5)}}>
                 <View style={{...HightLightValueTextView}}>
                <Text style={{...HighLightText}}>{item.DATA.HIGHLIGHTS.Value2}</Text>
                </View>
                <Text style={{...HightLightSubText}}>{item.DATA.HIGHLIGHTS.Label2}</Text>
            </View>
        
            <View style={{...HightLightView,paddingTop: hp(0.5)}}>
            <View style={{...HightLightValueTextView}}>
                <Text style={{...HighLightText}}>{item.DATA.HIGHLIGHTS.Value3}</Text>
            </View>
                <Text style={{...HightLightSubText}}>{item.DATA.HIGHLIGHTS.Label3}</Text>
            </View>
        </View>
        <View style={{...CreditCardBtnBgView,marginTop: (Platform.OS) == 'ios' ? hp(1.2): hp(2)}}>
            <TouchableOpacity style={{...ApplyBtnView}}>
                <Text style={{...ApplyBtnText}}>{strings('profile.applyNow')}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{...ShortListBtnView}}>
                <Image source={require('../FeaturedCard/img/heart.png')} style={{...HeartImage}} resizeMode='contain'/>
                <Text style={{...ShortListBtnText}}>{strings('profile.shortList')}</Text>
            </TouchableOpacity>
        </View>
        <View style={{flex: 1,height: hp(8)}}>
            <View style={{backgroundColor:'#FAF7F7',height: 2}}></View>
            <TouchableOpacity style={{...MoreDetailBtnView}} onPress={() => this.props.navigation.navigate('savingCardDetail', { item: item})}>
                <Text style={{...MoreDetailBtnText}}>More Details</Text>
                <Image source={require('../FeaturedCard/img/chevron-right.png')} style={{...RightBtnImage}} resizeMode='contain'/>
            </TouchableOpacity>
        </View>
    
    </View>
      );
  }

    render() {
        const { filterView }  = Creditcardstyles;
        const { WayHeaderView,container,wayNavigationItem,headerTitle,WayRightNavigationItem} = styles;
        const {CreditCardTitleText,} = cardstyles;
          return (
          
          <View style={container} >
            {/*header view*/}
            <View style={WayHeaderView}>
               <View style={{flexDirection:'row',paddingLeft:10}}>
               <TouchableOpacity style={wayNavigationItem} onPress={()=>this.handleBackButtonClick()}>
                    <Image resizeMode='contain' source={require('../../component/img/backicon.png')} style={{width: wp(5), height: hp(2.2)}}/>
               </TouchableOpacity>
               <Text style={{...headerTitle,padding:5}}>{strings('profile.savingCard')}</Text>
               </View>
               <View style={{flexDirection:'row',paddingRight:10}}>
               <TouchableOpacity style={wayNavigationItem} >
                    <Image source={require('../../component/img/question-icon.png')} style={WayRightNavigationItem} resizeMode='contain'/>
               </TouchableOpacity>
               <TouchableOpacity style={wayNavigationItem} >
                    <Image source={require('../../component/img/avatar-icon.png')} style={WayRightNavigationItem} resizeMode='contain'/>
               </TouchableOpacity>
               </View>
           
            </View>
                {/*filter list*/}
                <View style={{...filterView}}>
                      <FlatList
                        showsHorizontalScrollIndicator={false}
                        horizontal={true}
                        data={SavingsList}
                        renderItem={({item,  index}) => 
                           this.SavingsListItem(item,index)
                        }
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
                {/*card List*/}
                <FlatList
                        showsVerticalScrollIndicator={false}
                        data={this.state.dataSource}
                        renderItem={({item,  index}) => 
                           this.SavingCardListItem(item,index)
                        }
                        keyExtractor={(item, index) => index.toString()}
                    />
          </View> 
          
         
    )};

};