import {
    StyleSheet
  } from 'react-native';
  import Constant from '../../helper/themeHelper';
  import { wp, hp } from '../../helper/responsiveScreen';

  const styles = StyleSheet.create({
   
    HeaderListText:{
      flex: 1,
      color: Constant.color.textgray,
       fontSize: Constant.fontSize.small, 
       fontWeight: 'bold', 
       fontFamily: "Lato-Bold",
        paddingVertical: hp(1) ,
        textAlign:'left'
    }
});

export default styles;