import React, { Component } from 'react'
import Constant from '../../helper/themeHelper';
import { View, Text, Image, FlatList, ScrollView,SectionList } from 'react-native'
import { wp, hp } from '../../helper/responsiveScreen';
import styles from '../../component/styles';
import savingsACStyles from '../../screens/SavingAC/styles';
import Creditcardstyles from '../../screens/FeaturedCard/styles';
import savingDetailStyle from '../../screens/SavingCardDetail/styles';
import creditCardDetailStyles from '../../screens/CreditCardDetail/styles';
import { strings } from '../../component/translation/i18n';
import NumberFormate from '../../common/NumberFormate';
import { TouchableOpacity, TouchableNativeFeedback } from 'react-native-gesture-handler'

export default class savingCardDetail extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      Index: this.props.route.params.index,
      CardDetail: this.props.route.params.item,
      isKeyFeature: true,
      iseligibility: false,
      isinterest: false,
      isfees: false,
    }
  }

  componentDidMount() {
    console.log("....index", this.state.CardDetail);
  }

  //Back button click event
  handleBackButtonClick() {
    this.props.navigation.goBack()
    return true;
  };

  handleKeyFeatureSelection(value) {
    this.setState({ isKeyFeature: value });
    this.setState({ iseligibility: false });
    this.setState({ isinterest: false });
    this.setState({ isfees: false });
    }
  handleEligibilitySelection(value) {
    this.setState({ isKeyFeature: false });
    this.setState({ iseligibility: value });
    this.setState({ isinterest: false });
    this.setState({ isfees: false });
  }
  handlInterestFeature(value) {
    this.setState({ isKeyFeature: false });
    this.setState({ iseligibility: false });
    this.setState({ isinterest: value });
    this.setState({ isfees: false });
  }
  handleFeesFeature(value) {
    this.setState({ isKeyFeature: false });
    this.setState({ iseligibility: false });
    this.setState({ isinterest: false });
    this.setState({ isfees: value });

  }

  // interest list UI
  KeyInterestListItem = (item, index) => {
    const {sepratorView,DetailText,labelText} = creditCardDetailStyles;
    return (
      
      <View style={{justifyContent:'center'}}>
        <View style={{flex: 1,justifyContent:'space-between',flexDirection:'row',alignItems: 'flex-start' }}>
             <Text style={{...labelText,flex: 0.7}}>{item.Balance == "" ||item.Balance == "null" ? "-" : item.Balance}</Text>
          <Text style={{...DetailText,flex: 0.3}}>{(item.Interest == "" ||item.Interest == "null") ? '-' : item.Interest}</Text>
        </View>
        
        {/* <View style={{...sepratorView}}></View> */}
      </View>

    )
  }

  InterestFooter = () => {
    const {sepratorView} = creditCardDetailStyles;
    return ( 
      // <View style={{justifyContent:'center',paddingVertical: hp(0.5)}}>
            <View style={{...sepratorView}}></View>
             
      // </View>
    )
  }
  InterestHeader = (section,index) =>{
    const {HeaderListText} = savingDetailStyle;
  
    return ( 
      <View style={{justifyContent:'center',paddingVertical: hp(0.5)}}>

             <Text style={{...HeaderListText}}>{'Salary credit + ' + 1 + ' transaction category'}</Text>
             
      </View>
    )
  }
   // elegibility list UI
   ElegibilityListItem = (item, index) => {
    const {sepratorView,DetailText,labelText} = creditCardDetailStyles;

    var value = item.value
   
    if (item.value)
    {
      if (value.includes('$'))
      {
        value = (value).replace('$', '');
      }
    }
  
    return (
      
      <View style={{justifyContent:'center'}}>
        <View style={{flex: 1,justifyContent:'space-between',flexDirection:'row',alignItems: 'flex-start',paddingVertical: hp(0.5) }}>
             <Text style={{...labelText}}>{(item.label) ? item.label == "" ||item.label == "null" ? "-" : item.label : ""}</Text>
          <Text style={{...DetailText}}>{ (item.value) ? (item.value == "" ||item.value == "null") ? '-' : item.value.includes(',') ? item.value : (item.value == value ?  item.value : ((isNaN(parseFloat(value))) ? item.value :  '$' + NumberFormate.CommaFormatted(parseFloat(value).toFixed(2)))): "-"}</Text>
        </View>
      
        <View style={{...sepratorView}}></View>
      </View>

    )
  }


  render() {
    const {savingImageView} = savingsACStyles;
    const { } = creditCardDetailStyles;
    const { WayHeaderView, container, wayNavigationItem, headerTitle, WayRightNavigationItem } = styles;
    const {updownimg,DetailFeatureView, FeatureTitleText, updownImgView,sepratorView,DetailText,Conditiontext,hypertext,AddFeatureDetailText} = creditCardDetailStyles;
    const { HightLightValueTextView, CreditCardView, CreditCardImageView, ShortListBtnText,CreditCardNameText,HeartImage, HighLightText,ShortListBtnView, HightLightSubText, HightLightView,CreditCardBtnBgView,ApplyBtnView,ApplyBtnText} = Creditcardstyles;

    var detail = this.state.CardDetail;
    var keyFeatureDATA = detail.DATA.KEY_FEATURES.DATA;
    return (

      <View style={container} >
        {/*header view*/}
        <View style={WayHeaderView}>
          <View style={{ flexDirection: 'row', paddingLeft: 10 }}>
            <TouchableOpacity style={wayNavigationItem} onPress={() => this.handleBackButtonClick()}>
              <Image resizeMode='contain' source={require('../../component/img/backicon.png')} style={{ width: wp(5), height: hp(2.2) }} />
            </TouchableOpacity>
            <Text style={{ ...headerTitle, padding: 5 }}>{detail.Name}</Text>
          </View>
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
        {/* card view */}
        <View style={{ ...CreditCardView, marginBottom: hp(1) }}>
        <View style={{...CreditCardImageView}}>
            <Image source={require('../SavingAC/img/dbs_logo.png')} style={{alignSelf:'center',height: hp(5) ,width: wp(30),}} resizeMode='cover'/>
            <Text style={{...CreditCardNameText}}>{detail.Name}</Text>
        </View>
          <View style={{ flexDirection: 'row', backgroundColor: "#FAF7F7", justifyContent: 'center', marginBottom: hp(2) }} >

            <View style={{ ...HightLightView }}>
              <View style={{ ...HightLightValueTextView }}>
                <Text style={{ ...HighLightText }}>{detail.DATA.HIGHLIGHTS.Value1}</Text>
              </View>

              <Text style={{ ...HightLightSubText }}>{detail.DATA.HIGHLIGHTS.Label1}</Text>
            </View>

            <View style={{ ...HightLightView }}>
              <View style={{ ...HightLightValueTextView }}>
                <Text style={{ ...HighLightText }}>{detail.DATA.HIGHLIGHTS.Value2}</Text>
              </View>
              <Text style={{ ...HightLightSubText }}>{detail.DATA.HIGHLIGHTS.Label2}</Text>
            </View>

            <View style={{ ...HightLightView }}>
              <View style={{ ...HightLightValueTextView }}>
                <Text style={{ ...HighLightText }}>{detail.DATA.HIGHLIGHTS.Value3}</Text>
              </View>
              <Text style={{ ...HightLightSubText }}>{detail.DATA.HIGHLIGHTS.Label3}</Text>
            </View>
          </View>

        </View>
        {/*savings card Detail*/}
        <View style={{ marginHorizontal: wp(2.5) }}>
          {/* key feature */}
          <View>
            <TouchableOpacity style={{...DetailFeatureView}} onPress={() => this.state.isKeyFeature == true ? this.handleKeyFeatureSelection(false) : this.handleKeyFeatureSelection(true)}>
              <Text style={{...FeatureTitleText }}>{(detail.DATA.KEY_FEATURES.TITLE) ? detail.DATA.KEY_FEATURES.TITLE : "Additional Features"}</Text>

              <View style={updownImgView}>
                <Image style={updownimg} resizeMode='contain' source={ this.state.isKeyFeature == false ? require('../CreditCardDetail/img/downarrow.png'): require('../CreditCardDetail/img/uparrow.png')}></Image>
              </View>
            </TouchableOpacity>
          </View>
          {
            this.state.isKeyFeature == true ?
            <View>
                <View style={{...sepratorView,marginHorizontal: wp(2)}}></View>
              {
                Object.entries(keyFeatureDATA).map(([key, value]) => {

                  if (value)
                  {
                    if (value == 'null' || value == '') {

                    }
                    else {
                      return (
                        <View style={{ justifyContent: 'center', marginHorizontal: wp(2.5) }}>
                          <View style={{flexDirection: 'row',alignItems: 'flex-start',flexWrap: 'wrap', flex: 1}}> 
                            <View style={{width: 20,paddingVertical: hp(1)}}>
                                <Text style={{fontWeight: 'bold',fontFamily: "Lato-Bold"}}>{'\u2022'}</Text>
                             </View>
                           <Text style={{...AddFeatureDetailText,flex: 0.95}}>{value}</Text>
                          </View>
                           
                          <View style={{...sepratorView }}></View>
                        </View>
                         )
                    }
                  }
                  
                })
              }

            </View>
            : <View style={{ ...sepratorView ,marginHorizontal: wp(2)}}></View>
          }
          {/* Intetest */}
          <View>
            <TouchableOpacity style={{...DetailFeatureView}} onPress={() => this.state.isinterest == true ? this.handlInterestFeature(false) : this.handlInterestFeature(true)}>
              <Text style={{...FeatureTitleText}}>{(detail.DATA.INTEREST.TITLE) ? detail.DATA.INTEREST.TITLE == "" || detail.DATA.INTEREST.TITLE == undefined ? 'Account Balance & Interest' : detail.DATA.INTEREST.TITLE : "Account Balance & Interest"}</Text>
              <View style={updownImgView}>
                <Image style={updownimg} resizeMode='contain' source={this.state.isinterest == false ? require('../CreditCardDetail/img/downarrow.png'): require('../CreditCardDetail/img/uparrow.png')}></Image>
              </View>
            </TouchableOpacity>
         
          </View>
          {
            this.state.isinterest == true ?
              <View>
                <View style={{...sepratorView,marginHorizontal: wp(2)}}></View>
                <SectionList
                     sections={[{"Category1": "", "data": [{ "Balance": "Base","Interest": "0.05%"},{"Balance": "First $35,000 \n(Salary Credit)","Interest": "1.2%" },{"Balance": "First $35,000 \n(Spend with OCBC Cards)","Interest": "0.2%"},{
                      "Balance": "First $35,000 \n(Increase in average daily balance by at least $500)",
                      "Interest": "0.2%"
                    },
                    {
                      "Balance": "First $35,000 (Insure or Invest in eligible wealth products)",
                      "Interest": "0.6%"
                    },
                    {
                      "Balance": "Next $35,000 \n(Salary Credit)",
                      "Interest": "2.4%"
                    },]},{"Category1": "", "data": [{ "Balance": "Base","Interest": "0.05%"},{
                      "Balance": "Next $35,000 (Spend with OCBC Cards)",
                      "Interest": "0.4%"
                    },
                    {
                      "Balance": "Next $35,000 \n(Increase in average daily balance by at least $500)",
                      "Interest": "0.4%"
                    },
                    {
                      "Balance": "Next $35,000 \n(Insure or Invest in eligible wealth products)",
                      "Interest": "1.2%"
                    }]}]}
                  contentContainerStyle={{ marginHorizontal: wp(2.5) }}
                  showsVerticalScrollIndicator={false}
                    renderSectionFooter={() => this.InterestFooter()}
                  renderItem={({ item, index }) =>
                    this.KeyInterestListItem(item, index)
                  }
                  renderSectionHeader={({section,index}) => this.InterestHeader(section,index)}  
                  keyExtractor={(item, index) => index}
                />
              </View>
              : <View style={{...sepratorView ,marginHorizontal: wp(2)}}></View>
          }
          {/* ELIGIBILITY_FEES */}
          <View>
            <TouchableOpacity style={{...DetailFeatureView}} onPress={() => this.state.iseligibility == true ? this.handleEligibilitySelection(false) : this.handleEligibilitySelection(true)}>
              <Text style={{...FeatureTitleText}}>{(detail.DATA.ELIGIBILITY.TITLE) ? detail.DATA.ELIGIBILITY.TITLE : "Eligibility & Requirements"}</Text>

              <View style={updownImgView}>
                <Image style={updownimg} resizeMode='contain' source={this.state.iseligibility == false ? require('../CreditCardDetail/img/downarrow.png'): require('../CreditCardDetail/img/uparrow.png')}></Image>
              </View>
            </TouchableOpacity>
          </View>
          {
            this.state.iseligibility == true ?
              <View>
                 <View style={{...sepratorView,marginHorizontal: wp(2)}}></View>
                <FlatList
                  contentContainerStyle={{ marginHorizontal: wp(2.5) }}
                  showsVerticalScrollIndicator={false}
                  data={detail.DATA.ELIGIBILITY.DATA}
                  renderItem={({ item, index }) =>
                    this.ElegibilityListItem(item, index)
                  }
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
              : <View style={{...sepratorView ,marginHorizontal: wp(2)}}></View>
          }

          {/* FEES */}
          <View>
            <TouchableOpacity style={{...DetailFeatureView}} onPress={() => this.state.isfees == true ? this.handleFeesFeature(false) : this.handleFeesFeature(true)}>
              <Text style={{...FeatureTitleText}}>{(detail.DATA.FEES.TITLE) ? detail.DATA.FEES.TITLE : "Fee"}</Text>

              <View style={updownImgView}>
                <Image style={updownimg} resizeMode='contain' source={this.state.isfees == false ? require('../CreditCardDetail/img/downarrow.png'): require('../CreditCardDetail/img/uparrow.png')}></Image>
              </View>
            </TouchableOpacity>
          </View>
          {
            this.state.isfees == true ?
              <View>
                  <View style={{...sepratorView,marginHorizontal: wp(2)}}></View>
                <FlatList
                  contentContainerStyle={{ marginHorizontal: wp(2.5) }}
                  showsVerticalScrollIndicator={false}
                  data={detail.DATA.FEES.DATA}
                  renderItem={({ item, index }) =>
                    this.ElegibilityListItem(item, index)
                  }
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
              : <View style={{ ...sepratorView ,marginHorizontal: wp(2)}}></View>
          }
        </View>
        <View style={{...CreditCardBtnBgView,marginVertical: hp(2)}}>
            <TouchableOpacity style={{...ApplyBtnView}}>
                <Text style={{...ApplyBtnText}}>{strings('profile.applyNow')}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{...ShortListBtnView}}>
                <Image source={require('../FeaturedCard/img/heart.png')} style={{...HeartImage}} resizeMode='contain'/>
                <Text style={{...ShortListBtnText}}>{strings('profile.shortList')}</Text>
            </TouchableOpacity>
        </View>
        </ScrollView>
    </View>
    )
  };

};