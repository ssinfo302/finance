import React, { Component } from 'react'
import Constant from '../../helper/themeHelper';
import { View,Text, Image,FlatList,ScrollView,ImageBackground} from 'react-native'
import { wp, hp } from '../../helper/responsiveScreen';
import styles from '../../component/styles';
import cardstyles from '../../screens/FeaturedCard/styles';
import Creditcardstyles from '../../screens/CreditCard/styles';
import { strings } from '../../component/translation/i18n';
import { TouchableOpacity, TouchableNativeFeedback } from 'react-native-gesture-handler'
import creditCardJson from '../../screens/CreditCard/CreditCard.json';
import NumberFormate from '../../common/NumberFormate';

const keyfeature = ["All","Miles","Cashback","Transport","Dining","Groceries","shopping","Utilities"];

export default class creditCard extends React.Component {

    constructor(props) {
      super(props);
        this.state = {
            dataSource: [],
            selectedItem: 0
         }
    }

    componentDidMount() {
        this.setState({ dataSource: creditCardJson });
      }
//Back button click event
handleBackButtonClick() {
    this.props.route.params.onGoBack();
    this.props.navigation.goBack();
    return true;
  }
;

_choosen(isSelected) {
    console.log('....',isSelected);
    this.setState({ selectedItem : isSelected });
  }
  _categorySelected = () => {
    var { keyFeature } = this.state;
    console.log(keyFeature);
  }
  // key feature list UI
  KeyFeatureListItem =(item,index)=>{
    const { filtertextView,filterText }  = Creditcardstyles;
    const isSelected = (this.state.selectedItem === index);

    let backgroundColor = isSelected ? Constant.color.selectedtextColor : Constant.color.white;
    let fontColor = isSelected ? Constant.color.white : Constant.color.black;
    let borderColor = isSelected ? Constant.color.selectedtextColor : "#AEB7C8"
    
    return(
        <TouchableOpacity selected={this.state.selected} onPress={() => this._choosen(index)}>
        <View style={{backgroundColor: backgroundColor,borderColor: borderColor,...filtertextView}}>
         <Text style={{color:fontColor,...filterText}}>{item}</Text>
        </View>
        </TouchableOpacity>
       
    )
  }
  //Credit Card list UI
  CreditCardListItem =(item,index)=>{
    const {  MoreDetailBtnText,HeartImage,MoreDetailBtnView,ShortListBtnText,RightBtnImage,HightLightValueTextView,ShortListBtnView,ApplyBtnText,CustomMsgValuetext, ApplyBtnView,CreditCardView,CreditCardBtnBgView,CreditCardImageView,CreditCardNameText,HighLightText,HightLightSubText,HightLightView,CreditCardSubtext} = cardstyles;
   
    var HighlightValue1 = item.DATA.HIGHLIGHTS.Value1
    var HighlightValue2 = item.DATA.HIGHLIGHTS.Value2
    var HighlightValue3 = item.DATA.HIGHLIGHTS.Value3

    if (HighlightValue1)
    {
        if (HighlightValue1.includes('$'))
        {
          HighlightValue1 = (HighlightValue1).replace('$', '');
        }
    }
   
    if (HighlightValue2)
    {
        if (HighlightValue2.includes('$'))
        {
          HighlightValue2 = (HighlightValue2).replace('$', '');
        }
    }
    
    if (HighlightValue3)
    {
        if (HighlightValue3.includes('$'))
        {
          HighlightValue3 = (HighlightValue3).replace('$', '');
        }
    }
   

    return(
        <View style={{...CreditCardView}}>
        <View style={{...CreditCardImageView}}>
          
            <ImageBackground source={require('../FeaturedCard/img/BubbleCardImgBg.png')} style={{alignSelf:'center',width: (Platform.OS) == 'ios' ? wp(73): wp(67)}} resizeMode='contain'>
              <Image source={require('../FeaturedCard/img/card.png')} style={{alignSelf:'center',width: wp(65)}} resizeMode='contain'/>
            </ImageBackground>
              <Text style={{...CreditCardNameText}}>{item.CardName}</Text>
        </View>
        <View style={{flexDirection: 'row',backgroundColor: "#FAF7F7",justifyContent:'center'}} >
            
            <View style={{...HightLightView}}>
                <View style={{...HightLightValueTextView}}>
                <Text style={{...HighLightText}}>{(item.DATA.HIGHLIGHTS.Value1) ? ( item.DATA.HIGHLIGHTS.Value1 == "" || item.DATA.HIGHLIGHTS.Value1 == "null") ? '-' : (item.DATA.HIGHLIGHTS.Value1.includes(',') ? item.DATA.HIGHLIGHTS.Value1 : ( item.DATA.HIGHLIGHTS.Value1 == HighlightValue1 ?  item.DATA.HIGHLIGHTS.Value1 : ((isNaN(parseFloat(HighlightValue1))) ?  item.DATA.HIGHLIGHTS.Value1 :  '$' + NumberFormate.CommaFormatted(parseFloat(HighlightValue1).toFixed(2))))) : "-"}</Text>
                </View>
             
                <Text style={{...HightLightSubText}}>{item.DATA.HIGHLIGHTS.Label1}</Text>
            </View>
        
            <View style={{...HightLightView}}>
                 <View style={{...HightLightValueTextView}}>
                <Text style={{...HighLightText}}>{(item.DATA.HIGHLIGHTS.Value2) ? ( item.DATA.HIGHLIGHTS.Value2 == "" || item.DATA.HIGHLIGHTS.Value2 == "null") ? '-' : (item.DATA.HIGHLIGHTS.Value2.includes(',') ? item.DATA.HIGHLIGHTS.Value2 : ( item.DATA.HIGHLIGHTS.Value2 == HighlightValue2 ?  item.DATA.HIGHLIGHTS.Value2 : ((isNaN(parseFloat(HighlightValue2))) ?  item.DATA.HIGHLIGHTS.Value2 :  '$' + NumberFormate.CommaFormatted(parseFloat(HighlightValue2).toFixed(2))))) : "-"}</Text>
                </View>
                <Text style={{...HightLightSubText}}>{item.DATA.HIGHLIGHTS.Label2}</Text>
            </View>
        
            <View style={{...HightLightView}}>
            <View style={{...HightLightValueTextView}}>
                <Text style={{...HighLightText}}>{(item.DATA.HIGHLIGHTS.Value3) ? ( item.DATA.HIGHLIGHTS.Value3 == "" || item.DATA.HIGHLIGHTS.Value3 == "null") ? '-' : (item.DATA.HIGHLIGHTS.Value3.includes(',') ? item.DATA.HIGHLIGHTS.Value3 : ( item.DATA.HIGHLIGHTS.Value3 == HighlightValue3 ?  item.DATA.HIGHLIGHTS.Value3 : ((isNaN(parseFloat(HighlightValue3))) ?  item.DATA.HIGHLIGHTS.Value3 :  '$' + NumberFormate.CommaFormatted(parseFloat(HighlightValue3).toFixed(2))))) : "-"}</Text>
            </View>
                <Text style={{...HightLightSubText}}>{item.DATA.HIGHLIGHTS.Label3}</Text>
            </View>
        </View>
        <View style={{...CreditCardBtnBgView,marginTop:(Platform.OS) == 'ios' ? hp(1.2): hp(2)}}>
            <TouchableOpacity style={{...ApplyBtnView}} >
            <Text style={{...ApplyBtnText}}>{strings('profile.applyNow')}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{...ShortListBtnView}}>
                <Image source={require('../FeaturedCard/img/heart.png')} style={{...HeartImage}} resizeMode='contain'/>
                <Text style={{...ShortListBtnText}}>{strings('profile.shortList')}</Text>
            </TouchableOpacity>
        </View>
        <View style={{flex: 1,height: hp(8)}}>
            <View style={{backgroundColor:'#FAF7F7',height: 2}}></View>
            <TouchableOpacity style={{...MoreDetailBtnView}} onPress={() => this.props.navigation.navigate('creditCardDetail', { item: item})}>
                <Text style={{...MoreDetailBtnText}}>More Details</Text>
                <Image source={require('../FeaturedCard/img/chevron-right.png')} style={{...RightBtnImage}} resizeMode='contain'/>
            </TouchableOpacity>
        </View>
    
    </View>
      );
  }

    render() {
        const { filterView }  = Creditcardstyles;
        const { WayHeaderView,container,wayNavigationItem,headerTitle,WayRightNavigationItem} = styles;
        const {CreditCardTitleText,} = cardstyles;
          return (
          
          <View style={container} >
            {/*header view*/}
            <View style={WayHeaderView}>
               <View style={{flexDirection:'row',paddingLeft:10}}>
               <TouchableOpacity style={wayNavigationItem} onPress={()=>this.handleBackButtonClick()}>
                    <Image resizeMode='contain' source={require('../../component/img/backicon.png')} style={{width: wp(5), height: hp(2.2)}}/>
               </TouchableOpacity>
               <Text style={{...headerTitle,padding:5}}>{strings('profile.creditCard')}</Text>
               </View>
               <View style={{flexDirection:'row',paddingRight:10}}>
               <TouchableOpacity style={wayNavigationItem} >
                    <Image source={require('../../component/img/question-icon.png')} style={WayRightNavigationItem} resizeMode='contain'/>
               </TouchableOpacity>
               <TouchableOpacity style={wayNavigationItem} >
                    <Image source={require('../../component/img/avatar-icon.png')} style={WayRightNavigationItem} resizeMode='contain'/>
               </TouchableOpacity>
               </View>
           
            </View>
                {/*filter list*/}
                <View style={{...filterView}}>
                      <FlatList
                        showsHorizontalScrollIndicator={false}
                        horizontal={true}
                        data={keyfeature}
                        renderItem={({item,  index}) => 
                           this.KeyFeatureListItem(item,index)
                        }
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
                {/*card List*/}
                <FlatList
                        showsVerticalScrollIndicator={false}
                        data={this.state.dataSource}
                        renderItem={({item,  index}) => 
                           this.CreditCardListItem(item,index)
                        }
                        keyExtractor={(item, index) => index.toString()}
                    />
          </View> 
          
         
    )};

};