import {
    StyleSheet
  } from 'react-native';
  import Constant from '../../helper/themeHelper';
  import { wp, hp } from '../../helper/responsiveScreen';

  const styles = StyleSheet.create({
    filterView:{
        padding:hp(1),
        marginBottom: hp(1.5),
        marginHorizontal:wp(2.5),
    },
    filtertextView:{
        padding:wp(0.5),
        marginHorizontal: wp(1),
        alignItems:'center',
        borderRadius: 8,
        borderWidth:1
    },
    filterText:{
        fontWeight: 'normal',
        fontFamily: "Lato-Regular",
        paddingHorizontal:hp(1.2),
        paddingVertical:hp(1)
    }
 
});

export default styles;