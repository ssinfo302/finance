import React, { Component } from 'react'
import Constant from '../helper/themeHelper';
import { View, Text, Image, FlatList, ScrollView } from 'react-native'
import { wp, hp } from '../helper/responsiveScreen';
import styles from '../component/styles';
import { strings } from '../component/translation/i18n';
import { TouchableOpacity, TouchableNativeFeedback } from 'react-native-gesture-handler'

const topListData = [
  { name: 'Supermarket', bordercolor: '#E2E2E2', textColor: '#4E6BF4', image: require('../component/img/card1.png'), des: "see how you could save $20 on groceries", txtbgColor: 'rgba(218,224,253,1)', },
  { name: 'Do you prefer', bordercolor: 'rgba(166,214,251,1)', textColor: '#2175AA', image: require('../component/img/card2.png'), des: "Miles or Cash-back?", txtbgColor: 'rgba(189,231,253,1)', },
  { name: 'Bubble-Mania!!', bordercolor: 'rgba(251,228,223,1)', textColor: '#FF6232', image: require('../component/img/card3.png'), des: "$2 off your next bubble tea!", txtbgColor: 'rgba(253,224,219,1)', },
  { name: 'Investment Ideas', bordercolor: 'rgba(149,211,215,1)', textColor: '#2BBF25', image: require('../component/img/card4.png'), des: "New ways to Invest!", txtbgColor: '#DAF2DD', },
]

const financeData = [
  { name: "BANKING 1", image: require('../component/img/finance1.png'), offer: "$147", indes: "in Credit Cards" },
  { name: "BANKING 2", image: require('../component/img/finance2.png'), offer: "See Offers", indes: "in Savings" },
  { name: "INVESTMENT 3", image: require('../component/img/finance3.png'), offer: "Invest", indes: "in New Ways" },
  { name: "INSURANCE 4", image: require('../component/img/finance4.png'), offer: "Get Quote", indes: "in Auto" },
  { name: "BANKING 5", image: require('../component/img/finance1.png'), offer: "$147", indes: "in Credit Cards" },
  { name: "BANKING 6", image: require('../component/img/finance2.png'), offer: "See Offers", indes: "in Savings" },
  { name: "INVESTMENT 7", image: require('../component/img/finance3.png'), offer: "Invest", indes: "in New Ways" },
  { name: "INSURANCE 8", image: require('../component/img/finance4.png'), offer: "Get Quote", indes: "in Auto" }

]
const MerchantData = [
  { name: "BubbleTea", image: require('../component/img/merchant1.png'), offer: "Save $2 on Your next order", indes: "Ordered twice in the last month" },
  { name: "Burgers", image: require('../component/img/merchant2.png'), offer: "10% off + Free delivery", indes: "ordered twice in the last month" },
  { name: "Food", image: require('../component/img/merchant3.png'), offer: "Free cookies!", indes: "Ordered twice in the last month" },
  { name: "Bubble Tea", image: require('../component/img/merchant4.png'), offer: "save $2 on your next order", indes: "Ordered twice in the last month" },
  { name: "BubbleTea", image: require('../component/img/merchant1.png'), offer: "Save $2 on Your next order", indes: "Ordered twice in the last month" },
  { name: "Burgers", image: require('../component/img/merchant2.png'), offer: "10% off + Free delivery", indes: "ordered twice in the last month" },
  { name: "Food", image: require('../component/img/merchant3.png'), offer: "Free cookies!", indes: "Ordered twice in the last month" },
  { name: "Bubble Tea", image: require('../component/img/merchant4.png'), offer: "save $2 on your next order", indes: "iOrdered twice in the last month" },
  { name: "BubbleTea", image: require('../component/img/merchant1.png'), offer: "Save $2 on Your next order", indes: "Ordered twice in the last month" },
  { name: "Burgers", image: require('../component/img/merchant2.png'), offer: "10% off + Free delivery", indes: "ordered twice in the last month" },
  { name: "Food", image: require('../component/img/merchant3.png'), offer: "Free cookies!", indes: "Ordered twice in the last month" },
  { name: "Bubble Tea", image: require('../component/img/merchant4.png'), offer: "save $2 on your next order", indes: "Ordered twice in the last month" },
]

export default class waytoSavePage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isSelectedCat: "finance",
      enableScrollViewScroll: true,
      enableListFinaceScroll: false,
      enableListMerchantScroll: false,
      ContentScrollOffset: 0
    }
  }


  refresh() {
  
    this.onEnableScroll(true);
    this.setState({
      enableListMerchantScroll: false,
    });
    this.setState({
      enableListFinaceScroll: false,
    });
  }
  // finance and merchant click event
  SliderClick() {
    if (this.state.isSelectedCat == "finance") {
      this.setState({ isSelectedCat: "merchant" })

    }
    else {
      this.setState({ isSelectedCat: "finance" })
    }
  }

  onEnableScroll = (value) => {
    this.setState({
      enableScrollViewScroll: value,
    });
  };


  handleScroll = (event) => {
    //get offset by using   
     if ((Platform.OS) == 'ios') {
      if (this.state.isSelectedCat == "finance") {
        if (event.nativeEvent.contentOffset.y <= 0) {

          this.setState({
            enableListMerchantScroll: false,
          });
          this.setState({
            enableListFinaceScroll: false,
          });
          this.refs.FinacelistView.scrollToOffset({ x: 0, y: 0, animated: true })
        }
        else {
          if (this.state.enableListFinaceScroll == false) {
            if (event.nativeEvent.contentOffset.y >  hp(39.5)) {

              this.setState({
                enableListFinaceScroll: true,
              });

              this.setState({
                enableListMerchantScroll: false,
              });


              this.refs.FinacelistView.scrollToOffset({ x: 0, y: 0, animated: true })
            }
            else {
              console.log('.....false');
              this.setState({
                enableListFinaceScroll: false,
              });
              this.refs.FinacelistView.scrollToOffset({ x: 0, y: 0, animated: true })
            }
          }

        }
      }
      else {
        if (event.nativeEvent.contentOffset.y <= 0) {
          this.setState({
            enableListMerchantScroll: false,
          });
          this.setState({
            enableListFinaceScroll: false,
          });
          this.refs.MerchantlistView.scrollToOffset({ x: 0, y: 0, animated: true })
        }
        else {
          if (this.state.enableListMerchantScroll == false) {
            if (event.nativeEvent.contentOffset.y > hp(39.5)) {
              console.log(event.nativeEvent.contentOffset.y);
              this.setState({
                enableListMerchantScroll: true,
              });
              this.setState({
                enableListFinaceScroll: false,
              });
              this.refs.MerchantlistView.scrollToOffset({ x: 0, y: 0, animated: true })
            }
            else {
              this.setState({
                enableListMerchantScroll: false,
              });
              this.refs.MerchantlistView.scrollToOffset({ x: 0, y: 0, animated: true })
            }
          }
        }
      }
    }
    else {

        if (event.nativeEvent.contentOffset.y <= 0) {
          this.setState({
            enableListMerchantScroll: false,
          });
          this.setState({
            enableListFinaceScroll: false,
          });
          this.onEnableScroll(true);
          if (this.state.isSelectedCat == "finance") {
            this.refs.FinacelistView.scrollToOffset({ x: 0, y: 0, animated: true })
          }
          else {
            this.refs.MerchantlistView.scrollToOffset({ x: 0, y: 0, animated: true })
          }
        }
        else
        {
          this.setState({
            enableListMerchantScroll: true,
          });
          this.setState({
            enableListFinaceScroll: true,
          });
          
          if (this.state.ContentScrollOffset == 0)
          {
            this.onEnableScroll(true);
          }
          else
          {
            this.onEnableScroll(false);
          }
          
        }
     
    }


  }

  handleAndroidScroll = (event) => {
   
    this.setState({ ContentScrollOffset: event.nativeEvent.contentOffset.y })
    if ((Platform.OS) == 'ios') {
    }
    else {

      if (this.state.enableScrollViewScroll == false) {

        if (event.nativeEvent.contentOffset.y <= 0) {
          console.log(this.state.enableScrollViewScroll);
          this.onEnableScroll(true);

        }
        else {
          console.log(this.state.enableScrollViewScroll);
          this.onEnableScroll(false);

        }


      }
      else {
        this.setState({ ContentScrollOffset: 0 })
      }


    }

  }
  //Back button click event
  handleBackButtonClick() {
    this.props.navigation.goBack()
    return true;
  }



  //catrgoty list UI
  TopListItem = (item) => {
    const { wayTopListView, wayTopListSubView, wayListTitleText, wayListDestext, wayDesView } = styles;
    return (
      <TouchableOpacity onPress={() => this.props.navigation.navigate('featuredCard',{
        onGoBack: () => this.refresh(),
      })}>
        <View style={wayTopListView}>
          <View style={{ ...wayTopListSubView, borderColor: item.bordercolor, shadowColor: item.textColor }}>
            <View style={{ borderTopLeftRadius: 15, borderTopRightRadius: 15, overflow: 'hidden' }}>
              <Text style={{ ...wayListTitleText, backgroundColor: item.txtbgColor, color: item.textColor }}>{item.name}</Text>
            </View>

            <Image source={item.image} style={{ height: hp(15), width: wp(37) }} resizeMode='contain' />
            <View style={{ height: 1, backgroundColor: Constant.color.sepratorColor }}></View>
            <View style={wayDesView}>
              <Text style={wayListDestext}>{item.des}</Text>
            </View>

          </View>

        </View>
      </TouchableOpacity>

    );
  }

  //finance list
  FinaceListItem = (item, index) => {
    const { FinaceListView, financeListTitletext, financeOfferText, DescriptionText } = styles;
    var redirectionScreen = "";

    if (item.indes == "in Credit Cards")
    {
      redirectionScreen = "creditCard"
    }
    else if  (item.indes == "in Savings")
    {
      redirectionScreen = "savingAC"
    }
    else if  (item.indes == "in Auto")
    {
      redirectionScreen = "insuranceList"
    }
    else
    {
      redirectionScreen = "investmentList"
    }
    return (
      

        <View style={{ ...FinaceListView, width: wp(37) }}>
          <TouchableOpacity onPress={() =>  this.props.navigation.navigate(redirectionScreen,{ onGoBack: () => this.refresh(),})}>
          <Text style={financeListTitletext}>{item.name}</Text>
          <Image source={item.image} style={{ alignSelf: 'center', height: hp(6), width: wp(12) }} resizeMode="contain" />
          <Text style={financeOfferText}>{item.offer}</Text>
          <Text style={{ ...DescriptionText, paddingBottom: Constant.isX ? hp(2) : hp(3) }}>{item.indes}</Text>
          </TouchableOpacity>
        </View>

   

    );
  }

  //Merchant list
  MerchantListItem = (item) => {
    const { merchantListView, financeListTitletext, merchantOffertext, DescriptionText } = styles;
    return (
      <View style={{ alignItems: 'center', justifyContent: 'center' }}>
        <View style={merchantListView}>
          <Text style={financeListTitletext}>{item.name}</Text>
          <Image source={item.image} style={{ alignSelf: 'center', height: hp(8), width: wp(20) }} resizeMode="contain" />

          <View style={{ alignItems: 'center', paddingHorizontal: (Platform.OS) == 'ios' ? hp(2) : hp(4), height: Constant.isX ? hp(11) : hp(13) }}>
            <Text style={{ ...merchantOffertext }}>{item.offer}</Text>
            <Text style={{ ...DescriptionText, paddingBottom: hp(1), color: Constant.color.lightgrayText }}>{item.indes}</Text>
          </View>
        </View>
      </View>
    );
  }


  render() {

    const { WayHeaderView, MarchatListSpaceing, container, wayNavigationItem, headerTitle, WayRightNavigationItem, wayUserNameTitle, waysubtitle, sliderbtn, slidertext, waysliderView, LetsLooktext, FinanceListSpaceing, FinaceListBgView } = styles;
    return (

      <View style={container} >
        {/*header view*/}
        <View style={WayHeaderView} >
          <View style={{ flexDirection: 'row', paddingLeft: 10 }}>
            <TouchableOpacity style={wayNavigationItem} onPress={() => this.handleBackButtonClick()}>
              <Image resizeMode='contain' source={require('../component/img/backicon.png')} style={{ width: wp(5), height: hp(2.2) }} />
            </TouchableOpacity>
            <Text style={{ ...headerTitle, padding: 5 }}>{strings('profile.WaytoSave')}</Text>
          </View>
          <View style={{ flexDirection: 'row', paddingRight: 10 }}>
            <TouchableOpacity style={wayNavigationItem} >
              <Image source={require('../component/img/question-icon.png')} style={WayRightNavigationItem} resizeMode='contain' />
            </TouchableOpacity>
            <TouchableOpacity style={wayNavigationItem} >
              <Image source={require('../component/img/avatar-icon.png')} style={WayRightNavigationItem} resizeMode='contain' />
            </TouchableOpacity>
          </View>

        </View>
        <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false} stickyHeaderIndices={[1]} scrollEnabled={this.state.enableScrollViewScroll} onScroll={this.handleScroll}>
          {/*top view*/}
          <View style={{ alignItems: 'center' }}>

            <Text style={wayUserNameTitle}>{strings('profile.Allyssa')}</Text>
            <Text style={waysubtitle}>{strings('profile.Hereareyouwaystosave')}</Text>

            <FlatList
              style={{ paddingTop: (Platform.OS) == 'ios' ? hp(0.5) : hp(1), paddingBottom: (Platform.OS) == 'ios' ? hp(0.5) : hp(1) }}
              contentContainerStyle={{ padding: 10 }}
              showsHorizontalScrollIndicator={false}
              horizontal={true}
              data={topListData}
              renderItem={({ item }) =>
                this.TopListItem(item)
              }
              keyExtractor={(item, index) => index.toString()}
            />

          </View>

          <View style={{ flex: 1, backgroundColor: Constant.color.customWhite }}>
            {/*sliderview*/}
            <View style={waysliderView}>
              <TouchableOpacity style={{ ...sliderbtn, borderBottomColor: this.state.isSelectedCat == 'finance' ? Constant.color.selectedtextColor : 'transparent' }} onPress={() => this.SliderClick()}>
                <Text style={{ ...slidertext, color: this.state.isSelectedCat == 'finance' ? Constant.color.selectedtextColor : Constant.color.lightgrayText }}>Finance</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ ...sliderbtn, borderBottomColor: this.state.isSelectedCat == 'finance' ? 'transparent' : Constant.color.selectedtextColor }} onPress={() => this.SliderClick()}>
                <Text style={{ ...slidertext, color: this.state.isSelectedCat == 'merchant' ? Constant.color.selectedtextColor : Constant.color.lightgrayText }}>Merchant Offers</Text>
              </TouchableOpacity>

            </View>

            {/*bottom list view*/}
            <View style={{ flex: 1, paddingVertical: hp(0.5) }}>
              <Text style={LetsLooktext}>{strings('profile.Letslookathowtosavemoney')}</Text>
              {
                this.state.isSelectedCat == "finance" ?
                  <FlatList
                    // onScroll={this.handleAndroidScroll}
                    ref="FinacelistView"
                    style={{ height: (Platform.OS) == 'ios' ? hp(80) : hp(70) }}
                    columnWrapperStyle={FinanceListSpaceing}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    scrollEnabled={this.state.enableListFinaceScroll}
                    onEnableScroll={this.handleAndroidScroll}
                    onTouchStart={() => {
                      this.onEnableScroll((Platform.OS) == 'ios' ? true : false);

                    }}
                    onScrollEndDrag={this.handleAndroidScroll}
                    numColumns={2}
                    data={financeData}
                    renderItem={({ item, index }) =>

                      this.FinaceListItem(item, index)
                    }
                    keyExtractor={(item, index) => index.toString()} />
                  : <FlatList
                    // onScroll={this.handleAndroidScroll}
                    ref="MerchantlistView"
                    columnWrapperStyle={MarchatListSpaceing}
                    showsHorizontalScrollIndicator={false}
                    scrollEnabled={this.state.enableListMerchantScroll}
                    onTouchStart={() => {
                      this.onEnableScroll((Platform.OS) == 'ios' ? true : false);

                    }}
                    onScrollEndDrag={this.handleAndroidScroll}

                    style={{ height: (Platform.OS) == 'ios' ? hp(80) : hp(70) }}
                    showsVerticalScrollIndicator={false}
                    horizontal={false}
                    numColumns={2}
                    data={MerchantData}
                    renderItem={({ item }) =>
                      this.MerchantListItem(item)
                    }
                    keyExtractor={(item, index) => index.toString()}
                  />
              }

            </View>

          </View>

        </ScrollView >
      </View>

    );
  }
};

