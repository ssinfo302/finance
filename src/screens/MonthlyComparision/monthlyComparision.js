import React, { useState, useEffect } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  Image
} from 'react-native';
import Constant from '../../helper/themeHelper';
import { wp, hp } from '../../helper/responsiveScreen';
import { strings } from '../../component/translation/i18n';
import BarChartExample from '../../common/BarChart/index';
import styles from '../../component/styles';
import Monthlystyles from  './styles';
import { ScrollView } from 'react-native-gesture-handler';

import chartData from '../../common/BarChart/barChartData.json';

export default class monthlyComparision extends React.Component {
   
    constructor(props) {
        super(props);
          this.state = {
            Title: this.props.route.params.title,
           }
      }

      //Back button click event
  handleBackButtonClick() {
    this.props.navigation.goBack()
    return true;
  };

    render() {
        const { WayHeaderView, container, wayNavigationItem, headerTitle } = styles;
        const {screenTitle,screenTitleText,infoText,blueText,redText,suggestion,suggestionText,suggestionBtn,SuggestionBtnText} = Monthlystyles;
        return (
    <View style={container} >
        {/*header view*/}
        <View style={WayHeaderView}>
          <View style={{ flexDirection: 'row', paddingLeft: 10 }}>
            <TouchableOpacity style={wayNavigationItem} onPress={() => this.handleBackButtonClick()}>
              <Image resizeMode='contain' source={require('../../component/img/backicon.png')} style={{ width: wp(5), height: hp(2.2) }} />
            </TouchableOpacity>
            <Text style={{ ...headerTitle, padding: 5 }}>{strings('profile.MonthlyComparision')}</Text>
          </View>
        </View>
            {/*chart view */}
            <ScrollView style={container}>
              <View style={{...container,marginBottom: hp(2)}}>
                <View style={screenTitle}>
                  <Text style={screenTitleText}>{this.state.Title}</Text>
                </View>
                <View style={{height: hp(30)}}>
                  <BarChartExample data={chartData.data} criteria={chartData.criteria} />
                </View>
                <View>
                  <Text style={infoText}>{'Your current Dining out spend in '}<Text style={blueText}>Apr</Text> is <Text style={blueText}>$65</Text>{'\nIt was '}<Text style={redText}>$62</Text>{' over your average in March.'}</Text>
                </View>
                <View style={suggestion}>
                  <Text style={suggestionText}>Suggestions</Text>
                  <TouchableOpacity style={suggestionBtn}>
                    <Text style={SuggestionBtnText}>Set Budget for Dining Out</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={suggestionBtn}>
                    <Text style={SuggestionBtnText}>Alert me weekly on my spend</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </ScrollView>
        </View>
          )
    }
  
}

