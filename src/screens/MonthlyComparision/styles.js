import {
    StyleSheet,
  } from 'react-native';
  import Constant from '../../helper/themeHelper';
  import { wp, hp } from '../../helper/responsiveScreen';
  
  const styles = StyleSheet.create({
    
    screenTitle: {
      marginVertical: hp(2)
    },
    screenTitleText: {
      fontSize: Constant.fontSize.medium,
      textAlign: 'center',
      fontFamily: 'Lato-Regular',
      fontWeight: 'normal',
      color: "#080808"
    },
    infoText: {
      fontSize: Constant.fontSize.small,
      textAlign: 'center',
      fontFamily: 'Lato-Regular',
      fontWeight: 'normal',
      lineHeight: 25
    },
    blueText: {
        fontSize: Constant.fontSize.small,
      fontWeight: 'bold',
      color: '#0090FF',
      fontFamily: 'Lato-Bold',
    },
    redText: {
        
        fontSize: Constant.fontSize.xxsmall,
      fontWeight: 'bold',
      fontFamily: 'Lato-Bold',
      color: '#DC7263'
    },
    suggestion: {
      marginTop: (Platform.OS) == 'ios' ? hp(5) : hp(4),
      marginHorizontal: wp(6),
      flexDirection: 'column',
      paddingHorizontal: wp(3),
      paddingVertical: hp(2),
      backgroundColor: Constant.color.white,
      borderRadius: 10,
     ...Constant.shadowStyle
    },
    suggestionText: {
      marginBottom: 20,
      textAlign: 'center',
      fontSize: Constant.color.xmedium,
      fontFamily: 'Lato-Regular',
      fontWeight: 'normal',
    },
    suggestionBtn: {
      width: '100%',
      alignItems: 'center',
      marginVertical: hp(1),
      paddingVertical: hp(2.5),
      paddingHorizontal: wp(2),
      borderRadius: 30,
      backgroundColor: '#E3F3FF'
    },
    SuggestionBtnText: {
        textAlign: 'center',
        fontSize: Constant.color.xxsmall,
        fontFamily: 'Lato-Bold',
        fontWeight: 'bold',
    }
  });
  
  export default styles;