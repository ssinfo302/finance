import React, { Component } from 'react'
import Constant from '../../helper/themeHelper';
import { View, Text, Image, FlatList, ScrollView ,SectionList} from 'react-native';
import { WebView } from 'react-native-webview';
import { wp, hp } from '../../helper/responsiveScreen';
import styles from '../../component/styles';
import Creditcardstyles from '../../screens/FeaturedCard/styles';
import insuranceDetailStyle from '../InsuranceDetail/styles';
import insuranceStyle from '../../screens/Insurance/styles';
import creditCardDetailStyles from '../../screens/CreditCardDetail/styles';
import { strings } from '../../component/translation/i18n';
import { TouchableOpacity } from 'react-native-gesture-handler'
import NumberFormate from '../../common/NumberFormate';
import HTMLView from 'react-native-htmlview';

export default class investmentDetail extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            Index: this.props.route.params.index,
            CardDetail: this.props.route.params.item,
            iskeyfeature: true,
            isfees: false,
            isAdditionalFeature: false,

        }
    }

    //Back button click event
    handleBackButtonClick() {
        this.props.navigation.goBack()
        return true;
    };

    handleKeyFeatureSelection(value) {
        this.setState({ iskeyfeature: value });
        this.setState({ isfees: false });
        this.setState({ isAdditionalFeature: false });

    }
    handleFeesSelection(value) {
        this.setState({ iskeyfeature: false });
        this.setState({ isfees: value });
        this.setState({ isAdditionalFeature: false });
    }
    handleAdditionalFeatureFeature(value) {
        this.setState({ iskeyfeature: false });
        this.setState({ isfees: false });
        this.setState({ isAdditionalFeature: value });
    }

    // Fees list UI
    FeesListItem = (item, index) => {
        const { DetailText, labelText } = creditCardDetailStyles;
        return (

            <View style={{ justifyContent: 'center' }}>
                <View style={{ flex: 1, justifyContent: 'space-between', flexDirection: 'row', alignItems: 'flex-start' }}>
                    <Text style={{ ...labelText, flex: 0.7 }}>{item.label == "" || item.label == "null" ? "-" : item.label}</Text>
                    <Text style={{ ...labelText, textAlign:'right', flex: 0.3 }}>{(item.rate == "" || item.rate == "null") ? '-' : item.rate}</Text>
                </View>

            </View>

        )
    }

    FeesFooter = () => {
        const { sepratorView } = creditCardDetailStyles;
        return (
           
            <View style={{ ...sepratorView }}></View>

        )
    }
    FeesHeader = (section, index) => {
        const { DetailText, labelText } = creditCardDetailStyles;
        return (
            <View style={{ justifyContent: 'center', paddingVertical: hp(0.5) }}>
                <View style={{ flex: 1, justifyContent: 'space-between', flexDirection: 'row', alignItems: 'flex-start' }}>
                     <Text style={{ ...DetailText,fontSize: Constant.fontSize.small, textAlign:'left', flex: 0.5 }}>{section.title1}</Text>
                    <Text style={{ ...DetailText,fontSize: Constant.fontSize.small, flex: 0.5}}>{section.title2}</Text>
                </View>
            </View>
        )
    }

    // Additional list UI
    AdditionalFeatureListItem = (item, index) => {
        const { labelText } = creditCardDetailStyles;
        return (

            <View style={{ justifyContent: 'center' }}>
                <View style={{ flex: 1, justifyContent: 'space-between', flexDirection: 'row', alignItems: 'flex-start' }}>
                    <Text style={{ ...labelText, flex: 0.6 }}>{item.label == "" || item.label == "null" ? "-" : item.label}</Text>
                    <Text style={{ ...labelText, textAlign:'right', flex: 0.4 }}>{(item.value == "" || item.value == "null") ? '-' : item.value}</Text>
                </View>

            </View>

        )
    }

    AdditionalFeatureHeader = (section, index) => {
        const { DetailText } = creditCardDetailStyles;
        return (
            <View style={{ justifyContent: 'center', paddingVertical: hp(0.5) }}>
               
                     <Text style={{ ...DetailText,fontSize: Constant.fontSize.small, textAlign:'left', flex: 0.1 }}>{section.title}</Text>
                  
                </View>
        
        )
    }

    render() {
        const { WayHeaderView, container, wayNavigationItem, headerTitle } = styles;
        const { updownimg, DetailFeatureView, FeatureTitleText, updownImgView, sepratorView, AddFeatureDetailText, } = creditCardDetailStyles;
        const { HightLightValueTextView, CreditCardView, CreditCardImageView, ShortListBtnText, HeartImage, HighLightText, ShortListBtnView, HightLightSubText, HightLightView, CreditCardBtnBgView, ApplyBtnView, ApplyBtnText } = Creditcardstyles;
        const { CardNameText, sumText, Ammounttext, DesTextView } = insuranceStyle;

        var detail = this.state.CardDetail;
        var FetatureDATA = detail.DATA.FEATURE;


        var HighlightValue1 = detail.DATA.HIGHLIGHTS.Value1
        var HighlightValue2 = detail.DATA.HIGHLIGHTS.Value2
        var HighlightValue3 = detail.DATA.HIGHLIGHTS.Value3

        if (HighlightValue1.includes('$')) {
            HighlightValue1 = (HighlightValue1).replace('$', '');
        }
        if (HighlightValue2.includes('$')) {
            HighlightValue2 = (HighlightValue2).replace('$', '');
        }
        if (HighlightValue3.includes('$')) {
            HighlightValue3 = (HighlightValue3).replace('$', '');
        }


        return (

            <View style={container} >
                {/*header view*/}
                <View style={WayHeaderView}>
                    <View style={{ flexDirection: 'row', paddingLeft: 10 }}>
                        <TouchableOpacity style={wayNavigationItem} onPress={() => this.handleBackButtonClick()}>
                            <Image resizeMode='contain' source={require('../../component/img/backicon.png')} style={{ width: wp(5), height: hp(2.2) }} />
                        </TouchableOpacity>
                        <Text style={{ ...headerTitle, padding: 5 }}>{detail.name}</Text>
                    </View>
                </View>
                <ScrollView showsVerticalScrollIndicator={false}>
                    {/* card view */}
                    <View style={{ ...CreditCardView, marginBottom: hp(1) }}>
                        <View style={{ ...CreditCardImageView, paddingTop: hp(1) }}>
                            <Image source={require('../Investment/img/stash.png')} style={{ alignSelf: 'center', height: hp(10), width: wp(60) }} resizeMode='contain' />
                            <Text style={{ ...CardNameText }}>{detail.name}</Text>
                        </View>

                        <View style={{ flexDirection: 'row', backgroundColor: "#FAF7F7", justifyContent: 'center', marginBottom: hp(2) }} >

                            <View style={{ ...HightLightView }}>
                                <View style={{ ...HightLightValueTextView }}>
                                    <Text style={{ ...HighLightText }}>{(detail.DATA.HIGHLIGHTS.Value1) ? (detail.DATA.HIGHLIGHTS.Value1 == "" || detail.DATA.HIGHLIGHTS.Value1 == "null") ? '-' : (detail.DATA.HIGHLIGHTS.Value1.includes(',') ? detail.DATA.HIGHLIGHTS.Value1 : (detail.DATA.HIGHLIGHTS.Value1 == HighlightValue1 ? detail.DATA.HIGHLIGHTS.Value1 : ((isNaN(parseFloat(HighlightValue1))) ? detail.DATA.HIGHLIGHTS.Value1 : '$' + NumberFormate.CommaFormatted(parseFloat(HighlightValue1).toFixed(2))))) : "-"}</Text>
                                </View>

                                <Text style={{ ...HightLightSubText }}>{detail.DATA.HIGHLIGHTS.Label1}</Text>
                            </View>

                            <View style={{ ...HightLightView }}>
                                <View style={{ ...HightLightValueTextView }}>
                                    <Text style={{ ...HighLightText }}>{(detail.DATA.HIGHLIGHTS.Value2) ? (detail.DATA.HIGHLIGHTS.Value2 == "" || detail.DATA.HIGHLIGHTS.Value2 == "null") ? '-' : (detail.DATA.HIGHLIGHTS.Value2.includes(',') ? detail.DATA.HIGHLIGHTS.Value2 : (detail.DATA.HIGHLIGHTS.Value2 == HighlightValue2 ? detail.DATA.HIGHLIGHTS.Value2 : ((isNaN(parseFloat(HighlightValue2))) ? detail.DATA.HIGHLIGHTS.Value2 : '$' + NumberFormate.CommaFormatted(parseFloat(HighlightValue2).toFixed(2))))) : "-"}</Text>
                                </View>
                                <Text style={{ ...HightLightSubText }}>{detail.DATA.HIGHLIGHTS.Label2}</Text>
                            </View>

                            <View style={{ ...HightLightView }}>
                                <View style={{ ...HightLightValueTextView }}>
                                    <Text style={{ ...HighLightText }}>{(detail.DATA.HIGHLIGHTS.Value3) ? (detail.DATA.HIGHLIGHTS.Value3 == "" || detail.DATA.HIGHLIGHTS.Value3 == "null") ? '-' : (detail.DATA.HIGHLIGHTS.Value3.includes(',') ? detail.DATA.HIGHLIGHTS.Value3 : (detail.DATA.HIGHLIGHTS.Value3 == HighlightValue3 ? detail.DATA.HIGHLIGHTS.Value3 : ((isNaN(parseFloat(HighlightValue3))) ? detail.DATA.HIGHLIGHTS.Value3 : '$' + NumberFormate.CommaFormatted(parseFloat(HighlightValue3).toFixed(2))))) : "-"}</Text>
                                </View>
                                <Text style={{ ...HightLightSubText }}>{detail.DATA.HIGHLIGHTS.Label3}</Text>
                            </View>
                        </View>

                    </View>
                    {/*Investment card Detail*/}
                    <View style={{ marginHorizontal: wp(2.5) }}>

                        {/* KEY FEATURE */}
                        <View>
                            <TouchableOpacity style={{ ...DetailFeatureView }} onPress={() => this.state.iskeyfeature == true ? this.handleKeyFeatureSelection(false) : this.handleKeyFeatureSelection(true)}>
                                <Text style={{ ...FeatureTitleText }}>{(detail.DATA.FEATURE.TITLE) ? detail.DATA.FEATURE.TITLE : "Key Features"}</Text>

                                <View style={updownImgView}>
                                    <Image style={updownimg} resizeMode='contain' source={this.state.iskeyfeature == false ? require('../CreditCardDetail/img/downarrow.png') : require('../CreditCardDetail/img/uparrow.png')}></Image>
                                </View>
                            </TouchableOpacity>
                        </View>
                        {
                            this.state.iskeyfeature == true ?
                                <View>
                                    <View style={{ ...sepratorView, marginHorizontal: wp(2) }}></View>
                                    {
                                        Object.entries(FetatureDATA).map(([key, value]) => {
                                            if (key != "TITLE") {
                                                if (value == 'null' || value == '') {

                                                }
                                                else {
                                                    return (
                                                        <View style={DesTextView}>

                                                            <HTMLView
                                                                style={{ paddingVertical: hp(1.5), paddingLeft: wp(2), paddingRight: wp(2.5), textAlign: 'left' }}
                                                                value={value}
                                                                stylesheet={insuranceDetailStyle}
                                                            />

                                                        </View>
                                                    )
                                                }
                                            }

                                        })
                                    }
                                     <View style={{ ...sepratorView,marginHorizontal: wp(2) }}></View>

                                </View>
                                : <View style={{ ...sepratorView ,marginHorizontal: wp(2)}}></View>
                        }
                        {/* FEES */}
                        <View>
                            <TouchableOpacity style={{ ...DetailFeatureView }} onPress={() => this.state.isfees == true ? this.handleFeesSelection(false) : this.handleFeesSelection(true)}>
                                <Text style={{ ...FeatureTitleText }}>{(detail.DATA.FEES.TITLE) ? detail.DATA.FEES.TITLE : "Fees"}</Text>

                                <View style={updownImgView}>
                                    <Image style={updownimg} resizeMode='contain' source={this.state.isfees == false ? require('../CreditCardDetail/img/downarrow.png') : require('../CreditCardDetail/img/uparrow.png')}></Image>
                                </View>
                            </TouchableOpacity>
                        </View>
                        {
                            this.state.isfees == true ?
                                <View>
                                    <View style={{ ...sepratorView, marginHorizontal: wp(2) }}></View>
                                    <SectionList
                                        sections={[{
                                            "Category1": "", "title1": "Total Investment (SGD)",
                                            "title2": "Annual Fee rate \n(incl GST)", "data": [{
                                                "label": "First $25,000",
                                                "rate": "0.80%"
                                            },
                                            {
                                                "label": "$25,000 to $50,000",
                                                "rate": "0.70%"
                                            },
                                            {
                                                "label": "$50,000 to $100,000",
                                                "rate": "0.60%"
                                            },
                                            {
                                                "label": "$100,000 to $250,000",
                                                "rate": "0.50%"
                                            },
                                            {
                                                "label": "$250,000 to $500,000",
                                                "rate": "0.40%"
                                            },
                                            {
                                                "label": "$500,000 to $1,000,000",
                                                "rate": "0.30%"
                                            },
                                            {
                                                "label": "Above $1,000,000",
                                                "rate": "0.20%"
                                            }]
                                        }]}
                                        contentContainerStyle={{ marginHorizontal: wp(2.5) }}
                                        showsVerticalScrollIndicator={false}
                                        renderSectionFooter={() => this.FeesFooter()}
                                        renderItem={({ item, index }) =>
                                            this.FeesListItem(item, index)
                                        }
                                        renderSectionHeader={({ section, index }) => this.FeesHeader(section, index)}
                                        keyExtractor={(item, index) => index}
                                    />
                                </View>
                                : <View style={{ ...sepratorView ,marginHorizontal: wp(2)}}></View>
                        }
                        {/* ADDITIONAL FEATURE */}
                        <View>
                            <TouchableOpacity style={{ ...DetailFeatureView }} onPress={() => this.state.isAdditionalFeature == true ? this.handleAdditionalFeatureFeature(false) : this.handleAdditionalFeatureFeature(true)}>
                                <Text style={{ ...FeatureTitleText }}>{(detail.DATA.ADDITINAL_FEATURES.TITLE) ? detail.DATA.ADDITINAL_FEATURES.TITLE : "Additional Features"}</Text>

                                <View style={updownImgView}>
                                    <Image style={updownimg} resizeMode='contain' source={this.state.isAdditionalFeature == false ? require('../CreditCardDetail/img/downarrow.png') : require('../CreditCardDetail/img/uparrow.png')}></Image>
                                </View>
                            </TouchableOpacity>
                        </View>
                        {
                            this.state.isAdditionalFeature == true ?
                                <View>
                                    <View style={{ ...sepratorView, marginHorizontal: wp(2) }}></View>
                                    <SectionList
                                        sections={[{
                                            "title":"Access to financial advisors", "data": [{
                                                "label":"Blue (<$20k invested)",
                                                "value":"First month only"
                                             },
                                             {
                                                "label":"Black (<$20k to $100k)",
                                                "value":"Yes"
                                             },
                                             {
                                                "label":"Gold (>$100k)",
                                                "value":"Yes with dedicated advisor"
                                             }]
                                        }, {
                                            "title":"Financial Planning", "data": [{
                                                "label":"Blue (<$20k invested)",
                                                "value":"S$ 299"
                                             },
                                             {
                                                "label":"Black (<$20k to $100k)",
                                                "value":"Included"
                                             },
                                             {
                                                "label":"Gold (>$100k)",
                                                "value":"Included with quarterly review"
                                             }]
                                        },
                                        {
                                            "title":"Customer Care ", "data": [{
                                                "label":"Blue (<$20k invested)",
                                                "value":"All channels"
                                             },
                                             {
                                                "label":"Black (<$20k to $100k)",
                                                "value":"All channels"
                                             },
                                             {
                                                "label":"Gold (>$100k)",
                                                "value":"All channels"
                                             }]
                                        }]}
                                        contentContainerStyle={{ marginHorizontal: wp(2.5) }}
                                        showsVerticalScrollIndicator={false}
                                        renderSectionFooter={() => this.FeesFooter()}
                                        renderItem={({ item, index }) =>
                                            this.AdditionalFeatureListItem(item, index)
                                        }
                                        renderSectionHeader={({ section, index }) => this.AdditionalFeatureHeader(section, index)}
                                        keyExtractor={(item, index) => index}
                                    />
                                </View>
                                : <View style={{ ...sepratorView ,marginHorizontal: wp(2)}}></View>
                        }
                    </View>
                    <View style={{ ...CreditCardBtnBgView, marginVertical: hp(2) }}>
                        <TouchableOpacity style={{ ...ApplyBtnView }}>
                            <Text style={{ ...ApplyBtnText }}>{strings('profile.apply')}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ ...ShortListBtnView }}>
                            <Image source={require('../FeaturedCard/img/heart.png')} style={{ ...HeartImage }} resizeMode='contain' />
                            <Text style={{ ...ShortListBtnText }}>{strings('profile.shortList')}</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        )
    };

};