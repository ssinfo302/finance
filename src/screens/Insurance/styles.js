import {
    StyleSheet
  } from 'react-native';
  import Constant from '../../helper/themeHelper';
  import { wp, hp } from '../../helper/responsiveScreen';

  const styles = StyleSheet.create({
      CardNameText: {
        color: Constant.color.textgray,
        fontSize: Constant.fontSize.medium,
        fontWeight: 'bold',
        fontFamily: "Lato-Bold",
        paddingBottom: hp(1.5),
        paddingTop:hp(0.5),
         textAlign:"center"
      },
      sumText: {
        flex: 0.5,
        textAlign:'left',
        color: Constant.color.textgray, 
        fontSize: Constant.fontSize.xsmall, 
        fontWeight: 'normal', 
        fontFamily: "Lato-Regular", 
        paddingTop: hp(1),
        paddingBottom: hp(0.2)
      },
      Ammounttext:{
        flex: 0.5,
        color: Constant.color.textgray,
         fontSize: Constant.fontSize.xsmall, 
         fontWeight: 'bold', 
         fontFamily: "Lato-Bold",
         paddingTop: hp(1),
         paddingBottom: hp(0.2),
          textAlign:'right'
      },
      policyincludeText: {
        
            color: Constant.color.textgray,
             fontSize: Constant.fontSize.xsmall, 
             fontWeight: 'normal', 
             fontFamily: "Lato-Regular",
              textAlign:'center',
              paddingVertical: hp(0.5)
        
      }
});

export default styles;