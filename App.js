/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
} from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import FinanceProfile from  './src/screens/financeProfile';
import Budget from  './src/screens/budget';
import BudgetList from  './src/screens/budgetList';
import waytoSavePage from './src/screens/waytoSavePage';
import featuredCard from './src/screens/FeaturedCard/featuredCard';
import creditCard from './src/screens/CreditCard/creditCard';
import creditCardDetail from './src/screens/CreditCardDetail/creditCardDetail';
import savingAC from'./src/screens/SavingAC/savingAC';
import savingCardDetail from './src/screens/SavingCardDetail/savingCardDetail';
import spendingSummary from './src/screens/spendingSummary/spendingSummary';
import monthlyComparision from './src/screens/MonthlyComparision/monthlyComparision';
import insuranceList from './src/screens/Insurance/insuranceList';
import insuranceDetail from './src/screens/InsuranceDetail/insuranceDetail';
import investmentList from './src/screens/Investment/investmentList';
import investmentDetail from './src/screens/InvestmentDetail/investmentDetail';

const Stack = createStackNavigator();

class App extends React.Component {

  constructor() {
    super();

    this.state = {

    }
  }

  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator
        screenOptions={{
          headerShown: false
        }}>
           
          <Stack.Screen name="waytoSavePage" component={waytoSavePage} />
            <Stack.Screen name="featuredCard" component={featuredCard} />
            <Stack.Screen name="creditCard" component={creditCard} />
            <Stack.Screen name="creditCardDetail" component={creditCardDetail} />
            <Stack.Screen name="savingAC" component={savingAC} />
            <Stack.Screen name="savingCardDetail" component={savingCardDetail} />
            <Stack.Screen name="insuranceList" component={insuranceList} />
            <Stack.Screen name="insuranceDetail" component={insuranceDetail} />
            <Stack.Screen name="investmentList" component={investmentList} />
            <Stack.Screen name="investmentDetail" component={investmentDetail} />
          <Stack.Screen name="Budget" component={Budget} />
          <Stack.Screen name="BudgetList" component={BudgetList} />
          <Stack.Screen name="FinanceProfile" component={FinanceProfile} />
          <Stack.Screen name="spendingSummary" component={spendingSummary} />
          <Stack.Screen name="monthlyComparision" component={monthlyComparision} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

console.disableYellowBox = true;

export default App;
